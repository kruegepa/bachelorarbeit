# imports
import NeuralNetwork
import BasisModel
import ReferenceModel
import Parameters
import Simulation
import PlotModels
import Simulation


class Validation:
    def plot_no_air(self):
        # load and change parameters
        MyRotor = Parameters.Rotor()
        MyRotor.air_density = 1e-8
        MyRotor.force_retrain_neural_network = False

        # create models
        MyBasisModel = BasisModel.Rotor(MyRotor)
        MyReferenceModel = ReferenceModel.Rotor(MyRotor)
        MyNeuralNetwork = NeuralNetwork.Model(MyRotor, MyReferenceModel)

        # generate plot
        MyPlotter = PlotModels.Plotter()
        MyPlotter.generate_simulation_plot(Simulation.Simulator(), MyBasisModel, MyReferenceModel, MyNeuralNetwork, MyRotor.x0, MyRotor.U, MyRotor.delta_t, MyRotor.steps)


    def plot_small_wing_area(self):
        # load and change parameters
        MyRotor = Parameters.Rotor()
        MyRotor.wing_area = 1e-5
        MyRotor.wing_wide = 0
        MyRotor.force_retrain_neural_network = False

        # create models
        MyBasisModel = BasisModel.Rotor(MyRotor)
        MyReferenceModel = ReferenceModel.Rotor(MyRotor)
        MyNeuralNetwork = NeuralNetwork.Model(MyRotor, MyReferenceModel)

        # generate plot
        MyPlotter = PlotModels.Plotter()
        MyPlotter.generate_simulation_plot(Simulation.Simulator(), MyBasisModel, MyReferenceModel, MyNeuralNetwork, MyRotor.x0, MyRotor.U, MyRotor.delta_t, MyRotor.steps)


    def plot_no_wings(self):
        # load and change parameters
        MyRotor = Parameters.Rotor()
        MyRotor.number_wings = 1e-10
        MyRotor.force_retrain_neural_network = False

        # create models
        MyBasisModel = BasisModel.Rotor(MyRotor)
        MyReferenceModel = ReferenceModel.Rotor(MyRotor)
        MyNeuralNetwork = NeuralNetwork.Model(MyRotor, MyReferenceModel)

        # generate plot
        MyPlotter = PlotModels.Plotter()
        MyPlotter.generate_simulation_plot(Simulation.Simulator(), MyBasisModel, MyReferenceModel, MyNeuralNetwork, MyRotor.x0, MyRotor.U, MyRotor.delta_t, MyRotor.steps)



MyValidation = Validation()
#MyValidation.plot_no_air()
MyValidation.plot_no_wings()
#MyValidation.plot_small_wing_area()
