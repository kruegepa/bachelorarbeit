# imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from sklearn.metrics import r2_score
import BasisModel
import ReferenceModel
import NeuralNetwork
import Parameters
import Simulation
from scipy.ndimage import gaussian_filter1d
import scipy.optimize as sci
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split


#################################################
# PlotSzenario                                  #
#################################################
class PlotSzenario:
    def __init__(self, steps=1000, delta_t=0.001, incidence_angle=np.full(1000, 0), tension=np.full(1000, 0), number_wings=3, wing_area=0.25, wing_wide=0.294, air_density=1.225, name=""):
        # settings
        self.Simulator = Simulation.Simulator()
        self.Rotor = Parameters.Rotor()
        self.Rotor.steps = steps
        self.Rotor.delta_t = delta_t
        self.Rotor.incidence_angle = incidence_angle
        self.Rotor.tension = tension
        self.Rotor.number_wings = number_wings
        self.Rotor.wing_area = wing_area
        self.Rotor.wing_wide = wing_wide
        self.Rotor.air_density = air_density
        self.Rotor.U = np.column_stack((self.Rotor.incidence_angle, self.Rotor.tension))
        self.file_name = name
        plot_basis = True
        plot_reference = True
        plot_neural_network = True

        # create plot
        if plot_basis:
            MyBasisModel = BasisModel.Rotor(self.Rotor)
        else:
            MyBasisModel = 0
        if plot_reference:
            MyReferenceModel = ReferenceModel.Rotor(self.Rotor)
        else:
            MyReferenceModel = 0
        if plot_neural_network:
            MyNeuralNetwork = NeuralNetwork.Model(self.Rotor, MyReferenceModel)
        else:
            MyNeuralNetwork = 0

        self.generate_plots(self.Simulator, MyBasisModel, MyReferenceModel, MyNeuralNetwork, self.Rotor.x0, self.Rotor.U, self.Rotor.delta_t, self.Rotor.steps, self.file_name)


    def plot_height(self, data_basis_model, data_reference_model, data_neural_network, steps, delta_t):
        plt.subplot(2, 4, 1)
        plt.plot(data_basis_model[2], data_basis_model[1].T[0], "g", label="Basismodell")
        plt.plot(data_reference_model[2], data_reference_model[1].T[0], "b", label="Referenzmodell")
        plt.plot(data_neural_network[2], data_neural_network[1].T[0], "y", label="Neuronales Netz")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Höhe $z$ in m", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)

    def plot_vertical_speed(self, data_basis_model, data_reference_model, data_neural_network, steps, delta_t):
        plt.subplot(2, 4, 2)
        plt.plot(data_basis_model[2], data_basis_model[1].T[1], "g", label="Basismodell")
        plt.plot(data_reference_model[2], data_reference_model[1].T[1], "b", label="Referenzmodell")
        plt.plot(data_neural_network[2], data_neural_network[1].T[1], "y", label="Neuronales Netz")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("vertikale Geschwindigkeit $\dot{z}$ in m/s", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)

    def plot_rotation_speed(self, data_basis_model, data_reference_model, data_neural_network, steps, delta_t):
        plt.subplot(2, 4, 3)
        plt.plot(data_basis_model[2], data_basis_model[1].T[2], "g", label="Basismodell")
        plt.plot(data_reference_model[2], data_reference_model[1].T[2], "b", label="Referenzmodell")
        plt.plot(data_neural_network[2], data_neural_network[1].T[2], "y", label="Neuronales Netz")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Rotationsgeschwindigkeit $\Omega$ in U/s", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)

    def get_basis_model_kinetic_energy(self, BasisModel, data_simple_model):
        return np.abs(1/2 * BasisModel.Parameters.total_mass * np.square(data_simple_model[1].T[1]))

    def get_reference_model_kinetic_energy(self, ReferenceModel, data_reference_model):
        return np.abs(1/2 * ReferenceModel.Parameters.total_mass * np.square(data_reference_model[1].T[1]))

    def get_neural_network_kinetic_energy(self, ReferenceModel, data_neural_network_model):
        return np.abs(1/2 * ReferenceModel.Parameters.total_mass * np.square(data_neural_network_model[1].T[1]))

    def get_basis_model_potential_energy(self, BasisModel, data_simple_model):
        return BasisModel.Parameters.total_mass * BasisModel.Parameters.gravitation * data_simple_model[1].T[0]

    def get_reference_model_potential_energy(self, ReferenceModel, data_reference_model):
        return ReferenceModel.Parameters.total_mass * ReferenceModel.Parameters.gravitation * data_reference_model[1].T[0]

    def get_neural_network_potential_energy(self, ReferenceModel, data_neural_network_model):
        return ReferenceModel.Parameters.total_mass * ReferenceModel.Parameters.gravitation * data_neural_network_model[1].T[0]
        
    def get_basis_model_rotational_energy(self, BasisModel, data_simple_model):
        return 1/2 * BasisModel.inertial_moment * np.square(data_simple_model[1].T[2])

    def get_reference_model_rotational_energy(self, ReferenceModel, data_reference_model):
        return 1/2 * ReferenceModel.inertial_moment * np.square(data_reference_model[1].T[2])

    def get_neural_network_rotational_energy(self, ReferenceModel, data_neural_network_model):
        return 1/2 * ReferenceModel.inertial_moment * np.square(data_neural_network_model[1].T[2])

    def get_basis_model_total_energy(self, BasisModel, data_simple_model):
        return self.get_basis_model_kinetic_energy(BasisModel, data_simple_model) + self.get_basis_model_potential_energy(BasisModel, data_simple_model) + self.get_basis_model_rotational_energy(BasisModel, data_simple_model)
 
    def get_reference_model_total_energy(self, ReferenceModel, data_reference_model):
        return self.get_reference_model_kinetic_energy(ReferenceModel, data_reference_model) + self.get_reference_model_potential_energy(ReferenceModel, data_reference_model) + self.get_reference_model_rotational_energy(ReferenceModel, data_reference_model)

    def get_neural_network_total_energy(self, ReferenceModel, data_neural_network_model):
        return self.get_neural_network_kinetic_energy(ReferenceModel, data_neural_network_model) + self.get_neural_network_potential_energy(ReferenceModel, data_neural_network_model) + self.get_neural_network_rotational_energy(ReferenceModel, data_neural_network_model)       

    def plot_energy(self, BasisModel, ReferenceModel, data_basis_model, data_reference_model, data_neural_network, steps, delta_t):
        plt.subplot(2, 4, 4)
        
        # plot kinetic energy
        plt.plot(data_basis_model[2], self.get_basis_model_kinetic_energy(BasisModel, data_basis_model), "g--", label="Basismodell: $E_\mathrm{kin}$")
        plt.plot(data_reference_model[2], self.get_reference_model_kinetic_energy(ReferenceModel, data_reference_model), "b--", label="Referenzmodell: $E_\mathrm{kin}$")
        plt.plot(data_neural_network[2], self.get_neural_network_kinetic_energy(ReferenceModel, data_neural_network), "y--", label="Neuronales Netz: $E_\mathrm{kin}$")

        # plot potential energy
        plt.plot(data_basis_model[2], self.get_basis_model_potential_energy(BasisModel, data_basis_model), "g-.", label="Basismodell: $E_\mathrm{pot}$")
        plt.plot(data_reference_model[2], self.get_reference_model_potential_energy(ReferenceModel, data_reference_model), "b-.", label="Referenzmodell: $E_\mathrm{pot}$")
        plt.plot(data_neural_network[2], self.get_neural_network_potential_energy(ReferenceModel, data_neural_network), "y-.", label="Neuronales Netz: $E_\mathrm{pot}$")

        # plot rotational energy
        plt.plot(data_basis_model[2], self.get_basis_model_rotational_energy(BasisModel, data_basis_model), "g:", label="Basismodell: $E_\mathrm{rot}$")
        plt.plot(data_reference_model[2], self.get_reference_model_rotational_energy(ReferenceModel, data_reference_model), "b:", label="Referenzmodell: $E_\mathrm{rot}$")
        plt.plot(data_neural_network[2], self.get_neural_network_rotational_energy(ReferenceModel, data_neural_network), "y:", label="Neuronales Netz: $E_\mathrm{rot}$")

        # plot total energy
        plt.plot(data_basis_model[2], self.get_basis_model_total_energy(BasisModel, data_basis_model), "g-", label="Basismodell: $E_\mathrm{tot}$")
        plt.plot(data_reference_model[2], self.get_reference_model_total_energy(ReferenceModel, data_reference_model), "b-", label="Referenzmodell: $E_\mathrm{tot}$")
        plt.plot(data_neural_network[2], self.get_neural_network_total_energy(ReferenceModel, data_neural_network), "y-", label="Neuronales Netz: $E_\mathrm{tot}$")

        # plot settings
        plt.legend(handles=[
            Line2D([], [], linestyle='--', color='black'),
            Line2D([], [], linestyle='-.', color='black'),
            Line2D([], [], linestyle=':', color='black'),
            Line2D([], [], linestyle='-', color='black')],
            labels=["$E_\mathrm{kin}$", "$E_\mathrm{pot}$", "$E_\mathrm{rot}$", "$E_\mathrm{tot}$"],
            loc=1
        )
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.xlabel('Zeit $t$ in s', fontsize=10)
        plt.ylabel('Energie $E$ in J', fontsize=10)

    def plot_power(self, steps, delta_t):
        plt.subplot(2, 4, 8)
        plt.legend(loc=1)
        plt.xlim(0, steps * delta_t)
        plt.xlabel('Zeit $t$ in s', fontsize=10)
        plt.ylabel('Leistung $P$ in W', fontsize=10)
 
    def plot_incidence_angle(self, data_basis_model, data_reference_model, data_neural_network, steps, delta_t, U):
        plt.subplot(2, 4, 5)
        plt.plot(data_reference_model[2][:-1], U[:,0], "r", label="Anstellwinkel")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Anstellwinkel ${\\beta}$ in Grad", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)

    def plot_tension(self, data_basis_model, data_reference_model, data_neural_network, steps, delta_t, U):
        plt.subplot(2, 4, 6)
        plt.plot(data_reference_model[2][:-1], U[:,1], "r", label="Zugkraft")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Zugkraft $T$ in N", fontsize=10)
        plt.axhline(0, color='black', lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)

    def plot_average_force(self, data_simple_model, data_reference_model, data_neural_network, steps, delta_t, U):
        plt.subplot(2, 4, 7)
        plt.plot(data_simple_model[2], data_simple_model[1].T[3], "g", label="Basismodell")
        plt.plot(data_reference_model[2], data_reference_model[1].T[3], "b", label="Referenzmodell")
        plt.plot(data_neural_network[2], data_neural_network[1].T[3], "y", label="Neuronales Netz")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Mittlere Kraft $\\bar{F}$ in N", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)

    def plot_power(self, data_simple_model, data_reference_model, data_neural_network, BasisModel, ReferenceModel, NeuralNetwork, steps, delta_t):
        plt.subplot(2, 4, 8)
        # gravitational power
        reference_gravitational_power = self.Rotor.gravitational_force * data_reference_model[1].T[1][:-1]
        plt.plot(data_reference_model[2][:-1], reference_gravitational_power, "b--", label="Referenzmodell: $P_\mathrm{grav}$")
        basis_gravitational_power = self.Rotor.gravitational_force * data_simple_model[1].T[1][:-1]
        plt.plot(data_simple_model[2][:-1], basis_gravitational_power, "g--", label="Basismodell: $P_\mathrm{grav}$")

        # tension power
        reference_tension_power = self.Rotor.tension * data_reference_model[1].T[1][:-1]
        plt.plot(data_reference_model[2][:-1], reference_tension_power, "b-.", label="Referenzmodell: $P_\mathrm{Zug}$")
        basis_tension_power = self.Rotor.tension * data_simple_model[1].T[1][:-1]
        plt.plot(data_simple_model[2][:-1], basis_tension_power, "g-.", label="Basismodell: $P_\mathrm{Zug}$")

        # vertical power
        reference_vertical_power = (self.Rotor.total_mass * np.diff(data_reference_model[1].T[1]) / self.Rotor.delta_t) * data_reference_model[1].T[1][:-1]
        plt.plot(data_reference_model[2][:-1], reference_vertical_power, "b:", label="Referenzmodell: $P_\mathrm{vert}$")
        basis_vertical_power = (self.Rotor.total_mass * np.diff(data_simple_model[1].T[1]) / self.Rotor.delta_t) * data_simple_model[1].T[1][:-1]
        plt.plot(data_simple_model[2][:-1], basis_vertical_power, "g:", label="Basismodell: $P_\mathrm{vert}$")

        # tangential power
        reference_tangential_power = np.diff(data_reference_model[1].T[2]) * np.diff(data_reference_model[1].T[2]) * ReferenceModel.inertial_moment / self.Rotor.delta_t
        plt.plot(data_reference_model[2][:-1], reference_tangential_power, "b", linestyle=(0, (1, 3)), label="Referenzmodell: $P_\mathrm{tang}$")
        basis_tangential_power = np.diff(data_simple_model[1].T[2]) * np.diff(data_simple_model[1].T[2]) * BasisModel.inertial_moment / self.Rotor.delta_t
        plt.plot(data_simple_model[2][:-1], basis_tangential_power, "g", linestyle=(0, (1, 3)), label="Basismodell: $P_\mathrm{tang}$")

        # total power
        reference_total_power = reference_gravitational_power + reference_tension_power + reference_vertical_power + reference_tangential_power
        plt.plot(data_reference_model[2][:-1], reference_total_power, "b-", label="Referenzmodell: $P_\mathrm{tot}$")
        basis_total_power = basis_gravitational_power + basis_tension_power + basis_vertical_power + basis_tangential_power
        plt.plot(data_simple_model[2][:-1], basis_total_power, "g-", label="Basismodell: $P_\mathrm{tot}$")

        # plot settings
        plt.legend(handles=[Line2D([], [], linestyle='--', color='black'),
            Line2D([], [], linestyle='-.', color='black'),
            Line2D([], [], linestyle=':', color='black'),
            Line2D([], [], linestyle=(0, (1, 3)), color='black'),
            Line2D([], [], linestyle='-', color='black')],
            labels=["$P_\mathrm{grav}$", "$P_\mathrm{Zug}$", "$P_\mathrm{vert}$", "$P_\mathrm{tang}$", "$P_\mathrm{tot}$"],
            loc=1
        )

        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.Rotor.steps * self.Rotor.delta_t)
        plt.xlabel('Zeit $t$ in s', fontsize=10)
        plt.ylabel('Leistung $P$ in W', fontsize=10)

    def get_erk4_simulation_data(self, Simulator, BasisModel, ReferenceModel, NeuralNetwork, x0, U, delta_t):
        data_simple_model = Simulator.simulate_explicit_simple_model(BasisModel, x0, U, delta_t)
        data_reference_model = Simulator.simulate_explicit_reference_model(ReferenceModel, x0, U, delta_t)
        data_neural_network_model = data_reference_model
        # data_neural_network_model = Simulator.simulate_explicit_neuronal_network_model(NeuralNetwork, x0, U, delta_t)
        return [data_simple_model, data_reference_model, data_neural_network_model]


    def generate_simulation_plot(self, BasisModel, ReferenceModel, NeuralNetwork, data_simple_model, data_reference_model, data_neural_network, steps, delta_t, U, name):
        plt.figure(figsize=(12, 6))
        self.plot_height(data_simple_model, data_reference_model, data_neural_network, steps, delta_t)
        self.plot_vertical_speed(data_simple_model, data_reference_model, data_neural_network, steps, delta_t)
        self.plot_rotation_speed(data_simple_model, data_reference_model, data_neural_network, steps, delta_t)
        self.plot_energy(BasisModel, ReferenceModel, data_simple_model, data_reference_model, data_neural_network, steps, delta_t)
        self.plot_tension(data_simple_model, data_reference_model, data_neural_network, steps, delta_t, U)
        self.plot_incidence_angle(data_simple_model, data_reference_model, data_neural_network, steps, delta_t, U)
        self.plot_average_force(data_simple_model, data_reference_model, data_neural_network, steps, delta_t, U)
        self.plot_power(data_simple_model, data_reference_model, data_neural_network, BasisModel, ReferenceModel, NeuralNetwork, steps, delta_t)
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/Simulation" + name + ".png", dpi=500)
        plt.savefig("Simulation/Graphiken/Simulation" + name + ".svg")


    def generate_plots(self, Simulator, BasisModel, ReferenceModel, NeuralNetwork, x0, U, delta_t, steps, name):
        data_simple_model, data_reference_model, data_neural_network = self.get_erk4_simulation_data(Simulator, BasisModel, ReferenceModel, NeuralNetwork, x0, U, delta_t)
        self.generate_simulation_plot(BasisModel, ReferenceModel, NeuralNetwork, data_simple_model, data_reference_model, data_neural_network, steps, delta_t, U, name)
        self.generate_basis_data_plot(BasisModel, data_simple_model, steps, delta_t, name)


    def plot_angles(self, BasisModel, data, steps, delta_t):
        plt.subplot(1, 3, 1)
        plt.plot(data[2][:-1], [d[0] for d in BasisModel.data[::4]], label="$\gamma$ in $\deg$")
        plt.plot(data[2][:-1], [d[1] for d in BasisModel.data[::4]], label="$\\alpha$ in $\deg$")
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlabel("Zeit in s")
        plt.ylabel("Winkel in $\deg$")
        plt.xlim(0, steps * delta_t)
        plt.legend()


    def plot_coefficients_velocity(self, BasisModel, data, steps, delta_t):
        plt.subplot(1, 3, 2)
        plt.plot(data[2][:-1], [d[2] for d in BasisModel.data[::4]], label="$C_\mathrm{L}$")
        plt.plot(data[2][:-1], [d[3] for d in BasisModel.data[::4]], label="$C_\mathrm{D}$")
        plt.plot(data[2][:-1], [d[4] for d in BasisModel.data[::4]], label="$v_\mathrm{wind}$ in m/s")
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlabel("Zeit in s")
        plt.ylabel("Koeffizient / Geschwindigkeit in m/s")
        plt.xlim(0, steps * delta_t)
        plt.legend()

    
    def plot_forces(self, BasisModel, data, steps, delta_t):
        plt.subplot(1, 3, 3)
        plt.plot(data[2][:-1], [d[5] for d in BasisModel.data[::4]], label="$F_\mathrm{L,v}$ in N")
        plt.plot(data[2][:-1], [d[6] for d in BasisModel.data[::4]], label="$F_\mathrm{L,h}$ in N")
        plt.plot(data[2][:-1], [d[7] for d in BasisModel.data[::4]], label="$F_\mathrm{D,v}$ in N")
        plt.plot(data[2][:-1], [d[8] for d in BasisModel.data[::4]], label="$F_\mathrm{D,h}$ in N")
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlabel("Zeit in s")
        plt.ylabel("Kraft in N")
        plt.xlim(0, steps * delta_t)
        plt.legend()


    def generate_basis_data_plot(self, BasisModel, data, steps, delta_t, name):
        self.plot_angles(BasisModel, data, steps, delta_t)
        self.plot_coefficients_velocity(BasisModel, data, steps, delta_t)
        self.plot_forces(BasisModel, data, steps, delta_t)
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/SimulationBasisData" + name + ".png", dpi=500)
        plt.savefig("Simulation/Graphiken/SimulationBasisData" + name + ".svg")



#################################################
# PlotDifferentWakes                            #
#################################################
class PlotDifferentWakes():
    def __init__(self):
        # settings
        self.Simulator = Simulation.Simulator()
        self.wake_times = [0.01, 0.1, 1, 10, 100, 1000]
        self.simulations = []
        self.Rotor = Parameters.Rotor()
        self.Rotor.steps = 2000
        self.Rotor.delta_t = 0.001
        self.Rotor.incidence_angle = np.full(self.Rotor.steps, 0)           # incidence angle [deg]
        self.Rotor.tension = np.full(self.Rotor.steps, 0)                   # tension [N]
        self.Rotor.sigma = 10
        self.Rotor.U = np.column_stack((self.Rotor.incidence_angle, self.Rotor.tension))

        # create plot
        self.generate_simulation_data()
        self.create_plot()

    def generate_simulation_data(self):
        for t in self.wake_times:
            self.Rotor.T = t
            Model = ReferenceModel.Rotor(self.Rotor)
            self.simulations.append(self.Simulator.simulate_explicit_reference_model(Model, self.Rotor.x0, self.Rotor.U, self.Rotor.delta_t))

    def plot_hight(self):
        plt.subplot(2, 3, 1)
        for i, data in enumerate(self.simulations):
            plt.plot(data[2], data[1].T[0], "b", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15 * (i+1))
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Höhe $z$ in m", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.Rotor.steps * self.Rotor.delta_t)
        plt.legend(loc=1)

    def plot_vertical_velocity(self):
        plt.subplot(2, 3, 2)
        for i, data in enumerate(self.simulations):
            plt.plot(data[2], data[1].T[1], "b", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15 * (i+1))
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("vertikale Geschwindigkeit $\dot{z}$ in m/s", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.Rotor.steps * self.Rotor.delta_t)
        plt.legend(loc=1)

    def plot_rotational_speed(self):
        plt.subplot(2, 3, 3)
        for i, data in enumerate(self.simulations):
            plt.plot(data[2], data[1].T[2], "b", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15 * (i+1))
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Rotationsgeschwindigkeit $\Omega$ in U/s", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.Rotor.steps * self.Rotor.delta_t)
        plt.legend(loc=1)

    def plot_average_force(self):
        plt.subplot(2, 3, 4)
        for i, data in enumerate(self.simulations):
            plt.plot(data[2], data[1].T[3], "b", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15 * (i+1))
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Mittlere Kraft $\\bar{F}$ in N", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.Rotor.steps * self.Rotor.delta_t)
        plt.legend(loc=1)

    def plot_energy(self):
        plt.subplot(2, 3, 5)
        for i, data in enumerate(self.simulations):
            Model = ReferenceModel.Rotor(self.Rotor)
            kinetic_energy = np.abs(1/2 * self.Rotor.total_mass * np.square(data[1].T[1]))
            potential_energy = self.Rotor.total_mass * self.Rotor.gravitation * data[1].T[0]
            rotational_energy = 1/2 * Model.inertial_moment * np.square(data[1].T[2])
            total_energy = kinetic_energy + potential_energy + rotational_energy
            plt.plot(data[2], total_energy, color="b", linestyle="solid", label="$E_\mathrm{tot}$ mit $T$ = " + str(self.wake_times[i]) + " s", alpha=0.15*(i+1))

        plt.legend(loc=1)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.Rotor.steps * self.Rotor.delta_t)
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Energie $E$ in J", fontsize=10)

    def plot_power(self):
        plt.subplot(2, 3, 6)
        for i, data in enumerate(self.simulations):
            Model = ReferenceModel.Rotor(self.Rotor)
            gravitational_power = self.Rotor.gravitational_force * data[1].T[1][:-1]
            tension_power = self.Rotor.tension * data[1].T[1][:-1]
            vertical_power = (self.Rotor.total_mass * np.diff(data[1].T[1]) / self.Rotor.delta_t) * data[1].T[1][:-1]
            tangential_power = np.diff(data[1].T[2]) * np.diff(data[1].T[2]) * Model.inertial_moment / self.Rotor.delta_t
            total_power = gravitational_power + tension_power + vertical_power + tangential_power
            plt.plot(data[2][:-1], total_power, color="b", linestyle="solid", label="$E_\mathrm{tot}$ mit $T$ = " + str(self.wake_times[i]) + " s", alpha=0.15*(i+1))
    
        plt.legend(loc=1)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.Rotor.steps * self.Rotor.delta_t)
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Leistung $P$ in W", fontsize=10)

    def create_plot(self):
        plt.figure(figsize=(12, 6))
        self.plot_hight()
        self.plot_vertical_velocity()
        self.plot_rotational_speed()
        self.plot_average_force()
        self.plot_energy()
        self.plot_power()
        plt.tight_layout()
        plt.savefig("Simulation/Plots/SimulationDifferentWakes.png", dpi=500)
        plt.savefig("Simulation/Plots/SimulationDifferentWakes.svg")



#################################################
# OptimizeBasisModelParameters                  #
#################################################
class OptimizeBasisModelParameters:
    def __init__(self):
        self.determine_optimal_rotor_parameters()

    def optimize_rotor_parameters(self, estimated_parameters):
        print(estimated_parameters)

        # create parameter object
        Rotor = Parameters.Rotor()
        Rotor.estimated_lift_slope = estimated_parameters[0]
        Rotor.estimated_drag_slope = estimated_parameters[1]
        Rotor.estimated_zero_lift_coefficient = estimated_parameters[2]
        Rotor.estimated_zero_drag_coefficient = estimated_parameters[3]

        # set system input
        Rotor.steps = 1000
        Rotor.sigma = 10
        Rotor.incidence_angle = np.full(Rotor.steps, 0)
        Rotor.tension = np.full(Rotor.steps, 0)

        Rotor.incidence_angle = gaussian_filter1d(Rotor.incidence_angle, Rotor.sigma)
        Rotor.tension = gaussian_filter1d(Rotor.tension, Rotor.sigma)
        Rotor.U = np.column_stack((Rotor.incidence_angle, Rotor.tension))

        # create models
        MyBasisModel = BasisModel.Rotor(Rotor)
        MyReferenceModel = ReferenceModel.Rotor(Rotor)

        # simulate models
        Simulator = Simulation.Simulator()
        basis_model = Simulator.simulate_explicit_simple_model(MyBasisModel, Rotor.x0, Rotor.U, Rotor.delta_t)[0]
        reference_model = Simulator.simulate_explicit_reference_model(MyReferenceModel, Rotor.x0, Rotor.U, Rotor.delta_t)[0]

        # determine mean squared error
        if np.any(np.isnan(basis_model)) or np.any(np.isnan(reference_model)):
            print(np.inf)
            return np.inf
        else:
            print(mean_squared_error(reference_model, basis_model))
            return mean_squared_error(reference_model, basis_model)


    def determine_optimal_rotor_parameters(self):
        Rotor = Parameters.Rotor()
        return sci.minimize(self.optimize_rotor_parameters, Rotor.initial_estimation, bounds=[(0, np.inf)] * len(Rotor.initial_estimation))



#################################################
# Heatmaps                                      #
#################################################
class Heatmaps:
    def __init__(self):
        self.vertical_speeds = np.linspace(-10, 10, 10)
        self.rotation_speeds = np.linspace(0.1, 10, 10)
        self.incidence_angles = np.linspace(-20, 0, 3)
        self.tensions = np.linspace(0, 1000, 3)
        self.average_forces = np.linspace(0, 20, 3)
        self.initial_height = 5
        self.Simulator = Simulation.Simulator()
        self.Rotor = Parameters.Rotor()
        MyBasisModel = BasisModel.Rotor(self.Rotor)
        MyReferenceModel = ReferenceModel.Rotor(self.Rotor)
        MyNeuralNetwork = NeuralNetwork.Model(self.Rotor, MyReferenceModel)
        self.plot_basis_model_heatmap(MyBasisModel, MyReferenceModel)
        self.plot_neural_network_heatmap(MyNeuralNetwork, MyReferenceModel)


    def plot_basis_model_heatmap(self, BasisModel, ReferenceModel):
        plt.figure(figsize=(7, 5))
        vertical_speeds, rotation_speeds = np.meshgrid(self.vertical_speeds, self.rotation_speeds)
        losses = np.vectorize(self.calculate_loss)(BasisModel.system_differential_equation, ReferenceModel.system_differential_equation, vertical_speeds, rotation_speeds)
        plt.pcolormesh(vertical_speeds, rotation_speeds, losses, cmap="coolwarm")
        plt.colorbar()
        plt.xlabel('vertikale Geschwindigkeit $\dot{z}$ in m/s', fontsize=10)
        plt.ylabel('Rotationsgeschwindigkeit $\Omega$ in U/s', fontsize=10)
        plt.tight_layout()
        plt.savefig("Simulation/Plots/BasisModelHeatMap.png", dpi=500)
        plt.savefig("Simulation/Plots/BasisModelHeatMap.svg")


    def plot_neural_network_heatmap(self, NeuralNetwork, ReferenceModel):
        plt.figure(figsize=(7, 5))
        vertical_speeds, rotation_speeds = np.meshgrid(self.vertical_speeds, self.rotation_speeds)
        losses = np.vectorize(self.calculate_loss)(NeuralNetwork.predict, ReferenceModel.system_differential_equation, vertical_speeds, rotation_speeds)
        plt.pcolormesh(vertical_speeds, rotation_speeds, losses, cmap="coolwarm")
        plt.colorbar()
        plt.xlabel('vertikale Geschwindigkeit $\dot{z}$ in m/s', fontsize=10)
        plt.ylabel('Rotationsgeschwindigkeit $\Omega$ in U/s', fontsize=10)
        plt.tight_layout()
        plt.savefig("Simulation/Plots/NeuralNetworkHeatMap.png", dpi=500)
        plt.savefig("Simulation/Plots/NeuralNetworkHeatMap.svg")


    def calculate_loss(self, get_prediction, get_ground_truth, vertical_speed, rotation_speed):
        losses = []

        for angle in self.incidence_angles:
            for tension in self.tensions:
                for force in self.average_forces:
                    x = np.array([self.initial_height, vertical_speed, rotation_speed, force])
                    u = np.array([angle, tension])
                    for _ in range(3):
                        prediction = get_prediction(x, u)
                        ground_truth = get_ground_truth(x, u)
                    losses.append(mean_absolute_error(prediction, ground_truth))

        return np.mean(losses)



#################################################
# LearningCurve                                 #
#################################################
class LearningCurve:
    def __init__(self):
        self.Simulator = Simulation.Simulator()
        self.Rotor = Parameters.Rotor()
        self.BasisModel = BasisModel.Rotor(self.Rotor)
        self.ReferenceModel = ReferenceModel.Rotor(self.Rotor)
        self.NeuralNetwork = NeuralNetwork.Model(self.Rotor, self.ReferenceModel)
        self.plot_neural_network_learning_curve(self.NeuralNetwork.generate_file_name(self.Rotor.number_nodes, self.Rotor.activation_function, self.Rotor.number_layers, self.Rotor.number_epochs, self.Rotor.learning_rate, self.Rotor.number_wings, self.Rotor.wing_area, self.Rotor.wing_wide, self.Rotor.air_density)[:-2]+"txt")


    def plot_neural_network_learning_curve(self, data_file_name):
        # generate training data
        training_data = np.loadtxt(data_file_name, delimiter=",")
        XU = training_data[:, :6]
        dX = training_data[:, 6:]
        X_train, X_test, y_train, y_test = train_test_split(XU, dX, test_size=0.2, random_state=42)

        # calculate loss for different trainings set sizes
        losses = pd.DataFrame(columns=["set_size", "train_loss", "test_loss"])
        
        for end in np.linspace(len(X_train)*0.1, len(X_train), 10).astype(int):
            self.NeuralNetwork.fit_model(X_train[:end], y_train[:end], self.Rotor.number_epochs, self.Rotor.learning_rate)
            train_loss, test_loss = self.NeuralNetwork.evaluate_model(X_train[:end], y_train[:end], X_test, y_test)
            losses.loc[len(losses)] = [end, train_loss, test_loss]
        
        # create plot
        plt.figure()
        print(len(losses["set_size"]))
        print(len(losses["train_loss"]))
        print(len(losses["test_loss"]))

        plt.plot(losses["set_size"], losses["train_loss"], "gx", label="Training")
        plt.plot(losses["set_size"], losses["test_loss"], "bx", label="Test")
        plt.xlabel("Anzahl an Trainingsszenarien")
        plt.ylabel("Mean Squared Error")
        plt.legend()
        plt.tight_layout()
        plt.savefig("Simulation/Plots/NeuralNetworkLearningCurve.png", dpi=500)
        plt.savefig("Simulation/Plots/NeuralNetworkLearningCurve.svg")




#################################################
# Hyperparameter                                #
#################################################
class Hyperparameter:
    def __init__(self):
        self.Simulator = Simulation.Simulator()
        self.Rotor = Parameters.Rotor()
        self.ReferenceModel = ReferenceModel.Rotor(self.Rotor)
        self.NeuralNetwork = NeuralNetwork.Model(self.Rotor, self.ReferenceModel)

        self.layers = np.array([1, 2, 3])
        self.nodes = np.array([4, 16, 64, 256])
        self.activation_functions = np.array(["ReLU"])
        self.epochs = np.array([10, 100, 500])
        self.learning_rates = np.array([1e-2, 1e-3, 1e-4])

        file_name = self.NeuralNetwork.generate_file_name(self.Rotor.number_nodes, self.Rotor.activation_function, self.Rotor.number_layers, self.Rotor.number_epochs, self.Rotor.learning_rate, self.Rotor.number_wings, self.Rotor.wing_area, self.Rotor.wing_wide, self.Rotor.air_density)[:-2]+"txt"
        self.grid_search_hyperparameters(file_name)


    def grid_search_hyperparameters(self, data_file_name):
        # generate training data
        training_data = np.loadtxt(data_file_name, delimiter=",")
        XU = training_data[:, :6]
        dX = training_data[:, 6:]
        X_train, X_test, y_train, y_test = train_test_split(XU, dX, test_size=0.2, random_state=42)
        
        # data
        hyperparameter = pd.DataFrame({
            "LearningRate": [],
            "NumberLayer": [],
            "NumberNodes": [],
            "ActivationFunction": [],
            "NumberEpochs": [],
            "Loss": []
        })

        # grid search
        for layers in self.layers:
            for nodes in self.nodes:
                for activation_function in self.activation_functions:
                    for epochs in self.epochs:
                        for learning_rate in self.learning_rates:
                            neural_network_model = NeuralNetwork.Model(self.Rotor, self.ReferenceModel)
                            neural_network_model.create_model(nodes, activation_function, layers)
                            neural_network_model.fit_model(X_train, y_train, epochs, learning_rate)
                            loss = neural_network_model.evaluate_model(X_train, y_train, X_test, y_test)[1]
                            new = {"NumberLayer": layers, "NumberNodes": nodes, "ActivationFunction": activation_function, "NumberEpochs": epochs, "LearningRate": learning_rate, "Loss": loss}
                            hyperparameter = hyperparameter.append(new, ignore_index=True)

        hyperparameter.to_csv("Simulation/Daten/Hyperparameter.txt", index=False)
        return hyperparameter


    def plot_grid_search_data(self):
        pass


    def print_best_hyperparameters(self):
        pass



class EvaluateModelQuality:
    def __init__(self, steps=500, delta_t=0.01, incidence_angle=np.full(500, 0), tension=np.full(500, 0)):
        # create models
        self.Simulator = Simulation.Simulator()
        self.Rotor = Parameters.Rotor()
        self.BasisModel = BasisModel.Rotor(self.Rotor)
        self.ReferenceModel = ReferenceModel.Rotor(self.Rotor)
        self.NeuralNetwork = NeuralNetwork.Model(self.Rotor, self.ReferenceModel)

        # set realistic input and initial states
        self.Rotor.steps = steps
        self.Rotor.delta_t = delta_t
        self.Rotor.incidence_angle = incidence_angle
        self.Rotor.tension = tension
        self.Rotor.U = np.column_stack((self.Rotor.incidence_angle, self.Rotor.tension))


    def generate_basis_data(self):
        pass


    def generate_reference_data(self):
        pass


    def generate_neural_network_data(self):
        pass


    def calculate_r_squared(self, actual_values, predicted_values):
        return r2_score(actual_values, predicted_values)



class BestInput:
    def __init__(self):
        self.Simulator = Simulation.Simulator()
        self.MyParameter = Parameters.Rotor()
        self.MyParameter.steps = 301
        self.MyParameter.incidence_angle = np.full(self.MyParameter.steps, 0)
        self.MyParameter.tension = np.full(self.MyParameter.steps, 0)  
        self.MyParameter.sigma = 10
        best_angle, best_tension, best_time = self.first_pumping_phase()
        self.second_pumping_phase(best_angle, best_tension, int(best_time))


    def first_pumping_phase(self):
        # data
        first_phase_data = pd.DataFrame({"first_angle": [], "first_tension": [], "first_time": [], "energy": []})

        # search
        for a1 in np.round(np.linspace(-15, 0, 5)).astype(int):
            for t1 in np.round(np.linspace(0, 300, 5)).astype(int):
                for dt1 in np.round(np.linspace(50, 300, 5)).astype(int):
                    self.Simulator = Simulation.Simulator()
                    self.MyParameter = Parameters.Rotor()
                    self.MyParameter.steps = 301
                    self.MyParameter.incidence_angle = np.full(self.MyParameter.steps, 0)
                    self.MyParameter.tension = np.full(self.MyParameter.steps, 0)  
                    self.MyParameter.sigma = 10
                    self.MyParameter.incidence_angle[0:dt1] = a1
                    self.MyParameter.tension[0:dt1] = t1
                    self.MyParameter.incidence_angle = gaussian_filter1d(self.MyParameter.incidence_angle, self.MyParameter.sigma)
                    self.MyParameter.tension = gaussian_filter1d(self.MyParameter.tension, self.MyParameter.sigma)
                    self.MyParameter.U = np.column_stack((self.MyParameter.incidence_angle, self.MyParameter.tension))
                    self.MyReferenceModel = ReferenceModel.Rotor(self.MyParameter)
                    data_first_pumping_phase = self.Simulator.simulate_explicit_reference_model(self.MyReferenceModel, self.MyParameter.x0, self.MyParameter.U, self.MyParameter.delta_t)[1]
                    delta_total_energy = self.return_delta_total_energy(data_first_pumping_phase)
                    new = {"first_angle": a1, "first_tension": t1, "first_time": dt1, "energy": delta_total_energy}
                    first_phase_data = first_phase_data.append(new, ignore_index=True)
            
        # save data
        first_phase_data.to_csv("Simulation/Daten/InputFirstPumpingPhase.txt", index=False)

        # determine optimal input
        index_best_input = first_phase_data["energy"].idxmax()
        best_input = first_phase_data.loc[index_best_input]
        print(best_input)
        return [first_phase_data.loc[index_best_input]["first_angle"], first_phase_data.loc[index_best_input]["first_tension"], first_phase_data.loc[index_best_input]["first_time"]]



    def return_delta_total_energy(self, data_first_pumping_phase):
        initial_potential_energy = self.MyParameter.total_mass * self.MyParameter.gravitation * data_first_pumping_phase.T[0][0]
        initial_kinetic_energy = -(1/2 * self.MyParameter.total_mass * np.square(data_first_pumping_phase.T[1]))[0]
        initial_rotational_energy = 1/2 * self.MyReferenceModel.inertial_moment * np.square(data_first_pumping_phase.T[2])[0]

        
        potential_energy = self.MyParameter.total_mass * self.MyParameter.gravitation * data_first_pumping_phase.T[0][-1]
        kinetic_energy = -(1/2 * self.MyParameter.total_mass * np.square(data_first_pumping_phase.T[1]))[-1]
        rotational_energy = 1/2 * self.MyReferenceModel.inertial_moment * np.square(data_first_pumping_phase.T[2])[-1]
        
        total_energy = kinetic_energy + potential_energy + rotational_energy - initial_kinetic_energy - initial_potential_energy - initial_rotational_energy
        return total_energy

        

    def second_pumping_phase(self, first_phase_angle, first_phase_tension, first_phase_time):
        # data
        second_phase_data = pd.DataFrame({"second_angle": [], "second_time": [], "height": []})

        # search
        for second_phase_angle in np.round(np.linspace(-15, 0, 5)).astype(int):
            for second_phase_time in np.round(np.linspace(50, 800, 5)).astype(int):
                # prepare simulation
                self.Simulator = Simulation.Simulator()
                self.MyParameter = Parameters.Rotor()
                self.MyParameter.steps = 1101
                self.MyParameter.incidence_angle = np.full(self.MyParameter.steps, 0)
                self.MyParameter.tension = np.full(self.MyParameter.steps, 0) 
                self.MyParameter.sigma = 10
            
                # set first phase input
                self.MyParameter.incidence_angle[0:first_phase_time] = first_phase_angle
                self.MyParameter.tension[0:first_phase_time] = first_phase_tension

                # set second phase input
                self.MyParameter.incidence_angle[first_phase_time:first_phase_time+second_phase_time] = second_phase_angle

                # smooth input
                self.MyParameter.incidence_angle = gaussian_filter1d(self.MyParameter.incidence_angle, self.MyParameter.sigma)
                self.MyParameter.tension = gaussian_filter1d(self.MyParameter.tension, self.MyParameter.sigma)
                self.MyParameter.U = np.column_stack((self.MyParameter.incidence_angle, self.MyParameter.tension))
                
                # simulation and evaluation
                self.MyReferenceModel = ReferenceModel.Rotor(self.MyParameter)
                data_pumping = self.Simulator.simulate_explicit_reference_model(self.MyReferenceModel, self.MyParameter.x0, self.MyParameter.U, self.MyParameter.delta_t)
                height = self.return_height(data_pumping[1], first_phase_time+second_phase_time)
                new = {"second_angle": second_phase_angle, "second_time": second_phase_time, "height": height}
                second_phase_data = second_phase_data.append(new, ignore_index=True)
            
        # save data
        second_phase_data.to_csv("Simulation/Daten/InputSecondPumpingPhase.txt", index=False)

        # determine optimal input
        index_best_input = second_phase_data["height"].idxmax()
        best_input = second_phase_data.loc[index_best_input]

        print("*******************************************")
        print("first_angle: " + str(first_phase_angle))
        print("second_angle: " + str(best_input["second_angle"]))
        print("tension: " + str(first_phase_tension))
        print("first_time: " + str(first_phase_time))
        print("second_time: " + str(best_input["second_time"]))
        print("height: " + str(best_input["height"]))
        if best_input["height"] < self.MyParameter.initial_height:
            print("WARINING: PUMPING FAILED")
        else:
            print("PUMPING SUCCEEDED")
        print("*******************************************")


    def return_height(self, data_pumping, end):
        return data_pumping.T[0][end]



class ParameterFitReferenzModel:
    def __init__(self):
        # create objects
        self.Simulator = Simulation.Simulator()
        self.MyParameters = Parameters.Rotor()

        # measurement parameters
        self.measurement_delta_t = 0.25 # [s]
        self.measurement_data_time_duration = 2.25  # [s]
        self.measurement_data_path = "Videos/Height/HeightData.txt"

        # rotor parameters
        self.MyParameters.initial_height = 0  # [m]
        self.MyParameters.initial_vertical_speed = 0  # [m/s]
        self.MyParameters.initial_rotation_speed = 38  # [U/s]
        self.MyParameters.initial_average_force = 0  # [N]
        self.MyParameters.x0 = [self.MyParameters.initial_height, self.MyParameters.initial_vertical_speed, self.MyParameters.initial_rotation_speed, self.MyParameters.initial_average_force]
        self.rotor_blade_angle = -5
        self.MyParameters.rotor_hub_mass = 0.01                              # rotor hub mass [kg]    
        self.MyParameters.stick_mass = 0.001                                  # single stick mass [kg]
        self.MyParameters.wing_mass = 0.03                                   # single wing mass [kg]
        self.MyParameters.distance_rotor_hub = 0.025                          # distance to end of rotor hub [m]
        self.MyParameters.distance_wing_start = 0.005                        # distance to start of wing [m]
        self.MyParameters.distance_wing_end = 0.095                           # distance to end of wing [m]

        # measurement input
        self.MyParameters.steps = int(self.measurement_data_time_duration / self.MyParameters.delta_t)
        self.MyParameters.incidence_angle = np.full(self.MyParameters.steps, self.rotor_blade_angle)
        self.MyParameters.tension = np.full(self.MyParameters.steps, 0)
        self.MyParameters.incidence_angle = gaussian_filter1d(self.MyParameters.incidence_angle, self.MyParameters.sigma)
        self.MyParameters.tension = gaussian_filter1d(self.MyParameters.tension, self.MyParameters.sigma)
        self.MyParameters.U = np.column_stack((self.MyParameters.incidence_angle, self.MyParameters.tension))
        

    def load_measurement_data(self):
        data = np.loadtxt(self.measurement_data_path, skiprows=1)
        return data


    def calculate_error(self, estimated_parameters):
        # set estimated parameters
        print(estimated_parameters)
        self.MyParameters.real_lift_slope = estimated_parameters[0]
        self.MyParameters.real_zero_lift_coefficient = estimated_parameters[1]
        self.MyParameters.real_drag_parameter = estimated_parameters[2]
        self.MyParameters.real_zero_drag_coefficient = estimated_parameters[3]

        # create reference model with estimated parameters
        MyReferenceModel = ReferenceModel.Rotor(self.MyParameters)

        # simulate and adjust reference model
        data_reference_model = self.Simulator.simulate_explicit_reference_model(MyReferenceModel, self.MyParameters.x0, self.MyParameters.U, self.MyParameters.delta_t)[0].T[0]
        steps_size = int(self.measurement_delta_t / self.MyParameters.delta_t)
        data_reference_model = data_reference_model[::steps_size]

        # load measurement data
        data_measurement = self.load_measurement_data()

        # calculate mean squared error
        mse = mean_squared_error(data_reference_model, data_measurement)
        print(mse)
        print()
        return mse


    def determine_optimal_parameters(self, cla=0.1, cl0=0.2, cda=10, cd0=0.3):
        initial_estimation = np.array([cla, cl0, cda, cd0])
        result = sci.minimize(self.calculate_error, initial_estimation, bounds=[(0, np.inf)] * len(initial_estimation))
        return result

    
    def generate_plot(self):
        plt.figure()

        # plot measurement
        data_measurement = self.load_measurement_data()
        T = np.linspace(0, self.measurement_data_time_duration, int(self.measurement_data_time_duration//self.measurement_delta_t)+1)
        plt.plot(T, data_measurement)

        # plot simulation
        estimated_parameters = self.determine_optimal_parameters()
        self.MyParameters.real_lift_slope = estimated_parameters[0]
        self.MyParameters.real_zero_lift_coefficient = estimated_parameters[1]
        self.MyParameters.real_drag_parameter = estimated_parameters[2]
        self.MyParameters.real_zero_drag_coefficient = estimated_parameters[3]
        MyReferenceModel = ReferenceModel.Rotor(self.MyParameters)
        data_reference_model = self.Simulator.simulate_explicit_reference_model(MyReferenceModel, self.MyParameters.x0, self.MyParameters.U, self.MyParameters.delta_t)
        plt.plot(data_reference_model[2], data_reference_model[1].T[0], "b", label="Referenzmodell")

        plt.show()


# A = ParameterFitReferenzModel()
# A.generate_plot()




# validation: no air
#PlotSzenario(steps=1000, delta_t=0.001, incidence_angle=np.full(1000, 0), tension=np.full(1000, 0), air_density=1e-8, name="ValidationNoAir")
#PlotSzenario(steps=1000, delta_t=0.001, incidence_angle=np.full(1000, 0), tension=np.full(1000, 0), number_wings=1e-8, name="ValidationNoWings")
#PlotSzenario(steps=1000, delta_t=0.001, incidence_angle=np.full(1000, 0), tension=np.full(1000, 0), wing_area=1e-8, wing_wide=1e-8, name="ValidationSmallWingArea")
#PlotDifferentWakes()
#OptimizeBasisModelParameters()
#Heatmaps()
#LearningCurve()

#BestInput()



MyParameters = Parameters.Rotor()
MyParameters.initial_height = 0  # [m]
MyParameters.initial_vertical_speed = 0  # [m/s]
MyParameters.initial_rotation_speed = 2  # [U/s]
MyParameters.initial_average_force = 0  # [N]
MyParameters.x0 = [MyParameters.initial_height, MyParameters.initial_vertical_speed, MyParameters.initial_rotation_speed, MyParameters.initial_average_force]
rotor_blade_angle = -5
MyParameters.rotor_hub_mass = 0.01                              # rotor hub mass [kg]    
MyParameters.stick_mass = 0.001                                  # single stick mass [kg]
MyParameters.wing_mass = 0.03                                   # single wing mass [kg]
MyParameters.distance_rotor_hub = 0.025                          # distance to end of rotor hub [m]
MyParameters.distance_wing_start = 0.005                        # distance to start of wing [m]
MyParameters.distance_wing_end = 0.095                           # distance to end of wing [m]

# measurement input
MyParameters.steps = 300
MyParameters.delta_t = 0.01
MyParameters.incidence_angle = np.full(MyParameters.steps, rotor_blade_angle)
MyParameters.tension = np.full(MyParameters.steps, -20)
MyParameters.incidence_angle = gaussian_filter1d(MyParameters.incidence_angle, MyParameters.sigma)
MyParameters.tension = gaussian_filter1d(MyParameters.tension, MyParameters.sigma)
MyParameters.U = np.column_stack((MyParameters.incidence_angle, MyParameters.tension))

PlotSzenario(steps=MyParameters.steps, delta_t=MyParameters.delta_t, incidence_angle=MyParameters.incidence_angle, tension=MyParameters.tension, name="")
