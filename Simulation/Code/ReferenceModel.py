import numpy as np
import Parameters


class Rotor:
    def __init__(self, Parameters):
        # import parameters
        self.Parameters = Parameters

        # side calculations
        self.inertial_center = (np.power(self.Parameters.distance_rotor_hub, 3) / 3) * (self.Parameters.rotor_hub_mass / self.Parameters.distance_rotor_hub)
        self.inertial_stick = (np.power(self.Parameters.distance_wing_start, 3) / 3) * (self.Parameters.stick_mass / (self.Parameters.distance_wing_start - self.Parameters.distance_rotor_hub)) - (np.power(self.Parameters.distance_rotor_hub, 3) / 3) * (self.Parameters.rotor_hub_mass / self.Parameters.distance_rotor_hub)
        self.inertial_blade = (np.power(self.Parameters.distance_wing_end, 3) / 3) * (self.Parameters.wing_mass / (self.Parameters.distance_wing_end - self.Parameters.distance_wing_start)) - (np.power(self.Parameters.distance_wing_start, 3) / 3) * (self.Parameters.stick_mass / (self.Parameters.distance_wing_start - self.Parameters.distance_rotor_hub))
        self.inertial_moment = self.inertial_center + self.Parameters.number_wings * (self.inertial_stick + self.inertial_blade)
        self.r_max = 0.97 * self.Parameters.distance_wing_end
        self.rotor_area = np.pi * np.square(self.r_max)
                       
        # variables
        self.gamma = 0
        self.alpha = 0
        self.lift_coefficient = 0
        self.drag_coefficient = 0
        self.wind_velocity = 0
        self.infinitesimal_thrust = 0
        self.thrust = 0
        self.moment = 0

    
    def get_vertical_velocity(self, x, u):
        return x[1]


    def get_states(self, x, u):
        return x


    def get_lift_coefficient(self, x, u):
        # rad_gamma = np.deg2rad(self.gamma)
        # if 0 < self.gamma < np.rad2deg(np.pi / 8) or np.rad2deg(7 * np.pi / 8) < self.gamma < 180:
        #     self.lift_coefficient = self.Parameters.max_lift_coefficient * np.sin(6 * rad_gamma) + self.Parameters.zero_lift_coefficient
        # else:
        #     self.lift_coefficient = self.Parameters.max_lift_coefficient * np.sin(2 * rad_gamma) + self.Parameters.zero_lift_coefficient
        # return self.lift_coefficient
        self.lift_coefficient = self.Parameters.max_lift_coefficient * self.gamma + self.Parameters.zero_lift_coefficient
        return self.lift_coefficient


    def get_drag_coefficient(self, x, u):
        # rad_gamma = np.deg2rad(self.gamma)
        # self.drag_coefficient = self.Parameters.max_drag_coefficient * (1 - np.cos(2 * rad_gamma)) + self.Parameters.zero_drag_coefficient
        # return self.drag_coefficient
        self.drag_coefficient = np.square(self.gamma) * self.Parameters.max_drag_coefficient + self.Parameters.zero_drag_coefficient
        return self.drag_coefficient


    def get_wake_velocity_rotor(self, x, u):
        if x[3] > 0:
            numerator = x[3]
            coefficient = -1
        else:
            numerator = -x[3]
            coefficient = 1
        if self.Parameters.air_density < 1e-5:
            return 0
        denominator = 2 * self.Parameters.air_density * self.rotor_area
        wake_velocity_rotor = coefficient * np.sqrt(numerator / denominator)
        return wake_velocity_rotor


    def get_alpha(self, x, u, r):
        self.alpha = -np.rad2deg(np.arctan((x[1] - self.get_wake_velocity_rotor(x, u)) / (x[2] * r)))
        return self.alpha

    
    def get_wind_velocity(self, x, u, r):
        self.wind_velocity = np.sqrt(np.square(x[2] * r) + np.square(x[1] - self.get_wake_velocity_rotor(x, u)))
        return self.wind_velocity


    def get_gamma(self, x, u, r):
        self.gamma = self.alpha - u[0]
        return self.gamma


    def get_pulling_force(self, x, u):
        return u[1]


    def integral_thrust(self, r, delta_r):
        wind_velocity = self.get_wind_velocity(self.x, self.u, r)
        alpha = self.get_alpha(self.x, self.u, r)
        gamma = self.get_gamma(self.x, self.u, r)
        lift = self.get_lift_coefficient(gamma, self.u) * np.cos(np.deg2rad(alpha)) * np.square(wind_velocity) * self.Parameters.wing_wide * delta_r
        drag = self.get_drag_coefficient(gamma, self.u) * np.sin(np.deg2rad(alpha)) * np.square(wind_velocity) * self.Parameters.wing_wide * delta_r
        self.infinitesimal_thrust = lift + drag
        return self.infinitesimal_thrust


    def integral_moment(self, r, delta_r):
        wind_velocity = self.get_wind_velocity(self.x, self.u, r)
        alpha = self.get_alpha(self.x, self.u, r)
        gamma = self.get_gamma(self.x, self.u, r)
        lift = self.get_lift_coefficient(gamma, self.u) * np.sin(np.deg2rad(alpha)) * np.square(wind_velocity) * self.Parameters.wing_wide * delta_r
        drag = self.get_drag_coefficient(gamma, self.u) * np.cos(np.deg2rad(alpha)) * np.square(wind_velocity) * self.Parameters.wing_wide * delta_r
        self.moment = r * (lift - drag)
        return self.moment

    
    def my_integrator(self, func, r_start, r_stop, n_steps):
        result = 0
        step_size = (r_stop - r_start) / n_steps

        for i in range(n_steps+1):
            r = r_start + i * step_size
            result += func(r, step_size)

        return result

    
    def system_differential_equation(self, x, u):
        # update states, input and wake
        self.x = x
        self.u = u

        # side calculation
        self.thrust = 1/2 * self.Parameters.number_wings * self.Parameters.air_density * self.my_integrator(self.integral_thrust, self.Parameters.distance_wing_start, self.r_max, 100)
        self.moment = 1/2 * self.Parameters.number_wings * self.Parameters.air_density * self.my_integrator(self.integral_moment, self.Parameters.distance_wing_start, self.r_max, 100)
        average_force = x[3]
        instantaneous_force = self.thrust

        # calculate change of state
        delta_high = self.get_vertical_velocity(x, u)
        delta_vertical_speed = (self.thrust + self.Parameters.gravitational_force + self.get_pulling_force(x, u)) / self.Parameters.total_mass
        delta_rotation_speed = self.moment / self.inertial_moment
        delta_average_force = (instantaneous_force - average_force) / self.Parameters.T

        return np.array([delta_high, delta_vertical_speed, delta_rotation_speed, delta_average_force])
    