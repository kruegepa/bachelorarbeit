import numpy as np
from scipy.optimize import root


class Simulator:
    def __init__(self):
        pass


    def explicit_runge_kutta(self, ode, delta_t, x, u):
        k1 = ode(x, u)
        k2 = ode(x + delta_t * k1 / 2., u)
        k3 = ode(x + delta_t * k2 / 2., u)
        k4 = ode(x + delta_t * k3, u)
        return (x + delta_t * (k1 + 2*k2 + 2*k3 + k4) / 6.)


    def implicit_runge_kutta(self, ode, dae, delta_t, x, u):
        # Butcher tableaux
        a = np.array([[0.0, 0.0, 0.0, 0.0],
                      [0.5, 0.0, 0.0, 0.0],
                      [0.0, 0.5, 0.0, 0.0],
                      [0.0, 0.0, 1.0, 0.0]])
        b = np.array([1/6, 1/3, 1/3, 1/6])

        # system to solve
        def F(k):
            # reshape
            k10 = k[0:3]
            k20 = k[3:6]
            k30 = k[6:9]
            k40 = k[9:12]
            z10 = k[12]
            z20 = k[13]
            z30 = k[14]
            z40 = k[15]

            # equations
            s1 = x + delta_t*a[0,0]*k10 + delta_t*a[0,1]*k20 + delta_t*a[0,2]*k30 + delta_t*a[0,3]*k40
            k1 = ode(s1, u, z10)

            s2 = x + delta_t*a[1,0]*k1 + delta_t*a[1,1]*k20 + delta_t*a[1,2]*k30 + delta_t*a[1,3]*k40
            k2 = ode(s2, u, z20)

            s3 = x + delta_t*a[2,0]*k1 + delta_t*a[2,1]*k2 + delta_t*a[2,2]*k30 + delta_t*a[2,3]*k40
            k3 = ode(s3, u, z30)

            s4 = x + delta_t*a[3,0]*k1 + delta_t*a[3,1]*k2 + delta_t*a[3,2]*k3 + delta_t*a[3,3]*k40
            k4 = ode(s4, u, z40)

            z1 = dae(s1, u, z10)
            z2 = dae(s2, u, z20)
            z3 = dae(s3, u, z30)
            z4 = dae(s4, u, z40)

            # calculate loss 
            f1 = k10 - ode(x + delta_t*a[0,0]*k1 + delta_t*a[0,1]*k2 + delta_t*a[0,2]*k3 + delta_t*a[0,3]*k4, u, z1)
            f2 = k20 - ode(x + delta_t*a[1,0]*k1 + delta_t*a[1,1]*k2 + delta_t*a[1,2]*k3 + delta_t*a[1,3]*k4, u, z2)
            f3 = k30 - ode(x + delta_t*a[2,0]*k1 + delta_t*a[2,1]*k2 + delta_t*a[2,2]*k3 + delta_t*a[2,3]*k4, u, z3)
            f4 = k40 - ode(x + delta_t*a[3,0]*k1 + delta_t*a[3,1]*k2 + delta_t*a[3,2]*k3 + delta_t*a[3,3]*k4, u, z4)
            f5 = np.array([z1])
            f6 = np.array([z2])
            f7 = np.array([z3])
            f8 = np.array([z4])

            return np.concatenate([f1, f2, f3, f4, f5, f6, f7, f8])

        # initial guess
        k0 = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

        # solve system of equations
        res = root(F, k0)
        k = res.x

        # reshape
        k1 = k[0:3]
        k2 = k[3:6]
        k3 = k[6:9]
        k4 = k[9:12]

        # calculate next state
        return x + delta_t * (b[0]*k1 + b[1]*k2 + b[2]*k3 + b[3]*k4)


    def explicit_simulation(self, ode, x_0, U, delta_t, output_function):
        x = x_0
        X = [x_0]
        Y = [output_function(x_0, U[0:,])] 

        for u in U:
            x = self.explicit_runge_kutta(ode, delta_t, x, u)
            if np.any(np.isnan(x)):
                print("!!!!!")
            X.append(x)
            Y.append(output_function(x, u))

        T = np.linspace(0, delta_t * U.shape[0], U.shape[0] + 1)
        return(np.array(X), np.array(Y), T)


    def implicit_simulation(self, ode, dae, x_0, U, delta_t, output_function):
        x = np.array(x_0)
        X = [x_0]
        wake = 0
        Y = [output_function(x_0, U[0:,])] 

        for u in U:
            x = self.implicit_runge_kutta(ode, dae, delta_t, x, u)
            X.append(x)
            Y.append(output_function(x, u))

        T = np.linspace(0, delta_t * U.shape[0], U.shape[0] + 1)
        return(np.array(X), np.array(Y), T)


    def simulate_implicit_simple_model(self, SimpleModel, x_0, U, delta_t):
        ode = SimpleModel.system_differential_equation
        dae = lambda x: 0
        output_function = SimpleModel.get_states
        return self.implicit_simulation(ode, dae, x_0, U, delta_t, output_function)


    def simulate_implicit_reference_model(self, ReferenceModel, x_0, U, delta_t):
        ode = ReferenceModel.system_differential_equation
        dae = ReferenceModel.algebraic_equation
        output_function = ReferenceModel.get_states
        return self.implicit_simulation(ode, dae, x_0, U, delta_t, output_function)


    def simulate_implicit_neuronal_network_model(self, NeuronalNetwork, x_0, U, delta_t):
        ode = NeuronalNetwork.predict
        output_function = lambda x, u: x
        return self.implicit_simulation(ode, x_0, U, delta_t, output_function)


    def simulate_explicit_simple_model(self, SimpleModel, x_0, U, delta_t):
        ode = SimpleModel.system_differential_equation
        output_function = SimpleModel.get_states
        return self.explicit_simulation(ode, x_0, U, delta_t, output_function)


    def simulate_explicit_reference_model(self, ReferenceModel, x_0, U, delta_t):
        ode = ReferenceModel.system_differential_equation
        output_function = ReferenceModel.get_states
        return self.explicit_simulation(ode, x_0, U, delta_t, output_function)


    def simulate_explicit_neuronal_network_model(self, NeuronalNetwork, x_0, U, delta_t):
        ode = NeuronalNetwork.predict
        output_function = lambda x, u: x
        return self.explicit_simulation(ode, x_0, U, delta_t, output_function)


    def create_training_data(self, ReferenceModel, n, file_name):
        XU = np.empty((0, 6))
        dX = np.empty((0, 4))

        for _ in range(n):
            # generate state
            height = np.random.randint(-10, 10)
            vertical_velocity = np.random.randint(-10, 10)
            rotation_speed = np.random.randint(1, 100) / 10
            average_force = np.random.randint(-300, 300)
            x = np.array([height, vertical_velocity, rotation_speed, average_force])
            
            # generate input
            input_angle = np.random.randint(-20, 0)
            input_tension = np.random.randint(0, 500)
            u = np.array([input_angle, input_tension])

            # determine ground truth and add noise
            dx = ReferenceModel.system_differential_equation(x, u)
            noise_std = 0.1
            noise = np.random.normal(loc=0, scale=noise_std, size=dx.shape)
            dx_with_noise = dx + noise

            # add data
            xu = np.array([height, vertical_velocity, rotation_speed, average_force, input_angle, input_tension])
            XU = np.vstack((XU, xu))
            dX = np.vstack((dX, dx_with_noise))

        np.savetxt(file_name, np.hstack((XU, dX)), delimiter=",")
        

    def get_training_data(self, file_name):
        data = np.loadtxt(file_name, delimiter=",")
        XU = data[:, :6]
        dX = data[:, 6:]
        return [XU, dX]


    def simulate_simple_dae(self, x_0, U, delta_t):
        ode = lambda x, u, z: np.array([z, 0, 0])
        dae = lambda x, u, z: z - 1
        out = lambda x, u: x
        return self.implicit_simulation(ode, dae, x_0, U, delta_t, out)


Sim = Simulator()
Sim.simulate_simple_dae(np.array([4, 0, 0]), np.zeros(10), 1)