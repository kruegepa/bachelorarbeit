# imports
import numpy as np

class MediumRotorParameters:
    def __init__(self):
        # constances
        self.gravitation = -9.81                        # gravitation constant [m/s^2]
        self.air_density = 1.225                        # air density [kg/m^3]

        # easily determinable parameters
        self.rotor_hub_mass = 0.4                       # rotor hub mass [kg]    
        self.stick_mass = 0.02                          # single stick mass [kg]
        self.wing_mass = 0.2                            # single wing mass [kg]
        self.number_wings = 3                           # number of wings
        self.wing_area = 0.25                           # area of one wing [m^2]
        self.distance_rotor_hub = 0.2                   # distance to end of rotor hub [m]
        self.distance_wing_start = 0.25                 # distance to start of wing [m]
        self.distance_wing_end = 1.1                    # distance to end of wing [m]

        # difficultly determinable parameters
        self.max_lift_coefficient = 0.11                 # real lift slope
        self.max_drag_coefficient = 0.00015                 # real drag slope
        self.zero_lift_coefficient = 0                  # real zero lift coefficient
        self.zero_drag_coefficient = 0.005               # real zero drag coefficient
        self.estimated_lift_slope = 0.11                # estimated lift slope
        self.estimated_drag_parameter = 0.00015              # estimated drag slope
        self.estimated_zero_lift_coefficient = 0    # estimated zero lift coefficient
        self.estimated_zero_drag_coefficient = 0.005    # estimated zero drag coefficient
        self.T = 1e-1                                    # wake time coefficient [s]

        # side calculations
        self.wing_wide = self.wing_area / (self.distance_wing_end - self.distance_wing_start) 
        self.total_mass = self.rotor_hub_mass + self.number_wings * self.stick_mass + self.number_wings * self.wing_mass
        self.initial_estimation = [self.max_lift_coefficient, self.zero_lift_coefficient, self.max_drag_coefficient, self.zero_drag_coefficient]
        self.gravitational_force = self.total_mass * self.gravitation
        self.half_radius = self.distance_wing_end / 2


class SmallRotorParameters:
    def __init__(self):
        # constances
        self.gravitation = -9.81                        # gravitation constant [m/s^2]
        self.air_density = 1.225                        # air density [kg/m^3]

        # easily determinable parameters
        self.rotor_hub_mass = 0.001                     # rotor hub mass [kg] 
        self.stick_mass = 0                             # single stick mass [kg]
        self.wing_mass = 0.025                          # single wing mass [kg]
        self.number_wings = 3                           # number of wings
        self.wing_area = 0.0036                         # area of one wing [m^2]
        self.distance_rotor_hub = 0.025                 # distance to end of rotor hub [m]
        self.distance_wing_start = 0.0251               # distance to start of wing [m]
        self.distance_wing_end = 0.12                   # distance to end of wing [m]

        # difficultly determinable parameters
        self.max_lift_coefficient = 0.81                # real lift slope
        self.max_drag_coefficient = 0.00015             # real drag slope
        self.zero_lift_coefficient = 0                  # real zero lift coefficient
        self.zero_drag_coefficient = 0.005              # real zero drag coefficient
        self.estimated_lift_slope = 0.03                # estimated lift slope
        self.estimated_drag_parameter = 20              # estimated drag slope
        self.estimated_zero_lift_coefficient = 1.953    # estimated zero lift coefficient
        self.estimated_zero_drag_coefficient = 0.375    # estimated zero drag coefficient
        self.T = 1e-2                                   # wake time coefficient [s]

        # side calculations
        self.wing_wide = self.wing_area / (self.distance_wing_end - self.distance_wing_start) 
        self.total_mass = self.rotor_hub_mass + self.number_wings * self.stick_mass + self.number_wings * self.wing_mass
        self.initial_estimation = [self.estimated_lift_slope, self.estimated_drag_parameter, self.estimated_zero_lift_coefficient, self.estimated_zero_drag_coefficient]
        self.gravitational_force = self.total_mass * self.gravitation
        self.half_radius = self.distance_wing_end / 2


class MediumRotorNoWingsParameters:
    def __init__(self):
        # constances
        self.gravitation = -9.81                        # gravitation constant [m/s^2]
        self.air_density = 1.225                        # air density [kg/m^3]

        # easily determinable parameters
        self.rotor_hub_mass = 0.4                       # rotor hub mass [kg]    
        self.stick_mass = 0.02                          # single stick mass [kg]
        self.wing_mass = 0.2                            # single wing mass [kg]
        self.number_wings = 1e-10                       # number of wings
        self.wing_area = 0.25                           # area of one wing [m^2]
        self.distance_rotor_hub = 0.2                   # distance to end of rotor hub [m]
        self.distance_wing_start = 0.25                 # distance to start of wing [m]
        self.distance_wing_end = 1.1                    # distance to end of wing [m]

        # difficultly determinable parameters
        self.max_lift_coefficient = 0.11                # real lift slope
        self.max_drag_coefficient = 0.00015             # real drag slope
        self.zero_lift_coefficient = 0                  # real zero lift coefficient
        self.zero_drag_coefficient = 0.005              # real zero drag coefficient
        self.estimated_lift_slope = 0.03                # estimated lift slope
        self.estimated_drag_parameter = 20              # estimated drag slope
        self.estimated_zero_lift_coefficient = 1.953    # estimated zero lift coefficient
        self.estimated_zero_drag_coefficient = 0.375    # estimated zero drag coefficient
        self.T = 1e-1                                   # wake time coefficient [s]

        # side calculations
        self.wing_wide = self.wing_area / (self.distance_wing_end - self.distance_wing_start) 
        self.total_mass = self.rotor_hub_mass + self.number_wings * self.stick_mass + self.number_wings * self.wing_mass
        self.initial_estimation = [self.estimated_lift_slope, self.estimated_drag_parameter, self.estimated_zero_lift_coefficient, self.estimated_zero_drag_coefficient]
        self.gravitational_force = self.total_mass * self.gravitation
        self.half_radius = self.distance_wing_end / 2



class MediumRotorNoAirParameters:
    def __init__(self):
        # constances
        self.gravitation = -9.81                        # gravitation constant [m/s^2]
        self.air_density = 1e-10                        # air density [kg/m^3]

        # easily determinable parameters
        self.rotor_hub_mass = 0.4                       # rotor hub mass [kg]    
        self.stick_mass = 0.02                          # single stick mass [kg]
        self.wing_mass = 0.2                            # single wing mass [kg]
        self.number_wings = 3                           # number of wings
        self.wing_area = 0.25                           # area of one wing [m^2]
        self.distance_rotor_hub = 0.2                   # distance to end of rotor hub [m]
        self.distance_wing_start = 0.25                 # distance to start of wing [m]
        self.distance_wing_end = 1.1                    # distance to end of wing [m]

        # difficultly determinable parameters
        self.max_lift_coefficient = 0.11                # real lift slope
        self.max_drag_coefficient = 0.00015             # real drag slope
        self.zero_lift_coefficient = 0                  # real zero lift coefficient
        self.zero_drag_coefficient = 0.005              # real zero drag coefficient
        self.estimated_lift_slope = 0.03                # estimated lift slope
        self.estimated_drag_parameter = 20              # estimated drag slope
        self.estimated_zero_lift_coefficient = 1.953    # estimated zero lift coefficient
        self.estimated_zero_drag_coefficient = 0.375    # estimated zero drag coefficient
        self.T = 1e-1                                   # wake time coefficient [s]

        # side calculations
        self.wing_wide = self.wing_area / (self.distance_wing_end - self.distance_wing_start) 
        self.total_mass = self.rotor_hub_mass + self.number_wings * self.stick_mass + self.number_wings * self.wing_mass
        self.initial_estimation = [self.estimated_lift_slope, self.estimated_drag_parameter, self.estimated_zero_lift_coefficient, self.estimated_zero_drag_coefficient]
        self.gravitational_force = self.total_mass * self.gravitation
        self.half_radius = self.distance_wing_end / 2


class SimulutionScenario:
    def __init__(self):
        self.delta_t = 0.01                            # time between to simulation steps
        self.steps = 2000                               # number of simulation steps
        self.incidence_angle = np.full(self.steps, -10)   # incidence angle [deg]
        self.tension = np.full(self.steps, 0)           # tension [N]
        self.tension[1000:1500] = -100
        self.sigma = 2                                  # standard deviation of smoothing
        self.initial_height = 0                         # initial hight [m]
        self.initial_vertical_speed = 0                 # initial vertical velocity [m/s]
        self.initial_rotation_speed = 100                 # initial rotation speed [r/s]
        self.initial_average_force = 0                  # initial average force [N]
        self.U = np.column_stack((self.incidence_angle, self.tension))
        self.x0 = [self.initial_height, self.initial_vertical_speed, self.initial_rotation_speed, self.initial_average_force]


class SimulutionScenarioPumpingModels:
    def __init__(self):
        self.delta_t = 0.001                            # time between to simulation steps
        self.steps = 500                               # number of simulation steps
        self.U = np.transpose(np.load("datau.npy"))
        x = np.transpose(np.load("datax.npy"))
        self.x0 = np.array([x[0][0], x[0][1], x[0][2], x[0][3]])


class SimulutionValidationScenario:
    def __init__(self):
        self.delta_t = 0.01    #0.01                        # time between to simulation steps
        self.steps = 200   #200                             # number of simulation steps
        self.incidence_angle = np.full(self.steps, 0)   # incidence angle [deg]
        self.tension = np.full(self.steps, 0)           # tension [N]
        self.sigma = 2                                  # standard deviation of smoothing
        self.initial_height = 0                         # initial hight [m]
        self.initial_vertical_speed = 0                 # initial vertical velocity [m/s]
        self.initial_rotation_speed = 2                 # initial rotation speed [r/s]
        self.initial_average_force = 0                  # initial average force [N]
        self.U = np.column_stack((self.incidence_angle, self.tension))
        self.x0 = [self.initial_height, self.initial_vertical_speed, self.initial_rotation_speed, self.initial_average_force]


class SimulutionScenarioDifferentWakes:
    def __init__(self):
        self.delta_t = 0.01                             # time between to simulation steps
        self.steps = 200                                # number of simulation steps
        self.incidence_angle = np.full(self.steps, 0)   # incidence angle [deg]
        self.tension = np.full(self.steps, 0)           # tension [N]
        self.sigma = 2                                  # standard deviation of smoothing
        self.initial_height = 0                         # initial hight [m]
        self.initial_vertical_speed = 0                 # initial vertical velocity [m/s]
        self.initial_rotation_speed = 2                 # initial rotation speed [r/s]
        self.initial_average_force = 0                  # initial average force [N]
        self.U = np.column_stack((self.incidence_angle, self.tension))
        self.x0 = [self.initial_height, self.initial_vertical_speed, self.initial_rotation_speed, self.initial_average_force]


class SimulutionScenarioRealisticPumping:
    def __init__(self):
        self.delta_t = 0.01                             # time between to simulation steps
        self.steps = 200                                # number of simulation steps
        self.incidence_angle = np.full(self.steps, 0)   # incidence angle [deg]
        self.tension = np.full(self.steps, 0)           # tension [N]
        self.sigma = 2                                  # standard deviation of smoothing
        self.initial_height = 0                         # initial hight [m]
        self.initial_vertical_speed = 0                 # initial vertical velocity [m/s]
        self.initial_rotation_speed = 2                 # initial rotation speed [r/s]
        self.initial_average_force = 0                  # initial average force [N]
        self.U = np.column_stack((self.incidence_angle, self.tension))
        self.x0 = [self.initial_height, self.initial_vertical_speed, self.initial_rotation_speed, self.initial_average_force]



class SimulutionScenarioMeasurement:
    def __init__(self):
        self.delta_t = 0.001                            # time between to simulation steps
        self.steps = 2000 #22510                        # number of simulation steps
        self.incidence_angle = np.full(self.steps, -10) # incidence angle [deg]
        self.tension = np.full(self.steps, 0)           # tension [N]
        self.sigma = 2                                  # standard deviation of smoothing
        self.initial_height = 0                         # initial hight [m]
        self.initial_vertical_speed = 0                 # initial vertical velocity [m/s]
        self.initial_rotation_speed = 38                # initial rotation speed [r/s]
        self.initial_average_force = 0                  # initial average force [N]
        self.U = np.column_stack((self.incidence_angle, self.tension))
        self.x0 = [self.initial_height, self.initial_vertical_speed, self.initial_rotation_speed, self.initial_average_force]




class NeuralNetworkParameters:
    def __init__(self):
        self.learning_rate = 0.0001
        self.number_nodes = 64
        self.activation_function = "ReLU"
        self.number_layers = 3
        self.number_epochs = 150
        self.training_scenarios = 10000
        self.min_input_angle = -20
        self.max_input_angle = 0
        self.min_input_tension = 0
        self.max_input_tension = 1000
        self.max_height = 100
        self.max_vertical_speed = 20
        self.max_rotation_speed = 20
        self.input_size = 6
        self.output_size = 4
