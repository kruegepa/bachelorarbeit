# imports
import matplotlib.pyplot as plt
import BasisModel
import BasisModelWake
import Parameters
import Simulation

class Plotter:
    def __init__(self):
        self.Simulator = Simulation.Simulator()


    def plot_height(self, data_basis_model_with_wake, data_basis_model_without_wake, steps, delta_t):
        plt.subplot(1, 4, 1)
        plt.plot(data_basis_model_without_wake[2], data_basis_model_without_wake[1].T[0], "g", label="without wake")
        plt.plot(data_basis_model_with_wake[2], data_basis_model_with_wake[1].T[0], "b", label="with wake")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Höhe $z$ in m", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)


    def plot_vertical_speed(self, data_basis_model_with_wake, data_basis_model_without_wake, steps, delta_t):
        plt.subplot(1, 4, 2)
        plt.plot(data_basis_model_without_wake[2], data_basis_model_without_wake[1].T[1], "g", label="without wake")
        plt.plot(data_basis_model_with_wake[2], data_basis_model_with_wake[1].T[1], "b", label="with wake")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("vertikale Geschwindigkeit $\dot{z}$ in m/s", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)


    def plot_rotation_speed(self, data_basis_model_with_wake, data_basis_model_without_wake, steps, delta_t):
        plt.subplot(1, 4, 3)
        plt.plot(data_basis_model_without_wake[2], data_basis_model_without_wake[1].T[2], "g", label="without wake")
        plt.plot(data_basis_model_with_wake[2], data_basis_model_with_wake[1].T[2], "b", label="with wake")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Rotationsgeschwindigkeit $\Omega$ in U/s", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)


    def plot_average_force(self, data_basis_model_with_wake, data_basis_model_without_wake, steps, delta_t):
        plt.subplot(1, 4, 4)
        plt.plot(data_basis_model_without_wake[2], data_basis_model_without_wake[1].T[3], "g", label="without wake")
        plt.plot(data_basis_model_with_wake[2], data_basis_model_with_wake[1].T[3], "b", label="with wake")
        plt.xlabel("Zeit $t$ in s", fontsize=10)
        plt.ylabel("Mittlere Kraft $\\bar{F}$ in N", fontsize=10)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.legend(loc=1)


    def generate_simulation_plot(self, BasisModelWithWake, BasisModelWithoutWake, x0, U, delta_t, steps, file_name):
        # generate simulation data
        data_basis_model_with_wake = self.Simulator.simulate_explicit_simple_model(BasisModelWithWake, x0, U, delta_t)
        data_basis_model_without_wake = self.Simulator.simulate_explicit_simple_model(BasisModelWithoutWake, x0, U, delta_t)

        # generate simulation plot
        plt.figure(figsize=(12, 3))
        plt.suptitle(file_name)
        self.plot_height(data_basis_model_with_wake, data_basis_model_without_wake, steps, delta_t)
        self.plot_vertical_speed(data_basis_model_with_wake, data_basis_model_without_wake, steps, delta_t)
        self.plot_rotation_speed(data_basis_model_with_wake, data_basis_model_without_wake, steps, delta_t)
        self.plot_average_force(data_basis_model_with_wake, data_basis_model_without_wake, steps, delta_t)
        plt.tight_layout()
        plt.savefig("Simulation/Plots/BasisModels/" + file_name + ".png", dpi=500)
        plt.show()


def main():
    # load and change parameters
    Rotor = Parameters.Rotor()
    file_name = "T=" + str(Rotor.T * Rotor.delta_t) + "s"

    # create models
    ModelWithWake = BasisModelWake.Rotor(Rotor)
    ModelWithoutWake = BasisModel.Rotor(Rotor)

    # create plot
    BasisModels = Plotter()
    BasisModels.generate_simulation_plot(ModelWithWake, ModelWithoutWake, Rotor.x0, Rotor.U, Rotor.delta_t, Rotor.steps, file_name)


main()
