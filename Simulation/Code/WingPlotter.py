import pandas as pd
import matplotlib.pyplot as plt

# Read the data from the CSV file
df = pd.read_csv('Simulation/Code/data.csv')

# Extract the relevant columns from the dataframe
alpha = df['Alpha']
cl = df['Cl']
cd = df['Cd']

# Create a figure with two subplots
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))

# Plot Alpha vs. CL
ax1.plot(alpha, cl)
ax1.plot(alpha, 0.11 * alpha + 0, '--')  # Add a line (slope = 0.11, intercept = 0)
ax1.set_xlabel('Alpha', fontsize=18)
ax1.set_ylabel('Auftriebsbeiwert $C_\mathrm{L}$', fontsize=18)
ax1.set_title('Alpha vs. $C_\mathrm{L}$', fontsize=20)
ax1.tick_params(axis='both', labelsize=18)

# Add equation label for the line
equation_line = r'$C_\mathrm{L} \approx 0.11 \cdot \alpha + 0$'
ax1.text(0.05, 0.95, equation_line, transform=ax1.transAxes, fontsize=18, va='top', ha='left')

# Plot Alpha vs. CD
ax2.plot(alpha, cd)
ax2.plot(alpha, 0.00015*(alpha**2) + 0.005, '--')  # Add a parabola (coefficients: a=0.00015, b=0, c=0.005)
ax2.set_xlabel('Alpha', fontsize=18)
ax2.set_ylabel('Widerstandsbeiwert $C_\mathrm{D}$', fontsize=18)
ax2.set_title('Alpha vs. $C_\mathrm{D}$', fontsize=20)
ax2.tick_params(axis='both', labelsize=18)

# Add equation label for the parabola
equation_parabola = r'$C_\mathrm{D} \approx 0.00015 \cdot \alpha^2 + 0.005$'
ax2.text(0.05, 0.95, equation_parabola, transform=ax2.transAxes, fontsize=18, va='top', ha='left')

# Adjust spacing between subplots
plt.tight_layout()
plt.savefig("Simulation/Graphiken/Flügel.png", dpi=500)
plt.savefig("Simulation/Graphiken/Flügel.svg")
