import numpy as np


class Rotor:
    def __init__(self, Parameters):
        # import parameters
        self.Parameters = Parameters

        # side calculations
        self.inertial_moment = self.Parameters.total_mass * np.square(self.Parameters.distance_wing_end) 
        self.rotor_area = np.pi * np.square(self.Parameters.distance_wing_end)

        # variables
        self.wake_velocity = 0
        self.gamma = 0
        self.alpha = 0
        self.lift_coefficient = 0
        self.drag_coefficient = 0
        self.drag_force = 0
        self.wind_velocity = 0
        self.lift_force = 0

    
    def get_states(self, x, u):
        return x


    def get_lift_coefficient(self, x, u):
        return self.Parameters.estimated_lift_slope * self.gamma + self.Parameters.estimated_zero_lift_coefficient


    def get_drag_coefficient(self, x, u):
        return self.Parameters.estimated_drag_slope * self.gamma + self.Parameters.estimated_zero_drag_coefficient


    def get_lift_force(self, x, u):
        return 1/2 * self.Parameters.air_density * self.Parameters.wing_area * np.square(self.wind_velocity) * self.lift_coefficient


    def get_drag_force(self, x, u):
        return 1/2 * self.Parameters.air_density * self.Parameters.wing_area * np.square(self.wind_velocity) * self.drag_coefficient


    def get_alpha(self, x, u):
        return np.rad2deg(-np.arctan((x[1] - self.get_wake_velocity_rotor(x, u)) / (x[2] * self.Parameters.half_radius)))


    def get_wake_velocity_rotor(self, x, u):
        numerator = np.sqrt(2 * np.abs(x[3]))
        denominator = self.Parameters.air_density * self.rotor_area
        wake_velocity_infinity = numerator / denominator
        return 1/2 * wake_velocity_infinity
    

    def get_wind_velocity(self, x, u):
        return np.sqrt(np.square(x[2] * self.Parameters.half_radius) + np.square(x[1] - self.get_wake_velocity_rotor(x, u)))


    def get_gamma(self, x, u):
        return self.alpha - u[0]


    def get_pulling_force(self, x, u):
        return u[1]
    

    def system_differential_equation(self, x, u):
        self.gamma = self.get_gamma(x, u)
        self.alpha = self.get_alpha(x, u)
        self.lift_coefficient = self.get_lift_coefficient(x, u)
        self.drag_coefficient = self.get_drag_coefficient(x, u)
        self.drag_force = self.get_drag_force(x, u)
        self.wind_velocity = self.get_wind_velocity(x, u)
        self.lift_force = self.get_lift_force(x, u)
        average_force = x[3]
        instantaneous_force = self.Parameters.number_wings * self.lift_force * np.cos(np.deg2rad(self.alpha)) + self.Parameters.number_wings * self.drag_force * np.sin(np.deg2rad(self.alpha)) - self.Parameters.gravitational_force - self.get_pulling_force(x, u)

        change_height = x[1]
        change_vertical_speed = (1 / self.Parameters.total_mass) * (self.Parameters.number_wings * self.lift_force * np.cos(np.deg2rad(self.alpha)) + self.Parameters.number_wings * self.drag_force * np.sin(np.deg2rad(self.alpha)) - self.Parameters.gravitational_force - self.get_pulling_force(x, u))
        change_rotation_speed = ((self.Parameters.number_wings * self.Parameters.half_radius) / (self.Parameters.total_mass * self.inertial_moment)) * (self.Parameters.number_wings * self.lift_force * np.sin(np.deg2rad(self.alpha)) - self.Parameters.number_wings * self.drag_force * np.cos(np.deg2rad(self.alpha)))
        change_average_force = (instantaneous_force - average_force) / self.Parameters.T
        return np.array([change_height, change_vertical_speed, change_rotation_speed, change_average_force])
