# imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.colors as colors
from sklearn.metrics import r2_score
import BasisModel
import ReferenceModel
import NeuralNetwork
import Parameters
import Simulation
from scipy.ndimage import gaussian_filter1d
import scipy.optimize as sci
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split



##################################################################################################
# PlotSzenario                                                                                   #
##################################################################################################
class PlotSzenario:
    def __init__(self, RotorParameters, SimulutionScenario, NeuralNetworkParameters, file_name):
        # create models
        MyBasisModel = BasisModel.Rotor(RotorParameters)
        MyReferenceModel = ReferenceModel.Rotor(RotorParameters)
        MyNeuralNetwork = NeuralNetwork.Model(RotorParameters, NeuralNetworkParameters, MyReferenceModel)
        MySimulator = Simulation.Simulator()

        # simulation parameters
        self.SimulutionScenario = SimulutionScenario

        # simulate models
        data_reference_model = MySimulator.simulate_explicit_reference_model(MyReferenceModel, self.SimulutionScenario.x0, self.SimulutionScenario.U, self.SimulutionScenario.delta_t)
        data_basis_model = MySimulator.simulate_explicit_simple_model(MyBasisModel, self.SimulutionScenario.x0, self.SimulutionScenario.U, self.SimulutionScenario.delta_t)
        data_neural_network = MySimulator.simulate_explicit_neuronal_network_model(MyNeuralNetwork, self.SimulutionScenario.x0, self.SimulutionScenario.U, self.SimulutionScenario.delta_t)

        # create plots
        self.generate_simulation_plot(MyBasisModel, MyReferenceModel, MyNeuralNetwork, data_basis_model, data_reference_model, data_neural_network, file_name)


    def plot_height(self, data_basis_model, data_reference_model, data_neural_network):
        plt.subplot(4, 2, 1)
        plt.plot(data_basis_model[2], data_basis_model[1].T[0], "g", label="Basismodell", linestyle="solid")
        plt.plot(data_reference_model[2], data_reference_model[1].T[0], "b", label="Referenzmodell", linestyle="dashed")
        plt.plot(data_neural_network[2], data_neural_network[1].T[0], "y", label="Neuronales Netz")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$z$ in m", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_vertical_speed(self, data_basis_model, data_reference_model, data_neural_network):
        plt.subplot(4, 2, 2)
        plt.plot(data_basis_model[2], data_basis_model[1].T[1], "g", label="Basismodell", linestyle="solid")
        plt.plot(data_reference_model[2], data_reference_model[1].T[1], "b", label="Referenzmodell", linestyle="dashed")
        plt.plot(data_neural_network[2], data_neural_network[1].T[1], "y", label="Neuronales Netz")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\dot{z}$ in m/s", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_rotation_speed(self, data_basis_model, data_reference_model, data_neural_network):
        plt.subplot(4, 2, 3)
        plt.plot(data_basis_model[2], data_basis_model[1].T[2], "g", label="Basismodell", linestyle="solid")
        plt.plot(data_reference_model[2], data_reference_model[1].T[2], "b", label="Referenzmodell", linestyle="dashed")
        plt.plot(data_neural_network[2], data_neural_network[1].T[2], "y", label="Neuronales Netz")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\Omega$ in U/s", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def get_basis_model_kinetic_energy(self, BasisModel, data_simple_model):
        return np.abs(1/2 * BasisModel.Parameters.total_mass * np.square(data_simple_model[1].T[1]))


    def get_reference_model_kinetic_energy(self, ReferenceModel, data_reference_model):
        return np.abs(1/2 * ReferenceModel.Parameters.total_mass * np.square(data_reference_model[1].T[1]))


    def get_neural_network_kinetic_energy(self, ReferenceModel, data_neural_network_model):
        return np.abs(1/2 * ReferenceModel.Parameters.total_mass * np.square(data_neural_network_model[1].T[1]))


    def get_basis_model_potential_energy(self, BasisModel, data_simple_model):
        return -BasisModel.Parameters.total_mass * BasisModel.Parameters.gravitation * data_simple_model[1].T[0]


    def get_reference_model_potential_energy(self, ReferenceModel, data_reference_model):
        return -ReferenceModel.Parameters.total_mass * ReferenceModel.Parameters.gravitation * data_reference_model[1].T[0]


    def get_neural_network_potential_energy(self, ReferenceModel, data_neural_network_model):
        return -ReferenceModel.Parameters.total_mass * ReferenceModel.Parameters.gravitation * data_neural_network_model[1].T[0]


    def get_basis_model_rotational_energy(self, BasisModel, data_simple_model):
        return 1/2 * BasisModel.inertial_moment * np.square(data_simple_model[1].T[2])


    def get_reference_model_rotational_energy(self, ReferenceModel, data_reference_model):
        return 1/2 * ReferenceModel.inertial_moment * np.square(data_reference_model[1].T[2])


    def get_neural_network_rotational_energy(self, ReferenceModel, data_neural_network_model):
        return 1/2 * ReferenceModel.inertial_moment * np.square(data_neural_network_model[1].T[2])


    def get_basis_model_total_energy(self, BasisModel, data_simple_model):
        return self.get_basis_model_kinetic_energy(BasisModel, data_simple_model) + self.get_basis_model_potential_energy(BasisModel, data_simple_model) + self.get_basis_model_rotational_energy(BasisModel, data_simple_model)


    def get_reference_model_total_energy(self, ReferenceModel, data_reference_model):
        return self.get_reference_model_kinetic_energy(ReferenceModel, data_reference_model) + self.get_reference_model_potential_energy(ReferenceModel, data_reference_model) + self.get_reference_model_rotational_energy(ReferenceModel, data_reference_model)


    def get_neural_network_total_energy(self, ReferenceModel, data_neural_network_model):
        return self.get_neural_network_kinetic_energy(ReferenceModel, data_neural_network_model) + self.get_neural_network_potential_energy(ReferenceModel, data_neural_network_model) + self.get_neural_network_rotational_energy(ReferenceModel, data_neural_network_model)       


    def plot_energy(self, BasisModel, ReferenceModel, data_basis_model, data_reference_model, data_neural_network):
        plt.subplot(4, 2, 4)
        
        # plot kinetic energy
        plt.plot(data_basis_model[2], self.get_basis_model_kinetic_energy(BasisModel, data_basis_model), "g--", label="Basismodell: $E_\mathrm{kin}$")
        plt.plot(data_reference_model[2], self.get_reference_model_kinetic_energy(ReferenceModel, data_reference_model), "b--", label="Referenzmodell: $E_\mathrm{kin}$")
        plt.plot(data_neural_network[2], self.get_neural_network_kinetic_energy(ReferenceModel, data_neural_network), "y--", label="Neuronales Netz: $E_\mathrm{kin}$")

        # plot potential energy
        plt.plot(data_basis_model[2], self.get_basis_model_potential_energy(BasisModel, data_basis_model), "g-.", label="Basismodell: $E_\mathrm{pot}$")
        plt.plot(data_reference_model[2], self.get_reference_model_potential_energy(ReferenceModel, data_reference_model), "b-.", label="Referenzmodell: $E_\mathrm{pot}$")
        plt.plot(data_neural_network[2], self.get_neural_network_potential_energy(ReferenceModel, data_neural_network), "y-.", label="Neuronales Netz: $E_\mathrm{pot}$")

        # plot rotational energy
        plt.plot(data_basis_model[2], self.get_basis_model_rotational_energy(BasisModel, data_basis_model), "g:", label="Basismodell: $E_\mathrm{rot}$")
        plt.plot(data_reference_model[2], self.get_reference_model_rotational_energy(ReferenceModel, data_reference_model), "b:", label="Referenzmodell: $E_\mathrm{rot}$")
        plt.plot(data_neural_network[2], self.get_neural_network_rotational_energy(ReferenceModel, data_neural_network), "y:", label="Neuronales Netz: $E_\mathrm{rot}$")

        # plot total energy
        plt.plot(data_basis_model[2], self.get_basis_model_total_energy(BasisModel, data_basis_model), "g-", label="B: $E_\mathrm{tot}$")
        plt.plot(data_reference_model[2], self.get_reference_model_total_energy(ReferenceModel, data_reference_model), "b-", label="Referenzmodell: $E_\mathrm{tot}$")
        plt.plot(data_neural_network[2], self.get_neural_network_total_energy(ReferenceModel, data_neural_network), "y-", label="Neuronales Netz: $E_\mathrm{tot}$")

        # plot settings
        plt.legend(handles=[
            Line2D([], [], linestyle='--', color='black'),
            Line2D([], [], linestyle='-.', color='black'),
            Line2D([], [], linestyle=':', color='black'),
            Line2D([], [], linestyle='-', color='black')],
            labels=["$E_\mathrm{kin}$", "$E_\mathrm{pot}$", "$E_\mathrm{rot}$", "$E_\mathrm{tot}$"],
            loc=1, fontsize=18
        )
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.tick_params(axis='both', labelsize=18)
        plt.xlabel('$t$ in s', fontsize=18)
        plt.ylabel('$E$ in J', fontsize=18)


    def plot_incidence_angle(self, data_basis_model, data_reference_model, data_neural_network):
        plt.subplot(4, 2, 5)
        plt.plot(data_reference_model[2][:-1], self.SimulutionScenario.U[:,0], "r", label="$\\beta$")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("${\\beta}$ in Grad", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_tension(self, data_basis_model, data_reference_model, data_neural_network):
        plt.subplot(4, 2, 6)
        plt.plot(data_reference_model[2][:-1], self.SimulutionScenario.U[:,1], "r", label="$F_\mathrm{T}$")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$T$ in N", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color='black', lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_average_force(self, data_simple_model, data_reference_model, data_neural_network):
        plt.subplot(4, 2, 7)
        plt.plot(data_simple_model[2], data_simple_model[1].T[3], "g", label="Basismodell", linestyle="solid")
        plt.plot(data_reference_model[2], data_reference_model[1].T[3], "b", label="Referenzmodell", linestyle="dashed")
        plt.plot(data_neural_network[2], data_neural_network[1].T[3], "y", label="Neuronales Netz")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\\bar{F}$ in N", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_power(self, data_simple_model, data_reference_model, data_neural_network, BasisModel, ReferenceModel, NeuralNetwork):
        plt.subplot(4, 2, 8)
        # gravitational power
        reference_gravitational_power = ReferenceModel.Parameters.gravitational_force * data_reference_model[1].T[1][:-1]
        plt.plot(data_reference_model[2][:-1], reference_gravitational_power, "b--", label="Referenzmodell: $P_\mathrm{grav}$")
        basis_gravitational_power = BasisModel.Parameters.gravitational_force * data_simple_model[1].T[1][:-1]
        plt.plot(data_simple_model[2][:-1], basis_gravitational_power, "g--", label="Basismodell: $P_\mathrm{grav}$")
        neural_gravitational_power = ReferenceModel.Parameters.gravitational_force * data_neural_network[1].T[1][:-1]
        plt.plot(data_simple_model[2][:-1], neural_gravitational_power, "y--", label="Neuronales Netz: $P_\mathrm{grav}$")

        # tension power
        reference_tension_power = self.SimulutionScenario.U.T[1] * data_reference_model[1].T[1][:-1]
        plt.plot(data_reference_model[2][:-1], reference_tension_power, "b-.", label="Referenzmodell: $P_\mathrm{Zug}$")
        basis_tension_power = self.SimulutionScenario.U.T[1] * data_simple_model[1].T[1][:-1]
        plt.plot(data_simple_model[2][:-1], basis_tension_power, "g-.", label="Basismodell: $P_\mathrm{Zug}$")
        neural_tension_power = self.SimulutionScenario.U.T[1] * data_neural_network[1].T[1][:-1]
        plt.plot(data_reference_model[2][:-1], neural_tension_power, "y-.", label="Neuronales Netz: $P_\mathrm{Zug}$")

        # vertical power
        reference_vertical_power = -(ReferenceModel.Parameters.total_mass * np.diff(data_reference_model[1].T[1]) / self.SimulutionScenario.delta_t) * data_reference_model[1].T[1][:-1]
        plt.plot(data_reference_model[2][:-1], reference_vertical_power, "b:", label="Referenzmodell: $P_\mathrm{vert}$")
        basis_vertical_power = -(BasisModel.Parameters.total_mass * np.diff(data_simple_model[1].T[1]) / self.SimulutionScenario.delta_t) * data_simple_model[1].T[1][:-1]
        plt.plot(data_simple_model[2][:-1], basis_vertical_power, "g:", label="Basismodell: $P_\mathrm{vert}$")
        neural_vertical_power = -(ReferenceModel.Parameters.total_mass * np.diff(data_neural_network[1].T[1]) / self.SimulutionScenario.delta_t) * data_neural_network[1].T[1][:-1]
        plt.plot(data_reference_model[2][:-1], neural_vertical_power, "y:", label="Neuronales Netz: $P_\mathrm{vert}$")

        # tangential power
        reference_tangential_power = np.diff(data_reference_model[1].T[2]) * np.diff(data_reference_model[1].T[2]) * ReferenceModel.inertial_moment / self.SimulutionScenario.delta_t
        plt.plot(data_reference_model[2][:-1], reference_tangential_power, "b", linestyle=(0, (1, 3)), label="Referenzmodell: $P_\mathrm{tang}$")
        basis_tangential_power = np.diff(data_simple_model[1].T[2]) * np.diff(data_simple_model[1].T[2]) * BasisModel.inertial_moment / self.SimulutionScenario.delta_t
        plt.plot(data_simple_model[2][:-1], basis_tangential_power, "g", linestyle=(0, (1, 3)), label="Basismodell: $P_\mathrm{tang}$")
        neural_tangential_power = np.diff(data_neural_network[1].T[2]) * np.diff(data_neural_network[1].T[2]) * ReferenceModel.inertial_moment / self.SimulutionScenario.delta_t
        plt.plot(data_reference_model[2][:-1], neural_tangential_power, "y", linestyle=(0, (1, 3)), label="Neuronales Netz: $P_\mathrm{tang}$")

        # total power
        reference_total_power = reference_gravitational_power + reference_tension_power + reference_vertical_power + reference_tangential_power
        plt.plot(data_reference_model[2][:-1], reference_total_power, "b-", label="Referenzmodell: $P_\mathrm{tot}$")
        basis_total_power = basis_gravitational_power + basis_tension_power + basis_vertical_power + basis_tangential_power
        plt.plot(data_simple_model[2][:-1], basis_total_power, "g-", label="Basismodell: $P_\mathrm{tot}$")
        neural_total_power = neural_gravitational_power + neural_tension_power + neural_vertical_power + neural_tangential_power
        plt.plot(data_reference_model[2][:-1], neural_total_power, "b-", label="Referenzmodell: $P_\mathrm{tot}$")

        # plot settings
        plt.legend(handles=[Line2D([], [], linestyle='--', color='black'),
            Line2D([], [], linestyle='-.', color='black'),
            Line2D([], [], linestyle=':', color='black'),
            Line2D([], [], linestyle=(0, (1, 3)), color='black'),
            Line2D([], [], linestyle='-', color='black')],
            labels=["$P_\mathrm{grav}$", "$P_\mathrm{Zug}$", "$P_\mathrm{vert}$", "$P_\mathrm{tang}$", "$P_\mathrm{tot}$"],
            loc=1, fontsize=18
        )

        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.xlabel('$t$ in s', fontsize=18)
        plt.ylabel('$P$ in W', fontsize=18)


    def generate_simulation_plot(self, BasisModel, ReferenceModel, NeuralNetwork, data_simple_model, data_reference_model, data_neural_network, name):
        plt.figure(figsize=(15, 15))
        self.plot_height(data_simple_model, data_reference_model, data_neural_network)
        self.plot_vertical_speed(data_simple_model, data_reference_model, data_neural_network)
        self.plot_rotation_speed(data_simple_model, data_reference_model, data_neural_network)
        self.plot_energy(BasisModel, ReferenceModel, data_simple_model, data_reference_model, data_neural_network)
        self.plot_tension(data_simple_model, data_reference_model, data_neural_network)
        self.plot_incidence_angle(data_simple_model, data_reference_model, data_neural_network)
        self.plot_average_force(data_simple_model, data_reference_model, data_neural_network)
        self.plot_power(data_simple_model, data_reference_model, data_neural_network, BasisModel, ReferenceModel, NeuralNetwork)
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/Simulation" + name + ".png", dpi=500)
        plt.savefig("Simulation/Graphiken/Simulation" + name + ".svg")


    def plot_angles(self, BasisModel, data):
        plt.subplot(1, 3, 1)
        plt.plot(data[2][:-1], [d[0] for d in BasisModel.data[::4]], label="$\gamma$ in $\deg$")
        plt.plot(data[2][:-1], [d[1] for d in BasisModel.data[::4]], label="$\\alpha$ in $\deg$")
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlabel("Zeit in s")
        plt.ylabel("Winkel in $\deg$")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend()


    def plot_coefficients_velocity(self, BasisModel, data):
        plt.subplot(1, 3, 2)
        plt.plot(data[2][:-1], [d[2] for d in BasisModel.data[::4]], label="$C_\mathrm{L}$")
        plt.plot(data[2][:-1], [d[3] for d in BasisModel.data[::4]], label="$C_\mathrm{D}$")
        plt.plot(data[2][:-1], [d[4] for d in BasisModel.data[::4]], label="$v_\mathrm{wind}$ in m/s")
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlabel("Zeit in s")
        plt.ylabel("Koeffizient / Geschwindigkeit in m/s")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend()

    
    def plot_forces(self, BasisModel, data):
        plt.subplot(1, 3, 3)
        plt.plot(data[2][:-1], [d[5] for d in BasisModel.data[::4]], label="$F_\mathrm{L,v}$ in N")
        plt.plot(data[2][:-1], [d[6] for d in BasisModel.data[::4]], label="$F_\mathrm{L,h}$ in N")
        plt.plot(data[2][:-1], [d[7] for d in BasisModel.data[::4]], label="$F_\mathrm{D,v}$ in N")
        plt.plot(data[2][:-1], [d[8] for d in BasisModel.data[::4]], label="$F_\mathrm{D,h}$ in N")
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlabel("Zeit in s")
        plt.ylabel("Kraft in N")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend()


    def generate_basis_data_plot(self, BasisModel, data, name):
        self.plot_angles(BasisModel, data)
        self.plot_coefficients_velocity(BasisModel, data)
        self.plot_forces(BasisModel, data)
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/SimulationBasisData" + name + ".png", dpi=500)
        plt.savefig("Simulation/Graphiken/SimulationBasisData" + name + ".svg")




##################################################################################################
# PlotDifferentWakes                                                                             #
##################################################################################################
class PlotDifferentWakes():
    def __init__(self, RotorParameters, SimulutionScenario, file_name):
        # parameters and objects
        self.RotorParameters = RotorParameters
        self.SimulutionScenario = SimulutionScenario
        self.MySimulator = Simulation.Simulator()

        # wake parameters and simulation data
        self.wake_times = [0.01, 0.1, 1, 10, 100, 1000]
        self.simulations = []

        # create plot
        self.generate_simulation_data()
        self.create_plot()


    def generate_simulation_data(self):
        for t in self.wake_times:
            self.RotorParameters.T = t
            Model = ReferenceModel.Rotor(self.RotorParameters)
            self.simulations.append(self.MySimulator.simulate_explicit_reference_model(Model, self.SimulutionScenario.x0, self.SimulutionScenario.U, self.SimulutionScenario.delta_t))


    def plot_hight(self):
        plt.subplot(3, 2, 1)
        for i, data in enumerate(self.simulations):
            plt.plot(data[2], data[1].T[0], "b", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15 * (i+1))
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$z$ in m", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_vertical_velocity(self):
        plt.subplot(3, 2, 2)
        for i, data in enumerate(self.simulations):
            plt.plot(data[2], data[1].T[1], "b", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15 * (i+1))
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\dot{z}$ in m/s", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_rotational_speed(self):
        plt.subplot(3, 2, 3)
        for i, data in enumerate(self.simulations):
            plt.plot(data[2], data[1].T[2], "b", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15 * (i+1))
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\Omega$ in U/s", fontsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.tick_params(axis='both', labelsize=18)
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_average_force(self):
        plt.subplot(3, 2, 4)
        for i, data in enumerate(self.simulations):
            plt.plot(data[2], data[1].T[3], "b", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15 * (i+1))
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\\bar{F}$ in N", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.legend(loc=1, fontsize=18)


    def plot_energy(self):
        plt.subplot(3, 2, 5)
        for i, data in enumerate(self.simulations):
            Model = ReferenceModel.Rotor(self.RotorParameters)
            kinetic_energy = np.abs(1/2 * Model.Parameters.total_mass * np.square(data[1].T[1]))
            potential_energy = Model.Parameters.total_mass * Model.Parameters.gravitation * data[1].T[0]
            rotational_energy = 1/2 * Model.inertial_moment * np.square(data[1].T[2])
            total_energy = kinetic_energy + potential_energy + rotational_energy
            plt.plot(data[2], total_energy, color="b", linestyle="solid", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15*(i+1))

        plt.legend(loc=1, fontsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$E$ in J", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)


    def plot_power(self):
        plt.subplot(3, 2, 6)
        for i, data in enumerate(self.simulations):
            Model = ReferenceModel.Rotor(self.RotorParameters)
            gravitational_power = Model.Parameters.gravitational_force * data[1].T[1][:-1]
            tension_power = self.SimulutionScenario.tension * data[1].T[1][:-1]
            vertical_power = (Model.Parameters.total_mass * np.diff(data[1].T[1]) / self.SimulutionScenario.delta_t) * data[1].T[1][:-1]
            tangential_power = np.diff(data[1].T[2]) * np.diff(data[1].T[2]) * Model.inertial_moment / self.SimulutionScenario.delta_t
            total_power = gravitational_power + tension_power + vertical_power + tangential_power
            plt.plot(data[2][:-1], total_power, color="b", linestyle="solid", label="$T$ = " + str(self.wake_times[i]) + " s", alpha=0.15*(i+1))
    
        plt.legend(loc=1, fontsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, self.SimulutionScenario.steps * self.SimulutionScenario.delta_t)
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$P$ in W", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)


    def create_plot(self):
        plt.figure(figsize=(15, 11.25))
        self.plot_hight()
        self.plot_vertical_velocity()
        self.plot_rotational_speed()
        self.plot_average_force()
        self.plot_energy()
        self.plot_power()
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/SimulationDifferentWakes.png", dpi=500)
        plt.savefig("Simulation/Graphiken/SimulationDifferentWakes.svg")




##################################################################################################
# EvaluateModelQuality                                                                           #
##################################################################################################
class EvaluateModelQuality:
    def __init__(self, RotorParameters, SimulutionScenario, NeuralNetworkParameters):
        # create models
        self.Simulator = Simulation.Simulator()
        self.RotorParameters = RotorParameters
        self.SimulutionScenario = SimulutionScenario
        self.NeuralNetworkParameters = NeuralNetworkParameters
        self.BasisModel = BasisModel.Rotor(self.RotorParameters)
        self.ReferenceModel = ReferenceModel.Rotor(self.RotorParameters)
        self.NeuralNetwork = NeuralNetwork.Model(self.RotorParameters, self.NeuralNetworkParameters, self.ReferenceModel)

        # heatmap
        self.vertical_speeds = np.linspace(-10, 10, 10)
        self.rotation_speeds = np.linspace(0.5, 20, 10)
        self.incidence_angles = np.linspace(-30, 30, 10)
        self.tensions = np.linspace(0, 200, 10)
        self.initial_height = 10
        self.average_force = 0

        # load input data
        self.x = np.load("datax.npy")
        self.u = np.transpose(np.load("datau.npy"))
        self.x0 = np.array([self.x[0][0], self.x[1][0], self.x[2][0], self.x[3][0]])
        self.delta_t = 0.001

        # scores
        data_basis_model = self.generate_basis_data()
        data_reference_model = self.generate_reference_data()
        data_neural_network = self.generate_neural_network_data()
        #self.calculate_mean_squared_error(data_reference_model, data_basis_model, data_neural_network)
        plt.figure(figsize=(15, 6))
        plt.rcParams.update({'font.size': 18})
        self.plot_basis_model_heatmap()
        self.plot_neural_network_heatmap()
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/HeatMapsF0NMAE.png", dpi=500)
        plt.savefig("Simulation/Graphiken/HeatMapsF0NMAE.svg")
        plt.show()


    def generate_basis_data(self):
        data_basis_model = self.Simulator.simulate_explicit_simple_model(self.BasisModel, np.array([self.x[0][0], self.x[1][0], self.x[2][0], 0]), self.u, self.delta_t)[1]
        return data_basis_model


    def generate_reference_data(self):
        data_reference_model = self.Simulator.simulate_explicit_reference_model(self.ReferenceModel, self.x0, self.u, self.delta_t)[1]
        return data_reference_model


    def generate_neural_network_data(self):
        data_neural_network = self.Simulator.simulate_explicit_neuronal_network_model(self.NeuralNetwork, self.x0, self.u, self.delta_t)[1]
        return data_neural_network


    def calculate_mean_squared_error(self, data_reference_model, data_basis_model, data_neural_network):
        mse_basis = mean_squared_error(data_reference_model, data_basis_model)
        mse_neural = mean_squared_error(data_reference_model, data_neural_network)
        print("mse basis: " + str(mse_basis))
        print("mse neural: " + str(mse_neural))


    def calculate_loss(self, get_prediction, get_ground_truth, vertical_speed, rotation_speed, average_force):
        losses = []
        x = np.array([self.initial_height, vertical_speed, rotation_speed, average_force])

        for angle in self.incidence_angles:
            for tension in self.tensions:
                u = np.array([angle, tension])
                prediction = get_prediction(x, u)
                ground_truth = get_ground_truth(x, u)
                losses.append(mean_absolute_error(prediction, ground_truth))

        return np.mean(losses)


    def plot_basis_model_heatmap(self):
        plt.subplot(1, 2, 1)
        vertical_speeds, rotation_speeds = np.meshgrid(self.vertical_speeds, self.rotation_speeds)
        losses = np.vectorize(self.calculate_loss)(self.BasisModel.system_differential_equation, self.ReferenceModel.system_differential_equation, vertical_speeds, rotation_speeds, self.average_force)
        plt.pcolormesh(vertical_speeds, rotation_speeds, losses, cmap="coolwarm", norm=colors.LogNorm())
        plt.colorbar(label='$\mathcal{L}$')
        plt.xlabel('$\dot{z}$ in m/s', fontsize=18)
        plt.ylabel('$\Omega$ in U/s', fontsize=18)


    def plot_neural_network_heatmap(self):
        plt.subplot(1, 2, 2)
        vertical_speeds, rotation_speeds = np.meshgrid(self.vertical_speeds, self.rotation_speeds)
        losses = np.vectorize(self.calculate_loss)(self.NeuralNetwork.predict, self.ReferenceModel.system_differential_equation, vertical_speeds, rotation_speeds, self.average_force)
        plt.pcolormesh(vertical_speeds, rotation_speeds, losses, cmap="coolwarm", norm=colors.LogNorm())
        plt.colorbar(label='$\mathcal{L}$')
        plt.xlabel('$\dot{z}$ in m/s', fontsize=18)
        plt.ylabel('$\Omega$ in U/s', fontsize=18)

        


##################################################################################################
# FitReferenzModelParameters                                                                     #
##################################################################################################
class FitReferenzModelParameters:
    def __init__(self, RotorParameters, SimulutionScenario, measurement_delta_t=0.25, measurement_data_time_duration=2.25, measurement_data_path="Videos/Height/HeightData.txt"):
        # create objects
        self.Simulator = Simulation.Simulator()
        self.RotorParameters = RotorParameters
        self.SimulutionScenario = SimulutionScenario

        # measurement parameters
        self.measurement_delta_t = measurement_delta_t
        self.measurement_data_time_duration = measurement_data_time_duration
        self.measurement_data_path = measurement_data_path

        # fit parameters
        parameters = self.determine_optimal_parameters()
        self.generate_plot(parameters)


    def load_measurement_data(self):
        data = np.loadtxt(self.measurement_data_path, skiprows=1)
        return data


    def calculate_error(self, estimated_parameters):
        # set estimated parameters
        print(estimated_parameters)
        self.RotorParameters.lift_slope = estimated_parameters[0]
        self.RotorParameters.zero_lift_coefficient = estimated_parameters[1]
        self.RotorParameters.drag_slope = estimated_parameters[2]
        self.RotorParameters.zero_drag_coefficient = estimated_parameters[3]

        # create reference model with estimated parameters
        MyReferenceModel = ReferenceModel.Rotor(self.RotorParameters)

        # simulate and adjust reference model
        data_reference_model = self.Simulator.simulate_explicit_reference_model(MyReferenceModel, self.SimulutionScenario.x0, self.SimulutionScenario.U, self.SimulutionScenario.delta_t)[0].T[0]
        steps_size = int(self.measurement_delta_t / self.SimulutionScenario.delta_t)
        data_reference_model = data_reference_model[::steps_size]

        # load measurement data
        data_measurement = self.load_measurement_data()

        # calculate mean squared error
        mse = mean_squared_error(data_reference_model, data_measurement)
        print(mse)
        return mse


    def determine_optimal_parameters(self, cla=0.11, cl0=0, cda=0.00015, cd0=0.005):
        initial_estimation = np.array([cla, cl0, cda, cd0])
        result = sci.minimize(self.calculate_error, initial_estimation, bounds=[(0, np.inf)] * len(initial_estimation))
        return result

    
    def generate_plot(self, parameters):
        plt.figure()

        # plot measurement
        data_measurement = self.load_measurement_data()
        T = np.linspace(0, self.measurement_data_time_duration, int(self.measurement_data_time_duration//self.measurement_delta_t)+1)
        plt.plot(T, data_measurement)

        # plot simulation
        self.RotorParameters.lift_slope = parameters[0]
        self.RotorParameters.zero_lift_coefficient = parameters[1]
        self.RotorParameters.drag_slope = parameters[2]
        self.RotorParameters.zero_drag_coefficient = parameters[3]
        MyReferenceModel = ReferenceModel.Rotor(self.RotorParameters)
        data_reference_model = self.Simulator.simulate_explicit_reference_model(MyReferenceModel, self.SimulutionScenario.x0, self.SimulutionScenario.U, self.SimulutionScenario.delta_t)
        plt.plot(data_reference_model[2], data_reference_model[1].T[0], "b", label="Referenzmodell")
        plt.show()



##################################################################################################
##################################################################################################
class PlotPumping:
    def __init__(self, RotorParameters):
        # create models
        self.MyReferenceModel = ReferenceModel.Rotor(RotorParameters)
        steps = 500
        delta_t = 0.001

        # create plots
        x = np.load("datax.npy")
        u = np.load("datau.npy")
        t = np.linspace(0, 0.5, 500)
        self.generate_simulation_plot(t, x, u, steps, delta_t)


    def plot_height(self, t, x, steps, delta_t):
        plt.subplot(4, 2, 1)
        plt.plot(t, x[0], "b", label="Referenzmodell")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$z$ in m", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        #plt.legend(loc=1, fontsize=10)


    def plot_vertical_speed(self, t, x, steps, delta_t):
        plt.subplot(4, 2, 2)
        plt.plot(t, x[1], "b", label="Referenzmodell")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\dot{z}$ in m/s", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        #plt.legend(loc=1, fontsize=10)


    def plot_rotation_speed(self, t, x, steps, delta_t):
        plt.subplot(4, 2, 3)
        plt.plot(t, x[2], "b", label="Referenzmodell")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\Omega$ in U/s", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        #plt.legend(loc=1, fontsize=10)


    def get_reference_model_kinetic_energy(self, ReferenceModel, t, x, steps, delta_t):
        return np.abs(1/2 * self.MyReferenceModel.Parameters.total_mass * np.square(x[1]))


    def get_reference_model_potential_energy(self, ReferenceModel, t, x, steps, delta_t):
        return -self.MyReferenceModel.Parameters.total_mass * ReferenceModel.Parameters.gravitation * x[0]


    def get_reference_model_rotational_energy(self, ReferenceModel, t, x, steps, delta_t):
        return 1/2 * self.MyReferenceModel.inertial_moment * np.square(x[2])


    def get_reference_model_total_energy(self, ReferenceModel, t, x, steps, delta_t):
        return self.get_reference_model_kinetic_energy(ReferenceModel, t, x, steps, delta_t) + self.get_reference_model_potential_energy(ReferenceModel, t, x, steps, delta_t) + self.get_reference_model_rotational_energy(ReferenceModel, t, x, steps, delta_t)


    def plot_energy(self, ReferenceModel, t, x, steps, delta_t):
        plt.subplot(4, 2, 7)
        
        # plot kinetic energy
        plt.plot(t, self.get_reference_model_kinetic_energy(self.MyReferenceModel, t, x, steps, delta_t), "b--", label="$E_\mathrm{kin}$")

        # plot potential energy
        plt.plot(t, self.get_reference_model_potential_energy(self.MyReferenceModel, t, x, steps, delta_t), "b-.", label="$E_\mathrm{pot}$")

        # plot rotational energy
        plt.plot(t, self.get_reference_model_rotational_energy(self.MyReferenceModel, t, x, steps, delta_t), "b:", label="$E_\mathrm{rot}$")

        # plot total energy
        plt.plot(t, self.get_reference_model_total_energy(self.MyReferenceModel, t, x, steps, delta_t), "b-", label="$E_\mathrm{tot}$")

        # plot settings
        plt.legend(loc=1, fontsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.xlabel('$t$ in s', fontsize=18)
        plt.ylabel('$E$ in J', fontsize=18)
        plt.tick_params(axis='both', labelsize=18)


    def plot_incidence_angle(self, t, u, steps, delta_t):
        plt.subplot(4, 2, 5)
        plt.plot(t[:-1], u[0], "b", label="Anstellwinkel")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("${\\beta}$ in Grad", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        #plt.legend(loc=1, fontsize=10)


    def plot_tension(self, t, u, steps, delta_t):
        plt.subplot(4, 2, 6)
        plt.plot(t[:-1], u[1], "b", label="Zugkraft")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$F_\mathrm{T}$ in N", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color='black', lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        #plt.legend(loc=1, fontsize=10)


    def plot_average_force(self, t, x, steps, delta_t):
        plt.subplot(4, 2, 4)
        plt.plot(t, x[3], "b", label="Referenzmodell")
        plt.xlabel("$t$ in s", fontsize=18)
        plt.ylabel("$\\bar{F}$ in N", fontsize=18)
        plt.tick_params(axis='both', labelsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        #plt.legend(loc=1, fontsize=10)


    def plot_power(self, ReferenceModel, t, x, u, steps, delta_t):
        plt.subplot(4, 2, 8)
        # gravitational power
        reference_gravitational_power = self.MyReferenceModel.Parameters.gravitational_force * x[1]
        plt.plot(t, reference_gravitational_power, "b--", label="$P_\mathrm{grav}$")
 
        # tension power
        reference_tension_power = u[1] * x[1][:-1]
        plt.plot(t[:-1], reference_tension_power, "b-.", label="$P_\mathrm{Zug}$")

        # vertical power
        reference_vertical_power = (self.MyReferenceModel.Parameters.total_mass * np.diff(x[1]) / delta_t) * x[1][:-1]
        plt.plot(t[:-1], reference_vertical_power, "b:", label="$P_\mathrm{vert}$")

        # tangential power
        reference_tangential_power = np.diff(x[2]) * np.diff(x[2]) * self.MyReferenceModel.inertial_moment / delta_t
        plt.plot(t[:-1], reference_tangential_power, "b", linestyle=(0, (1, 3)), label="$P_\mathrm{tang}$")

        # total power
        reference_total_power = reference_gravitational_power[:-1] + reference_tension_power + reference_vertical_power + reference_tangential_power
        plt.plot(t[:-1], reference_total_power, "b-", label="$P_\mathrm{tot}$")

        # plot settings
        plt.legend(loc=1, fontsize=18)
        plt.axhline(0, color="black", lw=0.8, linestyle="dotted")
        plt.xlim(0, steps * delta_t)
        plt.xlabel('$t$ in s', fontsize=18)
        plt.ylabel('$P$ in W', fontsize=18)
        plt.tick_params(axis='both', labelsize=18)


    def generate_simulation_plot(self, t, x, u, steps, delta_t):
        plt.figure(figsize=(15, 15))
        self.plot_height(t, x, steps, delta_t)
        self.plot_vertical_speed(t, x, steps, delta_t)
        self.plot_rotation_speed(t, x, steps, delta_t)
        self.plot_energy(self.MyReferenceModel, t, x, steps, delta_t)
        self.plot_tension(t, u, steps, delta_t)
        self.plot_incidence_angle(t, u, steps, delta_t)
        self.plot_average_force(t, x, steps, delta_t)
        self.plot_power(self.MyReferenceModel, t, x, u, steps, delta_t)
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/SimulationPumping.png", dpi=500)
        plt.savefig("Simulation/Graphiken/SimulationPumping.svg")



##################################################################################################
##################################################################################################
class ParameterOptimizer:
    def __init__(self, RotorParameters):
        self.Parameters = RotorParameters
        self.Simulator = Simulation.Simulator()
        self.ReferenceModel = ReferenceModel.Rotor(self.Parameters)

        self.heights = np.array([10])
        self.vertical_velocities = np.linspace(-10, 10, 3)
        self.rotation_speeds = np.linspace(0.1, 10, 3)
        self.wake_speeds = np.array([0])
        self.angles = np.linspace(-30, 30, 2)
        self.tensions = np.linspace(-200, 0, 2)

        self.determine_optimal_parameters()


    def parameter_optimization_loss(self, estimated_parameters):
        print(estimated_parameters)
        # unpack parameters
        self.Parameters.estimated_lift_slope = estimated_parameters[0]
        self.Parameters.estimated_zero_lift_coefficient = estimated_parameters[1]
        self.Parameters.estimated_drag_parameter = estimated_parameters[2]
        self.Parameters.estimated_zero_drag_coefficient = estimated_parameters[3]

        # create models
        simple_rotor_model = BasisModel.Rotor(self.Parameters)
        reference_rotor_model = ReferenceModel.Rotor(self.Parameters)

        # calculate loss
        self.tension = np.full(200, 0) 
        self.incidence_angle = np.full(200, 0)
        self.u = np.column_stack((self.incidence_angle, self.tension))
        data_reference_model = self.Simulator.simulate_explicit_reference_model(reference_rotor_model, np.array([0, 0, 2, 10]), self.u, 0.001)[1]
        data_basis_model = self.Simulator.simulate_explicit_simple_model(simple_rotor_model, np.array([0, 0, 2, 10]), self.u, 0.001)[1]
        error =  mean_squared_error(data_reference_model, data_basis_model)
        print(error)
        return error


    def determine_optimal_parameters(self):
        print(sci.minimize(self.parameter_optimization_loss, self.Parameters.initial_estimation, bounds=[(0, np.inf)] * len(self.Parameters.initial_estimation)))



##################################################################################################
# Main                                                                                           #
##################################################################################################
#PlotSzenario(Parameters.SmallRotorParameters(), Parameters.SimulutionScenario(), Parameters.NeuralNetworkParameters(), "")
#PlotDifferentWakes(Parameters.MediumRotorParameters(), Parameters.SimulutionScenarioDifferentWakes(), "Neu")
#EvaluateModelQuality(Parameters.SmallRotorParameters(), Parameters.SimulutionScenarioRealisticPumping(), Parameters.NeuralNetworkParameters())
#FitReferenzModelParameters(Parameters.SmallRotorParameters(), Parameters.SimulutionScenarioMeasurement())
#PlotSzenario(Parameters.SmallRotorParameters(), Parameters.SimulutionScenarioMeasurement(), Parameters.NeuralNetworkParameters(), "")

#FitReferenzModelParameters(Parameters.SmallRotorParameters(), Parameters.SimulutionScenarioMeasurement())
#PlotSzenario(Parameters.MediumRotorParameters(), Parameters.SimulutionScenario(), Parameters.NeuralNetworkParameters(), "")
#PlotPumping(Parameters.MediumRotorParameters())
#EvaluateModelQuality(Parameters.MediumRotorNoWingsParameters(), Parameters.SimulutionValidationScenario(), Parameters.NeuralNetworkParameters())
PlotSzenario(Parameters.MediumRotorParameters(), Parameters.SimulutionScenarioPumpingModels(), Parameters.NeuralNetworkParameters(), "AllModelsPumping")
#ParameterOptimizer(Parameters.MediumRotorParameters())



#PlotSzenario(Parameters.MediumRotorNoWingsParameters(), Parameters.SimulutionValidationScenario(), Parameters.NeuralNetworkParameters(), "NoWings")
#PlotSzenario(Parameters.MediumRotorNoAirParameters(), Parameters.SimulutionValidationScenario(), Parameters.NeuralNetworkParameters(), "NoAir")
#EvaluateModelQuality(Parameters.MediumRotorNoWingsParameters(), Parameters.SimulutionValidationScenario(), Parameters.NeuralNetworkParameters())
#PlotPumping(Parameters.MediumRotorNoWingsParameters())