import pandas as pd
import matplotlib.pyplot as plt

# Load the data from the CSV file
data = pd.read_csv("/Users/kruegepa/Desktop/Universität/6. Semester/Bachelorarbeit/Simulation/Code/topdata.csv")

# Extract the x, y, and z values from the data
x = data["x"]
y = data["y"]
z = data["z"]

# Plot the data
plt.figure(figsize=(15, 4))
plt.plot(x, y, label="y", color="black", linewidth=2.5)
plt.plot(x, z, label="z", color="black", linewidth=2.5)
plt.tick_params(axis='both', labelsize=26)
plt.xticks([])
plt.yticks([])
plt.xlabel("Breite", fontsize=26)
plt.ylabel("Höhe", fontsize=26)


# Display the plot
plt.tight_layout()
plt.savefig("Simulation/Graphiken/Wing.png", dpi=500)
plt.savefig("Simulation/Graphiken/Wing.svg")
