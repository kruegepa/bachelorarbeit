# imports
import numpy as np
import casadi as ca
import matplotlib.pyplot as plt
import Parameters
import ReferenceModelCasadi


# define states, input and ode
RotorModel = ReferenceModelCasadi.Rotor(Parameters.MediumRotorParameters())
x = ca.SX.sym('x', 4)
u = ca.SX.sym('u', 2)
x_dot = RotorModel.system_differential_equation(x, u)
ode_func = ca.Function('ode', [x, u], [x_dot])


# simulation parameters
x0 = ca.DM([0, -1.5, 15, 10])
#u = ca.DM([0, 0])
dt = 0.001
num_steps = 500


# perform integration
def rk4(x, u):
    k1 = ode_func(x, u)
    k2 = ode_func(x + 0.5 * dt * k1, u)
    k3 = ode_func(x + 0.5 * dt * k2, u)
    k4 = ode_func(x + dt * k3, u)
    return x + (dt / 6.0) * (k1 + 2 * k2 + 2 * k3 + k4)

rk4 = ca.Function('rk4', [x, u], [rk4(x,u)])
opti = ca.Opti()
x0_bar = ca.DM([0, -1.5, 15, 10])


X = opti.variable(4,num_steps)
opti.set_initial(X, np.tile(x0_bar, num_steps))
U = opti.variable(2,num_steps-1)
x0 = X[:,0]


for k in range(num_steps-1):
    xk_next = X[:,k+1]
    uk = U[:, k]
    xk = X[:, k]
    # side calculations
    delta_angle = U[0, k-1] - U[0, k]
    delta_tension = U[1, k-1] - U[1, k]

    # high priority constrains
    opti.subject_to(xk[2] > 0)                          # high priority constrain: realistic state
    opti.subject_to(uk[0] > -30)                        # high priority constrain: realistic input
    opti.subject_to(uk[0] < 30)                         # high priority constrain: realistic input
    opti.subject_to(uk[1] <= 0)                         # high priority constrain: realistic input
    opti.subject_to(uk[1] >= -200)                      # high priority constrain: realistic input
    opti.subject_to(rk4(xk,uk) == xk_next)              # high priority constrain: must fulfill ODE


# side calculations
err_x = X[:,0] - X[:,-1]
err_u = U[:,0] - U[:,-1]


# high priority constrains
opti.subject_to(x0[0] == x0_bar[0])                     # high priority constrain: initial state
opti.subject_to(err_x[0]==0)                            # high priority constrain: cyclic states                     # medium priority constrain: cyclic states
opti.subject_to(err_x[1]==0)  
opti.subject_to(err_x[2]==0)  
opti.minimize(ca.power(err_x[3], 2))

# objective function
# error = ca.sum1(ca.power(err_x, 2))
# e1 = (X[0,0] - X[0,-1]) / X[0,0]



# solve
opti.solver("ipopt")
sol = opti.solve()
x_sol = sol.value(X)
u_sol = sol.value(U)

np.save("datax.npy", x_sol)
np.save("datau.npy", u_sol)

# plot
plt.figure(figsize=(12, 6))
plt.subplot(1, 6, 1)
plt.plot(x_sol[0], label="Höhe")
plt.legend()
plt.subplot(1, 6, 2)
plt.plot(x_sol[1], label="vert. Geschwindigkeit")
plt.legend()
plt.subplot(1, 6, 3)
plt.plot(x_sol[2], label="Rotationsgeschwindigkeit")
plt.legend()
plt.subplot(1, 6, 4)
plt.plot(x_sol[3], label="mittlere Kraft")
plt.subplot(1, 6, 5)
plt.legend()
plt.plot(u_sol[0], label="Winkel")
plt.subplot(1, 6, 6)
plt.legend()
plt.plot(u_sol[1], label="Kraft")
plt.legend()
plt.show()


# # plot data
# t = range(len(res))

# x1 = [x[0] for x in res]
# x2 = [x[1] for x in res]
# x3 = [x[2] for x in res]
# x4 = [x[3] for x in res]

# plt.plot(t, np.ravel(x1), label='Höhe')
# plt.plot(t, np.ravel(x2), label='Geschwindigkeit')
# plt.plot(t, np.ravel(x3), label='Rotationsgeschwindigkeit')
# plt.plot(t, np.ravel(x4), label='mittlere Kraft')

# plt.xlabel('Zeit')
# plt.ylabel('Wert')
# plt.legend()
# plt.show()
