import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import NeuralNetwork
import BasisModel as SimpleModel
import ReferenceModel
import Parameters
import Simulation
import scipy.optimize as sci
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split


class Evaluation:
    def __init__(self):
        # rotor
        self.Parameters = Parameters.MediumRotorParameters()
        self.Simulation = Parameters.NeuralNetworkParameters()

        # simulation
        self.steps = 1000
        self.delta_t = 0.001

        # initial states
        self.initial_height = 5
        self.initial_vertical_speed = 0
        self.initial_rotation_speed = 5
        self.initial_state = [self.initial_height, self.initial_vertical_speed, self.initial_rotation_speed]

        # input
        self.incidence_angle = np.full(self.steps, 0)
        self.tension = np.full(self.steps, 0)
        self.input = np.column_stack((self.incidence_angle, self.tension))
        self.wakes = np.linspace(-10, 10, 5)

        # neural network training
        self.nn_number_nodes = 80
        self.nn_activation_function = "ReLU"
        self.nn_number_epochs = 5
        self.nn_number_layers = 3
        self.nn_data_size = 100000
        self.nn_learning_rate = 1e-4

        # heat map
        self.vertical_speeds = np.linspace(-10, 10, 3)
        self.rotation_speeds = np.linspace(0.5, 10, 3)
        self.incidence_angles = np.linspace(-20, 0, 3)
        self.tensions = np.linspace(0, 1000, 3)
        self.trainings = np.arange(1, 20, 1)
        self.n = 10

        # plots
        self.figure_size = (12, 4)

        # train model
        self.train_number_nodes = 80
        self.train_activation_function = "ReLU"
        self.train_number_layers = 3
        self.train_number_epochs = 1
        self.train_learning_rate = 1e-4
        self.train_training_size = 10000

        # learning curve
        self.lr_number_training_cases = 10000
        self.lr_trainings = np.array([10, 100, 200, 300, 400, 500])
        self.lr_number_nodes = 80
        self.lr_activation_function = "ReLU"
        self.lr_number_layers = 3
        self.lr_number_epochs = 250
        self.lr_learning_rate = 1e-3

        # grid search
        self.gs_number_training_cases = 1000
        self.gs_number_layers = np.array([2, 3])
        self.gs_number_nodes = np.array([20, 50, 80])
        self.gs_number_epochs = np.array([50, 100, 250])
        self.gs_learning_rates = np.array([1e-4, 1e-3])
        self.gs_activation_functions = np.array(["ReLU", "sigmoid"])
        
        # optimal parameters simple model
        self.op_heights = np.arange(-10, 10, 2)
        self.op_vertical_velocities = np.arange(-10, 10, 2)
        self.op_rotation_speeds = np.arange(1, 100, 40) / 10
        self.op_angles = np.arange(-20, 0, 5)
        self.op_tensions = np.arange(0, 1000, 300)
        self.iterations = []
        self.losses = []

        # machine learning
        self.training_size = 10000
        self.number_nodes = np.array([1, 10, 50])
        self.activation_function = "ReLU"
        self.number_epochs = np.array([1, 50, 100, 200])
        self.number_layers = np.array([1, 2, 3])
        self.learning_rates = np.array([1e-5, 1e-4, 1e-3])


    def calculate_loss(self, get_prediction, get_ground_truth, vertical_speed, rotation_speed):
        losses = []
        x = np.array([self.initial_height, vertical_speed, rotation_speed])

        for angle in self.incidence_angles:
            for tension in self.tensions:
                for z in self.wakes:
                    u = np.array([angle, tension])
                    prediction = get_prediction(x, u, z)
                    ground_truth = get_ground_truth(x, u, z)
                    losses.append(mean_squared_error(prediction, ground_truth))

        return np.mean(losses)


    def plot_neural_network_heatmap(self, neural_network_model, reference_model):
        plt.figure(figsize=(7, 5))
        vertical_speeds, rotation_speeds = np.meshgrid(self.vertical_speeds, self.rotation_speeds)
        losses = np.vectorize(self.calculate_loss)(neural_network_model.predict, reference_model.system_differential_equation, vertical_speeds, rotation_speeds)
        plt.pcolormesh(vertical_speeds, rotation_speeds, losses, cmap="coolwarm")
        plt.colorbar()
        plt.xlabel('vertikale Geschwindigkeit $\dot{z}$ in m/s', fontsize=10)
        plt.ylabel('Rotationsgeschwindigkeit $\Omega$ in U/s', fontsize=10)
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/NeuralNetworkHeatMap.png", dpi=500)
        plt.savefig("Simulation/Graphiken/NeuralNetworkHeatMap.svg")
        plt.show()


    def plot_simple_model_heatmap(self, simple_model, reference_model):
        plt.figure(figsize=(7, 5))
        vertical_speeds, rotation_speeds = np.meshgrid(self.vertical_speeds, self.rotation_speeds)
        losses = np.vectorize(self.calculate_loss)(simple_model.system_differential_equation, reference_model.system_differential_equation, vertical_speeds, rotation_speeds)
        plt.pcolormesh(vertical_speeds, rotation_speeds, losses, cmap="coolwarm")
        plt.colorbar()
        plt.xlabel('vertikale Geschwindigkeit $\dot{z}$ in m/s', fontsize=10)
        plt.ylabel('Rotationsgeschwindigkeit $\Omega$ in U/s', fontsize=10)
        plt.tight_layout()
        plt.savefig("Simulation/Graphiken/SimpleModelHeatMap.png", dpi=500)
        plt.savefig("Simulation/Graphiken/SimpleModelHeatMap.svg")
        plt.show()


    def plot_data(self, simple_rotor_model, reference_rotor_model, neural_network_model, x_0, U, delta_t, steps, name):
        # generate data
        Simulator = Simulation.Simulator()
        data_simple_model, data_reference_model, data_neural_network_model = self.get_simulation_data(Simulator, simple_rotor_model, reference_rotor_model, neural_network_model, x_0, U, delta_t)
        
        data_reference_model = Simulator.simulate_implicit_reference_model(reference_rotor_model, x_0, U, delta_t)
        data_simple_model = Simulator.simulate_explicit_simple_model(simple_rotor_model, x_0, U, delta_t)
        data_neural_network_model = Simulator.simulate_explicit_neuronal_network_model(neural_network_model, x_0, U, delta_t)

        # generate plot
        plt.figure(figsize=(12, 8))
        self.plot_height(data_simple_model, data_reference_model, data_neural_network_model, steps, delta_t)
        self.plot_vertical_speed(data_simple_model, data_reference_model, data_neural_network_model, steps, delta_t)
        self.plot_rotation_speed(data_simple_model, data_reference_model, data_neural_network_model, steps, delta_t)
        self.plot_energy(simple_rotor_model, reference_rotor_model, data_simple_model, data_reference_model, data_neural_network_model, steps, delta_t)
        self.plot_incidence_angle(data_simple_model, U, steps, delta_t)
        self.plot_tension(data_simple_model, U, steps, delta_t)
        plt.tight_layout()
        plt.savefig("Simulation/Plots/Simulation" + name + ".png", dpi=500)
        plt.show()

    
    def get_simulation_data(self, Simulator, SimpleRotorModel, ReferenceRotorModel, NeuralNetworkModel, initial_state, input, delta_t):
        data_simple_model = Simulator.simulate_explicit_simple_model(SimpleRotorModel, initial_state, input, delta_t)
        data_reference_model = Simulator.simulate_implicit_reference_model(ReferenceRotorModel, initial_state, input, delta_t)
        data_neural_network_model = Simulator.simulate_explicit_neuronal_network_model(NeuralNetworkModel, initial_state, input, delta_t)
        return [data_simple_model, data_reference_model, data_neural_network_model]


    def prepare_models(self):
        # create objects
        ReferenceRotorModel = ReferenceModel.Rotor(Eval.Parameters)
        SimpleRotorModel = SimpleModel.Rotor(Eval.Parameters)
        Simulator = Simulation.Simulator()
        NeuralNetworkModel = NeuralNetwork.Model(Eval.Parameters, Eval.Simulation, ReferenceRotorModel)

        # train neural network
        #NeuralNetworkModel.create_model(self.nn_number_nodes, self.nn_activation_function, self.nn_number_layers)
        #Simulator.create_training_data(ReferenceRotorModel, self.nn_data_size)
        #X_train, y_train = Simulator.get_training_data()
        #NeuralNetworkModel.fit_model(X_train, y_train, self.nn_number_epochs, self.nn_learning_rate)

        return [ReferenceRotorModel, SimpleRotorModel, NeuralNetworkModel]

    
    def plot_neural_network_learning_curve(self, reference_rotor_model, number_training_cases, number_trainings, number_nodes, activation_function, number_layers, number_epochs, learning_rate):
        # generate training data
        Simulator = Simulation.Simulator()
        X, y = Simulator.get_training_data(reference_rotor_model, number_training_cases)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        # calculate loss for different trainings set sizes
        losses = pd.DataFrame(columns=["set_size", "train_loss", "test_loss"])

        for trainings in number_trainings:
            end = len(X_train) * trainings // np.max(trainings)
            Model = NeuralNetwork.Model()
            Model.create_model(number_nodes, activation_function, number_layers)
            Model.fit_model(X_train[:end], y_train[:end], number_epochs, learning_rate)
            train_loss, test_loss = Model.evaluate_model(X_train[:end], y_train[:end], X_test, y_test)
            losses.loc[len(losses)] = [trainings, train_loss, test_loss]
        
        # create plot
        plt.figure(figsize=self.figure_size)
        ax = losses.plot(x="set_size", y=["train_loss", "test_loss"], kind='line')
        ax.set_xlabel("Anzahl an Trainingsszenarien")
        ax.set_ylabel("Mean Squared Error")
        plt.tight_layout()
        plt.savefig("Simulation/Plots/NeuralNetworkLearningCurve.png", dpi=500)
        plt.show()


    def grid_search(self, reference_rotor_model, number_training_cases, activation_function, number_layers, number_nodes, number_epochs, learning_rates):
        # generate training data
        Simulator = Simulation.Simulator()
        X, y = Simulator.get_training_data(reference_rotor_model, number_training_cases)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        # create table
        data = pd.DataFrame(columns=["number_layers", "number_nodes", "score"])

        # try every combination of layers and nodes
        for layers in number_layers:
            for nodes in number_nodes:
                best_score = np.inf
                best_parameters = []
                for epochs in number_epochs:
                    for learning_rate in learning_rates:
                        neural_network_model = NeuralNetwork.Model()
                        neural_network_model.create_model(nodes, activation_function, layers)
                        neural_network_model.fit_model(X_train, y_train, epochs, learning_rate)
                        score = neural_network_model.evaluate_model(X_train, y_train, X_test, y_test)[1]
                        if score < best_score:
                            best_score = score
                            best_parameters = [layers, nodes, score]
                data.loc[len(data)] = best_parameters

        # return data
        return data


    def plot_loss_by_activation_layers_nodes(self, reference_rotor_model, number_training_cases, number_layers, number_nodes, number_epochs, learning_rates):
        # generate data
        data_relu = self.grid_search(reference_rotor_model, number_training_cases, "ReLU", number_layers, number_nodes, number_epochs, learning_rates)
        data_sigmoid = self.grid_search(reference_rotor_model, number_training_cases, "sigmoid", number_layers, number_nodes, number_epochs, learning_rates)
        data_tanh = self.grid_search(reference_rotor_model, number_training_cases, "tanh", number_layers, number_nodes, number_epochs, learning_rates)

        # create figure
        _, axs = plt.subplots(ncols=3, figsize=(12, 4))

        # plot relu data
        data_relu.plot(x="number_layers", y="number_nodes", c="score", kind="scatter", cmap="coolwarm", ax=axs[0])
        axs[0].set_xlabel("Anzahl an Schichten")
        axs[0].set_ylabel("Anzahl an Knoten")
        axs[0].set_title("ReLU-Aktivierung")

        # plot sigmoid data
        data_sigmoid.plot(x="number_layers", y="number_nodes", c="score", kind="scatter", cmap="coolwarm", ax=axs[1])
        axs[1].set_xlabel("Anzahl an Schichten")
        axs[1].set_ylabel("Anzahl an Knoten")
        axs[1].set_title("Sigmoid-Aktivierung")

        # plot tanh data
        data_tanh.plot(x="number_layers", y="number_nodes", c="score", kind="scatter", cmap="coolwarm", ax=axs[2])
        axs[2].set_xlabel("Anzahl an Schichten")
        axs[2].set_ylabel("Anzahl an Knoten")
        axs[2].set_title("Tanh-Aktivierung")

        # plot settings
        plt.tight_layout()
        plt.savefig("Simulation/Plots/LossLayersNodesActivation.png", dpi=500)
        plt.show()


    def determine_best_hyperparameters(self, reference_rotor_model, number_training_cases, number_layers, number_nodes, activation_functions, number_epochs, learning_rates):
        # generate training data
        Simulator = Simulation.Simulator()
        X, y = Simulator.get_training_data(reference_rotor_model, number_training_cases)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        
        # grid search
        best_parameters = [] 
        lowest_loss = np.inf
        for layers in number_layers:
            for nodes in number_nodes:
                for activation_function in activation_functions:
                    for epochs in number_epochs:
                        for learning_rate in learning_rates:
                            neural_network_model = NeuralNetwork.Model()
                            neural_network_model.create_model(nodes, activation_function, layers)
                            neural_network_model.fit_model(X_train, y_train, epochs, learning_rate)
                            loss = neural_network_model.evaluate_model(X_train, y_train, X_test, y_test)[1]
                            if loss < lowest_loss:
                                lowest_loss = loss
                                best_parameters = [learning_rate, layers, nodes, activation_function, epochs, loss]
        return best_parameters


    def parameter_optimization_loss(self, estimated_parameters):
        # unpack parameters
        self.Parameters.estimated_slope_lift_coefficient = estimated_parameters[0]
        self.Parameters.estimated_zero_lift_coefficient = estimated_parameters[1]
        self.Parameters.estimated_zero_drag_coefficient = estimated_parameters[2]

        # create models
        simple_rotor_model = SimpleModel.Rotor(self.Parameters)
        get_prediction = simple_rotor_model.system_differential_equation
        reference_rotor_model = ReferenceModel.Rotor(self.Parameters)
        get_ground_truth = reference_rotor_model.system_differential_equation

        # calculate loss
        losses = []
        for height in self.op_heights:
            for vertical_velocity in self.op_vertical_velocities:
                for rotation_speed in self.op_rotation_speeds:
                    for angle in self.op_angles:
                        for tension in self.op_tensions:
                            x = np.array([height, vertical_velocity, rotation_speed])
                            u = np.array([angle, tension])
                            prediction = get_prediction(x, u)
                            ground_truth = get_ground_truth(x, u)
                            losses.append(mean_squared_error(prediction, ground_truth))
        return np.mean(losses)


    def callback(self, estimated_parameters):
        self.iterations.append(len(self.iterations)+1)
        self.losses.append(self.parameter_optimization_loss(estimated_parameters))


    def plot_simple_model_learning_curve(self):
        print(sci.minimize(self.parameter_optimization_loss, self.Parameters.initial_estimation, bounds=[(0, np.inf)] * len(self.Parameters.initial_estimation), callback=self.callback))
        plt.figure(figsize=self.figure_size)
        plt.plot(self.iterations, self.losses, "x")
        plt.xlabel("Anzahl an Optimierungsschritten")
        plt.ylabel("Mean Squared Error")
        plt.tight_layout()
        plt.savefig("Simulation/Plots/SimpleModelLearningCurve.png", dpi=500)
        plt.show()








# MAIN
Eval = Evaluation()
ReferenceRotorModel, SimpleRotorModel, NeuralNetworkModel = Eval.prepare_models()
#Eval.plot_neural_network_heatmap(NeuralNetworkModel, ReferenceRotorModel)
Eval.plot_simple_model_heatmap(SimpleRotorModel, ReferenceRotorModel)
#Eval.plot_data(SimpleRotorModel, ReferenceRotorModel, NeuralNetworkModel, Eval.initial_state, Eval.input, Eval.delta_t, Eval.steps, "")
#Eval.plot_neural_network_learning_curve(ReferenceRotorModel, Eval.lr_number_training_cases, Eval.lr_trainings, Eval.lr_number_nodes, Eval.lr_activation_function, Eval.lr_number_layers, Eval.lr_number_epochs, Eval.lr_learning_rate)
#Eval.plot_loss_by_activation_layers_nodes(ReferenceRotorModel, Eval.gs_number_training_cases, Eval.gs_number_layers, Eval.gs_number_nodes, Eval.gs_number_epochs, Eval.gs_learning_rates)
#print(Eval.determine_best_hyperparameters(ReferenceRotorModel, Eval.gs_number_training_cases, Eval.gs_number_layers, Eval.gs_number_nodes, Eval.gs_activation_functions, Eval.gs_number_epochs, Eval.gs_learning_rates))
#print(Eval.plot_simple_model_learning_curve())
#Eval.plot_test_case_small_wing_area()
#Eval.plot_test_case_no_wings()
#Eval.plot_test_case_small_zero_drag_coefficient()
#Eval.plot_test_case_high_zero_drag_coefficient()
#Eval.plot_test_case_small_input_angle()
#Eval.plot_test_case_high_input_angle()
#Eval.plot_explicit_implicit_data(ReferenceRotorModel, Eval.initial_state, Eval.input, Eval.steps, Eval.delta_t)
