import numpy as np
import keras
import ReferenceModel
import os
import Simulation
from keras.models import Sequential
from keras.layers import Dense
from keras.models import load_model
from sklearn.model_selection import train_test_split
import Parameters
import timeit


class Model:
    def __init__(self, RotorParameters, NeuralNetworkParameters, ReferenceModel):
        # settings
        self.ReferenceModel = ReferenceModel
        self.RotorParameters = RotorParameters
        self.NeuralNetworkParameters = NeuralNetworkParameters

        # create file name
        file_name = self.generate_file_name(self.NeuralNetworkParameters.number_nodes, self.NeuralNetworkParameters.activation_function, self.NeuralNetworkParameters.number_layers, self.NeuralNetworkParameters.number_epochs, self.NeuralNetworkParameters.learning_rate, self.RotorParameters.number_wings, self.RotorParameters.wing_area, self.RotorParameters.wing_wide, self.RotorParameters.air_density)

        # try to load existing model
        if os.path.exists(file_name):
            self.load_model(file_name)
        else:
            data_name = file_name[:-2] + "txt"
            self.get_training_test_data(data_name)
            self.create_model(self.NeuralNetworkParameters.number_nodes, self.NeuralNetworkParameters.activation_function, self.NeuralNetworkParameters.number_layers)
            self.fit_model(self.X_train, self.y_train, self.NeuralNetworkParameters.number_epochs, self.NeuralNetworkParameters.learning_rate)
            self.save_model(file_name)


    def generate_file_name(self, number_nodes, activation_function, number_layers, number_epochs, learning_rate, number_wings, wing_area, wing_width, air_density):
        parameter_labels = {
            "number_nodes": "NumberOfNodes",
            "activation_function": "ActivationFunction",
            "number_layers": "NumberOfLayers",
            "number_epochs": "NumberOfEpochs",
            "learning_rate": "LearningRate",
            "number_wings": "NumberOfWings",
            "wing_area": "WingArea",
            "wing_width": "WingWidth",
            "air_density": "AirDensity"
        }

        name_parts = []
        input = locals().items()
        for param_name, param_value in input:
            if param_name in parameter_labels:
                label = parameter_labels[param_name]
                name_parts.append(f"{label}{param_value}")

        file_name = "".join(name_parts) + ".h5"
        file_path = os.path.join("Simulation", "Daten", file_name)
        return file_path


    def create_model(self, number_nodes, activation_function, number_layers):
        self.model = Sequential()
        self.model.add(Dense(number_nodes, input_shape=[self.NeuralNetworkParameters.input_size], activation=activation_function))
        for i in range(number_layers):
            self.model.add(Dense(number_nodes, activation=activation_function))
        self.model.add(Dense(self.NeuralNetworkParameters.output_size, activation="linear"))

    
    def fit_model(self, X_train, y_train, number_epochs, learning_rate):
        optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
        self.model.compile(loss="mse", optimizer=optimizer, metrics=['mse'])
        self.model.fit(X_train, y_train, epochs=number_epochs)


    def evaluate_model(self, X_train, y_train, X_test, y_test):
        train_loss = self.model.evaluate(X_train, y_train)[0]
        test_loss = self.model.evaluate(X_test, y_test)[1]
        return [train_loss, test_loss]


    def predict(self, x, u):
        return self.model.predict(np.array([np.hstack((x, u))]))[0]


    def save_model(self, file_name):
        self.model.save(file_name)


    def load_model(self, file_name):
        self.model = load_model(file_name)


    def get_training_test_data(self, data_file_name):
        if not os.path.exists(data_file_name):
            Sim = Simulation.Simulator()
            Sim.create_training_data(self.ReferenceModel, self.NeuralNetworkParameters.training_scenarios, data_file_name)

        data = np.loadtxt(data_file_name, delimiter=",")
        XU = data[:, :6]
        dX = data[:, 6:]
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(XU, dX, test_size=0.2, random_state=42)



MyParameter = Parameters.MediumRotorParameters()
NeuralParameter = Parameters.NeuralNetworkParameters()
ReferenceModel = ReferenceModel.Rotor(MyParameter)
#MyRotor = Model(MyParameter, NeuralParameter, ReferenceModel)
x = np.array([0, -1.5, 15, 10])
u = np.array([10, -20])
n = 1

def time_predict():
    MyRotor = Model(MyParameter, NeuralParameter, ReferenceModel)

ausfuehrungszeit = timeit.timeit(time_predict, number=n) / n # Funktion 10 Mal ausführen

print("Die Ausführungszeit betrug:", ausfuehrungszeit, "Sekunden")
