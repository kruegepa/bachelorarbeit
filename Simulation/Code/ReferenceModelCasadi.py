
import casadi as ca
import Parameters
import timeit


class Rotor:
    def __init__(self, Parameters):
        # import parameters
        self.Parameters = Parameters

        # side calculations
        self.inertial_center = (ca.power(self.Parameters.distance_rotor_hub, 3) / 3) * (self.Parameters.rotor_hub_mass / self.Parameters.distance_rotor_hub)
        self.inertial_stick = (ca.power(self.Parameters.distance_wing_start, 3) / 3) * (self.Parameters.stick_mass / (self.Parameters.distance_wing_start - self.Parameters.distance_rotor_hub)) - (ca.power(self.Parameters.distance_rotor_hub, 3) / 3) * (self.Parameters.rotor_hub_mass / self.Parameters.distance_rotor_hub)
        self.inertial_blade = (ca.power(self.Parameters.distance_wing_end, 3) / 3) * (self.Parameters.wing_mass / (self.Parameters.distance_wing_end - self.Parameters.distance_wing_start)) - (ca.power(self.Parameters.distance_wing_start, 3) / 3) * (self.Parameters.stick_mass / (self.Parameters.distance_wing_start - self.Parameters.distance_rotor_hub))
        self.inertial_moment = self.Parameters.number_wings * (self.inertial_center + self.inertial_stick + self.inertial_blade)
        self.r_max = 0.97 * self.Parameters.distance_wing_end
        self.rotor_area = ca.pi * ca.power(self.r_max, 2)
                       
        # variables
        self.gamma = 0
        self.alpha = 0
        self.lift_coefficient = 0
        self.drag_coefficient = 0
        self.wind_velocity = 0
        self.infinitesimal_thrust = 0
        self.thrust = 0
        self.moment = 0

    
    def get_vertical_velocity(self, x, u):
        return x[1]


    def get_states(self, x, u):
        return x


    def get_lift_coefficient(self, x, u):
        self.lift_coefficient = self.Parameters.max_lift_coefficient * self.gamma + self.Parameters.zero_lift_coefficient
        return self.lift_coefficient


    def get_drag_coefficient(self, x, u):
        self.drag_coefficient = ca.power(self.gamma, 2) * self.Parameters.max_drag_coefficient + self.Parameters.zero_drag_coefficient
        return self.drag_coefficient


    def get_wake_velocity_rotor(self, x, u):
        numerator = ca.if_else(x[3] > 0, x[3], -x[3])
        coefficient = ca.if_else(x[3] > 0, -1, 1)
        air_density_check = ca.if_else(self.Parameters.air_density < 1e-5, 0, 1)
        denominator = 2 * self.Parameters.air_density * self.rotor_area
        wake_velocity_rotor = coefficient * air_density_check * ca.sqrt(numerator / denominator)
        return wake_velocity_rotor

        # numerator = ca.if_else(x[3] >= 0, x[3], 0)
        # denominator = 2 * self.Parameters.air_density * self.rotor_area
        # wake_velocity_rotor = -ca.sqrt(numerator / denominator)
        # return wake_velocity_rotor


    def get_alpha(self, x, u, r):
        self.alpha = -180.0 / ca.pi * ca.atan((x[1] - self.get_wake_velocity_rotor(x, u)) / (x[2] * r))
        return self.alpha

    
    def get_wind_velocity(self, x, u, r):
        wind_velocity = ca.sqrt(ca.power(x[2] * r, 2) + ca.power(x[1] - self.get_wake_velocity_rotor(x, u), 2))
        return wind_velocity


    def get_gamma(self, x, u, r):
        self.gamma = self.alpha - u[0]
        return self.gamma


    def get_pulling_force(self, x, u):
        return u[1]


    def integral_thrust(self, r, delta_r):
        wind_velocity = self.get_wind_velocity(self.x, self.u, r)
        alpha = self.get_alpha(self.x, self.u, r)
        gamma = self.get_gamma(self.x, self.u, r)
        lift = self.get_lift_coefficient(gamma, self.u) * ca.cos(alpha * ca.pi / 180.0) * ca.power(wind_velocity, 2) * self.Parameters.wing_wide * delta_r
        drag = self.get_drag_coefficient(gamma, self.u) * ca.sin(alpha * ca.pi / 180.0) * ca.power(wind_velocity, 2) * self.Parameters.wing_wide * delta_r
        self.infinitesimal_thrust = lift + drag
        return self.infinitesimal_thrust


    def integral_moment(self, r, delta_r):
        wind_velocity = self.get_wind_velocity(self.x, self.u, r)
        alpha = self.get_alpha(self.x, self.u, r)
        gamma = self.get_gamma(self.x, self.u, r)
        lift = self.get_lift_coefficient(gamma, self.u) * ca.sin(alpha * ca.pi / 180.0) * ca.power(wind_velocity, 2) * self.Parameters.wing_wide * delta_r
        drag = self.get_drag_coefficient(gamma, self.u) * ca.cos(alpha * ca.pi / 180.0) * ca.power(wind_velocity, 2) * self.Parameters.wing_wide * delta_r
        self.moment = r * (lift - drag)
        return self.moment

    
    def my_integrator(self, func, r_start, r_stop, n_steps):
        result = 0
        step_size = (r_stop - r_start) / n_steps

        for i in range(n_steps+1):
            r = r_start + i * step_size
            result += func(r, step_size)

        return result


    def get_rotor_force(self, x, u):
        rotor_force = 1/2 * self.Parameters.number_wings * self.Parameters.air_density * self.my_integrator(self.integral_thrust, self.Parameters.distance_wing_start, self.r_max, 100)
        return rotor_force

    
    def system_differential_equation(self, x, u):
        # update states, input and wake
        self.x = x
        self.u = u

        # side calculation
        self.thrust = 1/2 * self.Parameters.number_wings * self.Parameters.air_density * self.my_integrator(self.integral_thrust, self.Parameters.distance_wing_start, self.r_max, 100)
        self.moment = 1/2 * self.Parameters.number_wings * self.Parameters.air_density * self.my_integrator(self.integral_moment, self.Parameters.distance_wing_start, self.r_max, 100)
        average_force = x[3]
        instantaneous_force = self.thrust
        wake_velo = self.get_wake_velocity_rotor(x, u)

        # calculate change of state
        delta_high = self.get_vertical_velocity(x, u)
        delta_vertical_speed = (self.thrust + self.Parameters.gravitational_force + self.get_pulling_force(x, u)) / self.Parameters.total_mass
        delta_rotation_speed = self.moment / self.inertial_moment
        delta_average_force = (instantaneous_force - average_force) / self.Parameters.T

        return ca.vertcat(delta_high, delta_vertical_speed, delta_rotation_speed, delta_average_force)



MyParameter = Parameters.MediumRotorParameters()
MyRotor = Rotor(MyParameter)
x = ca.DM([0, -1.5, 15, 10])
u = ca.DM([10, -20])
n = 1000

def calculate_system_differential_equation():
    return MyRotor.system_differential_equation(x, u)

ausfuehrungszeit = timeit.timeit(calculate_system_differential_equation, number=n) / n # Funktion 10 Mal ausführen

print("Die Ausführungszeit betrug:", ausfuehrungszeit, "Sekunden")
