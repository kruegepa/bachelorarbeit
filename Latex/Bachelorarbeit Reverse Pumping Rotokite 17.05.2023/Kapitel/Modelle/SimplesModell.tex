\subsection{Grundmodell} \label{SimpleModell}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Allgemein
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Allgemein} % Wofür wird das Modell benötigt?
    Ziel dieses Abschnitts ist es ein Modell herzuleiten, welches die Änderung der Rotorzustände beschreibt. Gesucht ist eine ODE welche zur Simulation des Rotorverhaltens verwendet werden kann. Zur Simulation des IVPs kann das im Abschnitt~\ref{theorie:simulation} beschriebene vierstufige Runge-Kutta-Verfahren verwendet werden. Das Modell basiert auf grundlegenden Kenntnissen der Physik von Drehflüglern und geht mit zahlreichen Annahmen einher, die im nächsten Abschnitt erläutert werden.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Methode und Annahmen
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Methode und Annahmen} % Welche Annahmen werden getroffen?
    Im Folgenden werden zahlreiche Annahmen getroffen, um die Modellierung zu erleichtern, wodurch aber auch die Genauigkeit des Modells nachlässt. Da sich Fehler bei der Simulation summieren, können bereits kleine Fehler bei der Vorhersage der Zustandsänderung nach wenigen Sekundenbruchteilen zu großen Abweichungen von der Realität führen. \\

    % Rotorannahmen
    Für den Rotor wird angenommen, dass die tangentiale Geschwindigkeit~$v_\mathrm{t}$ unabhängig vom Radius~$r$ ist und überall der Geschwindigkeit beim halben Radius~$\tilde{r}$ mit $\tilde{r}=0,5\cdot r_\mathrm{Blatt}$ entspricht:

    \begin{equation}
    v_\mathrm{t} = \Omega \cdot \tilde{r} \mathrm{.}
    \end{equation}

    Der Rotor wird durch eine infitesimal dünne Scheibe approximiert, welche eine Druckdifferenz erzeugt \cite{van2015grundlagen}. Weiter wird davon ausgegangen, dass über den gesamten Rotorradius Schub erzeugt wird. Es wird somit ignoriert, dass der Rotor neben den Rotorblättern auch eine Rotornabe und Stäbe enthält, welche keinen Schub erzeugen. Die Form und Anzahl der Rotorblätter wird ebenfalls unbeachtet belassen. Zusätzlich wird außen vor gelassen, dass der Rotor einen Luftstrom generiert, also $v_\mathrm{abw}=0$. \\

    % Fliudannahmen
    Außerdem wird angenommen, dass das Fluid, welches den Rotor umströmt eindimensional, qusistationär, inkompresibel und reibungsfrei sei. Innerhalb von Ebenen, welche parallel zum Rotor ausgerichtet sind, sollen außerdem identische Verhältnisse gelten. Zusätzlich wird angenommen, dass sich die Strömung nicht mit der Zeit ändert \cite{van2015grundlagen}. \\


    % Masseannahmen
    Eine weitere Annahme ist, dass der Rotor das Trägheitsmoment~$J$ eines Zylinders besitzt:

    \begin{equation}
    J = \frac{1}{2} \cdot m \cdot \tilde{r}^2 \mathrm{.}
    \end{equation}

    Die Dichte des Rotors wird ebenso über den gesamten Radius als konstant angenommen.

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Herleitung der Systemgleichung
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Herleitung der Systemgleichung}
    % Zustände
    Zunächst muss untersucht werden, welche Zustände~$x$ erforderlich sind, um den Rotor zu beschreiben, bevor eine ODE aufgestellt werden kann, die die Zustandsänderung~$\dot{x}$ des Rotors beschreibt. Wie bereits im Abschnitt~\ref{Aufbau:Setup} erwähnt, kann der Zustand des Rotors durch seine Höhe~$z$, seine vertikale Geschwindigkeit~$\dot{z}$ und seine Rotationsgeschwindigkeit~$\Omega$ beschrieben werden. Für den Zustand~$x$ gilt somit:

    \begin{equation}
    x =
    \begin{bmatrix} 
    z \\
    \dot z \\ 
    \Omega \\ 
    \end{bmatrix} \mathrm{.}
    \end{equation}

    Die zeitliche Änderung des Zustands entspricht somit:

    \begin{equation}
    \dot{x} =
    \begin{bmatrix} 
    \dot{z} \\
    \ddot z \\ 
    \dot{\Omega} \\ 
    \end{bmatrix} \mathrm{.}
    \end{equation}

    Es gilt eine Funktion~$f(x, u)$ aufzustellen, welche der Zustandsänderung~$\dot{x}$ entspricht. Die Änderung der Höhe~$z$, also die vertikale Geschwindigkeit, lässt sich auf dem ursprünglichen Zustand entnehmen. \\

    Das Aufstellen einer Gleichung, die die Änderung der vertikalen Geschwindigkeit~$\dot{z}$, also $\ddot{z}$ beschreibt, ist bereits deutlich komplizierter. Gemäß dem 2. Newtonschen Axiom gilt für die Kraft~$F$ auf einen Körper der Masse~$m$ in Abhängigkeit von der Beschleunigung~$a$:

    \begin{equation} \label{eq:Newton}
    F = m \cdot a = m \cdot \ddot{z} \mathrm{.}
    \end{equation}

    Um die Beschleunigung des Rotors zu erhalten, müssen alle Kräfte bestimmt werden, die auf den Rotor wirken. In Abschnitt~\ref{Theorie:Kräfte, Winkel und Bewegungen} wurden bereits die Ausdrücke für die Auftriebs-, Widerstands, Gravitations- und Zugkraft hergeleitet~(Gleichung~\ref{eq:gravitation},~\ref{eq:auftrieb}~und~\ref{eq:widerstand}). \\

    Da sich der Rotor nur entlang der z-Achse bewegen kann, besteht nur Interesse an Kräften, die entlang dieser Achse wirken. Die Richtung der Kräfte muss demnach berücksichtigt werden. Da die Gravitations- und Zugkraft entlang der z-Achse nach unten wirken, können diese vollständig einbezogen werden. Anders sieht es bei der Auftriebs- und Widerstandskraft aus: Da der relative Wind den Rotor mit dem Anströmwinkel~$\alpha$ erreicht, wirkt die Widerstandskraft ebenso in diese Richtung. Für den vertikalen Anteil~$F_\mathrm{D,v}$ an der Widerstandskraft~$F_\mathrm{D}$ in Abhängigkeit vom Anströmwinkel~$\alpha$ gilt:

    \begin{equation} \label{eq:force drag vert}
    F_\mathrm{D,v} = F_\mathrm{D} \cdot \mathrm{cos}(\alpha) \mathrm{.}
    \end{equation}

    Die Auftriebskraft~$F_\mathrm{L}$ ist stets senkrecht zur Strömungsrichtung gerichtet \cite{Hall_2022}. Für den vertikalen Kraftanteil~$F_\mathrm{L,v}$ gilt:

    \begin{equation} \label{eq:force lift vert}
    F_\mathrm{L,v} = F_\mathrm{L} \cdot \mathrm{sin}(\alpha) \mathrm{.}
    \end{equation}

    Durch Umstellen von Gleichung~(\ref{eq:Newton}) und Einsetzen der hergeleiteten vertikalen Kräfte (Gleichung~\ref{eq:gravitation},~\ref{eq:force drag vert}~und~\ref{eq:force lift vert}) erhält man die vertikale Beschleunigung des Rotors in Abhängigkeit vom Anströmwinkel~$\alpha$, Einstellwinkel~$\beta$ und Anstellwinkel~$\gamma$:

    \begin{equation} \label{BasisModell:VertikaleBeschleunigung}
    \ddot{z} = \frac{1}{m}  \cdot [n \cdot F_\mathrm{Lift}(\gamma, \tilde{r}) \cdot \mathrm{cos}(\alpha(\tilde{r})) - n \cdot F_\mathrm{Drag}(\gamma, \tilde{r}) \cdot \mathrm{sin}(\alpha(\tilde{r})) - F_\mathrm{Gravity}] \mathrm{.}
    \end{equation}
    
    Zuletzt muss noch eine Formel für die Rotationsbeschleunigung~$\dot{\Omega}$ gefunden werden. Für die Rotationsbeschleunigung in Abhängigkeit vom Trägheitsmoment~$J$ und dem Drehmoment~$M$ gilt allgemein:

    \begin{equation}
    \dot{\Omega} = \frac{M}{J} \mathrm{.}
    \end{equation}

    Für das Drehmoment~$M$ in Abhängigkeit vom Radius~$r$ mit $r = \tilde{r}$ gilt:

    \begin{equation}
    M = F \cdot \tilde{r} \mathrm{.}
    \end{equation}

    Da sich die Ebene in der sich der Rotor dreht orthogonal zur z-Achse befindet, sind nur horizontale Kräfte relevant. Für den senkrechten Anteil der Auftriebs- $F_\mathrm{L,h}$ bzw. Widerstandskraft $F_\mathrm{D,h}$ gilt:

    \begin{equation} \label{BasisModell:Rotationsbeschleunigung}
    F_\mathrm{L,h} = F_\mathrm{L} \cdot \mathrm{cos}(\alpha) \quad \mathrm{bzw.} \quad F_\mathrm{D,h} = F_\mathrm{D} \cdot \mathrm{sin}(\alpha)\mathrm{.}
    \end{equation}

    Für das Rotationsmoment gilt somit:

    \begin{equation} \label{eq:Moment}
    M = \tilde{r} \cdot (F_\mathrm{L,h} - F_\mathrm{D,h}) \mathrm{.}
    \end{equation}
    

    Mittels der Gleichungen~(\ref{BasisModell:VertikaleBeschleunigung}) und~(\ref{BasisModell:Rotationsbeschleunigung}) lässt sich schließlich die gesuchte Funktion für die Zustandsänderung aufstellen:

    \begin{equation}
    f(x, u) = 
    \begin{bmatrix}
    \dot z \\ 
    \ddot z \\ 
    \dot \Omega \\ 
    \end{bmatrix}
    =
    \begin{bmatrix}
    \dot z \\

    \frac{1}{m}  \cdot [n \cdot F_\mathrm{L,v}(\gamma, \tilde{r}) - n \cdot F_\mathrm{D,v}(\gamma, \tilde{r}) - F_\mathrm{G}] \\

    n \cdot \tilde{r} \cdot \frac{1}{2 \cdot m \cdot J} \cdot [F_\mathrm{L,h}(\gamma, \tilde{r}) - F_\mathrm{D,h}(\gamma, \tilde{r})]
    \end{bmatrix} \mathrm{.}
    \end{equation}
    