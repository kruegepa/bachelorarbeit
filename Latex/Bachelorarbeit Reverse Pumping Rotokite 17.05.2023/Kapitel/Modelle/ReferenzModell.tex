\subsection{Referenzmodell}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Allgemein
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Allgemein}
    Das Referenzmodell basiert auf der Blattelemententheorie. Wie in \cite{van2015grundlagen} beschrieben, werden die Rotorblätter in radialer Richtung numerisch zerlegt. Die Strömungsgeschwindigkeit und -winkel wird für jedes Element separat betrachtet. Es wird angenommen, dass die Strömung quasistationär ist. Für jedes Element wird die Strahltheorie angewandt. Eine Integration über den Rotorblattradius und Summierung der einzelnen Kräfte liefert das Drehmoment und die an der Rotornabe angreifende Kraft. 
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Methode und Annahmen
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Methode und Annahmen}
    % Winkel und Geschwindigkeiten
    Im Gegensatz zu dem Basismodell auf Kapitel~\ref{SimpleModell} wird für das Referenzmodell nicht mehr angenommen, dass die vertikale Geschwindigkeit~$v_\mathrm{vert}$ über das gesamte Rotorblatt konstant ist. Stattdessen wird der Flügel in infitisimal kleine Abschnitte mit gleichem Radius zerlegt und die Geschwindigkeit des realen Winds und Anströmwinkels für jedes Blattelement separat mittels Gleichung~(\ref{eq:velocity}) und~(\ref{eq:alpha}) berechnet. Die Kräfte auf die einzelnen Elemente werden zu einer Auftriebs- und Widerstandskraft summiert. \\

    Weiter wird diesmal berücksichtigt, dass nicht über den gesamten Radius Auftrieb generiert wird. Es wird davon ausgegangen, dass für $r < r_\mathrm{stab}$ weder eine Auftriebs- noch Abtriebskraft wirkt. Ebenso wird angenommen, dass Verluste an den Rotorblattspitzen auftreten~\cite{Glauert}, weshalb die letzten 3~\% des Radius ignoriert werden:

    \begin{equation}
    r_\mathrm{max} = 0,97 \cdot r_\mathrm{Blatt} \mathrm{.}
    \end{equation}

    Beim Referenzmodell wird beachtet, dass der Rotor eine Kraft auf die Luft ausübt und diese somit beschleunigt, sodass ein Abwind entsteht, welcher den Rotor ebenfalls beeinflusst. Die Geschwindigkeit des Abwinds~$v_\mathrm{abw}$ wird in Abschnitt~\ref{Referenzmodell:Herleitung der Systemgleichung} hergeleitet.

    Während beim Basismodell angenommen wurde, dass der Rotor das Trägheitsmoment~$J$ eines Zylinders besitzt, wird dieses für das Referenzmodell genauer berechnet. Es wird angenommen, dass Rotornabe, die Verbindungsstäbe und die Rotorblätter unterschiedliche Dichten besitzen. Weiter wird der Abstand vom Mittelpunkt berücksichtigt. Für das Trägheitsmoment entlang der z-Achse gilt nach~\cite{Carceller_Candau_2020}:

    \begin{equation}
    \begin{aligned}
    J &= n \cdot \bigg(\rho_\mathrm{core}  \cdot \int _0 ^{r_\mathrm{core}} x^2 dx + \rho_\mathrm{stick} \cdot \int _{r_\mathrm{core}} ^{r_\mathrm{stick}} x^2 dx + \rho_\mathrm{wing} \cdot \int _{r_\mathrm{stick}} ^{r_\mathrm{wing}} x^2 dx \bigg) \\
    &= \frac{1}{3} r_\mathrm{core}^3 \frac{m_\mathrm{core}}{r_\mathrm{core}} + \frac{n}{3} r_\mathrm{stick}^3 \frac{m_\mathrm{stick}}{r_\mathrm{stick} - r_\mathrm{core}} - \frac{1}{3} r_\mathrm{core}^3 \frac{m_\mathrm{core}}{r_\mathrm{core}} + \frac{n}{3} r_\mathrm{wing}^3 \frac{m_\mathrm{wing}}{r_\mathrm{wing} - r_\mathrm{stick}} - \frac{n}{3} r_\mathrm{stick}^3 \frac{m_\mathrm{stick}}{r_\mathrm{stick} - r_\mathrm{core}} \mathrm{.}
    \end{aligned}
    \end{equation}
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Herleitung der Systemgleichung
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Herleitung der Systemgleichung} \label{Referenzmodell:Herleitung der Systemgleichung}
    % Herleitung Wake
    Alle Kräfte die vertikal auf den Rotor wirken, wirken laut dem 3.~Newtonschen Gesetz auch auf die Luft. Die Luft wird somit durch den Rotor beschleunigt und besitzt eine Geschwindigkeit~$v_\mathrm{abw}$. Um diese Geschwindigkeit zu bestimmen, müssem alle vertikalen Rotorkräfte~$F_\mathrm{vert, tot}$ summiert werden:

    \begin{equation}
    F_\mathrm{vert, tot} = \sum F = n \cdot F_\mathrm{Lift, vert} - n \cdot F_\mathrm{Drag, vert} - F_\mathrm{Gravity} \mathrm{.}
    \end{equation}

    Für die Wakegeschwindigkeit gilt somit in Abhängigkeit von der Kraft auf die Luft~$F_\mathrm{vert, tot}$:

    \begin{equation}
    \bar{F} = \frac{1}{2} \rho A v_\mathrm{2}^2 \quad \Longleftrightarrow \quad v_\mathrm{2} = \frac{\sqrt{2 \bar{F}}}{\rho A} \mathrm{.}
    \end{equation}

    Die Geschwindigkeit des Abwinds~$v_2$ hängt von der mittleren Kraft~$\bar{F}$ auf die Luft über die Zeit ab:

    \begin{equation}
    \bar{F}(t) = \int _0 ^\infty w(\tau) F(t-\tau) d \tau = \int _{- \infty} ^t w(\tau) F(t -\tau) d(t - \tau) 
    \end{equation}

    wobei für die Zeitgewichtung~$w$ gilt: $w(\tau) = \frac{1}{T} e^{-\tau / T}$ und $\int _0 ^\infty w(\tau) = 1$. \\

    Das in Kapitel~\ref{SimpleModell} beschriebene Modell wird um den Zustand~$\bar{F}$ erweitert um darauf die Abwindgeschwindigkeit zu bestimmen. Für die Änderung des neu eingeführten Zustands gilt:

    \begin{equation}
    \dot{\bar{F}} = \frac{1}{T} (F(t) - \bar{F(t)}) \mathrm{.}
    \end{equation}

    Für die Änderung der vertikalen Geschwindigkeit und der Rotortionsgeschwindigkeit gelten weiterhin die durch Gleichung~(\ref{eq:force drag vert}-\ref{eq:Moment}) hergeleiteten Zusammenhänge. Für die Änderung des Zustands~$x$:

    \begin{equation}
    x =
    \begin{bmatrix} 
    z \\
    \dot z \\ 
    \Omega \\
    \bar{F}
    \end{bmatrix}
    \end{equation}

    gilt somit:

        \begin{equation}
    f(x, u) = 
    \begin{bmatrix}
    \dot z \\ 
    \ddot z \\ 
    \dot \Omega \\ 
    \bar{F}
    \end{bmatrix}
    =
    \begin{bmatrix}
    \dot z \\

    \frac{1}{m}  \cdot [n \cdot F_\mathrm{L,v}(\gamma, \tilde{r}) - n \cdot F_\mathrm{D,v}(\gamma, \tilde{r}) - F_\mathrm{G}] \\

    n \cdot \tilde{r} \cdot \frac{1}{2 \cdot m \cdot J} \cdot [F_\mathrm{L,h}(\gamma, \tilde{r}) - F_\mathrm{D,h}(\gamma, \tilde{r})] \\

    \frac{1}{T} (F(t) - \bar{F(t)})
    \end{bmatrix} \mathrm{.}
    \end{equation}