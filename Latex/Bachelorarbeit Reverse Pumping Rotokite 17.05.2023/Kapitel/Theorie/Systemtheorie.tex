\subsection{Systemtheorie}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemein
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Allgemein}
Ziel der Systemtheorie ist die Modellierung und Simulation des Verhalten von Systemen. Das Systemverhalten wird mittels mathematischer Modelle beschrieben. Ein System $\mathcal{H}$ besitzt einen Eingang $u$, internen Zuständen $x$ und einen Ausgang $y$.

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=2]
% Rechteck zeichnen
\draw (1,0) rectangle (4,1) node[midway] {Dynamisches System};
% Pfeil von links
\draw [->] (-1,0.5) -- (1,0.5) node[midway,above] {Eingang};
% Pfeil aus Rechteck
\draw [->] (4,0.5) -- (6,0.5) node[midway,above] {Ausgang};
\end{tikzpicture}
\caption{Schaubild eines Systems mit Eingang und Ausgang.}
\label{fig:schaubild_dynamisches_system}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eingang
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Eingang}
Das Rotokite wird mittels der Wahl des Einstellwinkel $\beta$ und der Zugkraft $F_\mathrm{Zug}$ gesteuert. Für den Eingang $u(t)$ gilt $u: \mathbb{R} \rightarrow \mathbb{R}^2$, $t \mapsto u(t)$ mit 
\begin{equation}
u(t) = \begin{bmatrix} \beta(t) \\ F_\mathrm{Zug}(t) \end{bmatrix} \mathrm{.}
\end{equation}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% System
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
\subsubsection{System}
Ein System $\mathcal{H}$ ist eine abstrahierte Anordung, die mehrere Signale zueinander in Beziehung setzt. Es entspricht der Abbildung eines oder mehrerer Eingangssignale auf ein oder mehrere Ausgangssingnale $y(t) = \mathcal{H} \{ u(t) \}$ \cite{frey2008signal}. Alle nötigen Informationen um das zukünftige Systemverhalten zu bestimmen, sind im Systemzustand $x(t)$ mit $x(t) \in \mathbb{R}^n$ enthalten, wobei $n \in \mathbb{N}$ der Anzahl an internen Zuständen entspricht \cite{sr_diehl_2022}. Das Rotokite besitzt die Höhe $z$, die vertikale Geschwindigkeit $\dot{z}$ und die Rotationsgeschwindigkeit $\Omega$ als Zustände. Es gilt
            
\begin{equation}
x(t) = \begin{bmatrix} z(t)\\ \dot{z}(t)\\ \Omega(t)\end{bmatrix} \quad  \mathrm{mit} \quad x(0) = x_0 = \begin{bmatrix} z_0\\ \dot{z}_0\\ \Omega_0\end{bmatrix} \mathrm{.}
\end{equation}
            
Die Änderung der Zustände über die Zeit $\dot{x}(t)$ wird durch eine Differentialgleichung $f$ mit $f: \mathbb{R}^n \times \mathbb{R}^m \rightarrow \mathbb{R}^n$ beschrieben:
        
\begin{equation}
\dot{x}(t) = f(x(t), u(t)) = \begin{bmatrix} \dot{z}(t)\\ \ddot{z}(t)\\ \dot{\Omega}(t)\end{bmatrix}  \mathrm{.}
\end{equation}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ausgang
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Ausgang}
Der Ausgang des Systems wird mit $y(t)$ bezeichnet. Es wird angenommen, dass alle Zustände des Systems gemessen werden können. In der Praxis ist dies jedoch nicht immer der Fall. Weiter wird angenommen, dass durch die Messung ein Rauschen $w(t)$ auf die Zustände addiert wird. Für den Ausgang des Rotokites mit $y, w: \mathbb{R} \rightarrow \mathbb{R}^3$, $t \mapsto y(t), w(t)$ mit 
            
\begin{equation}
y(t) = I \cdot x(t) + w(t) \mathrm{.}
\end{equation}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Simulation} \label{theorie:simulation}
Im Modellierungsteil dieser Arbeit wird eine gewöhnliche Differentialgleichung~(ODE) hergeleitet, welche die Änderung der Rotorzustände beschreibt. Außerdem ist ein Initialzustand~$x_\mathrm{0}$ des Rotors  vorgegeben. Gesucht ist eine Lösung der Differentialgleichung, die den Initialzustand erfüllt. Probleme dieser Klasse werden als Anfangswert- bzw. Cauchy-Probleme~(IVP) bezeichnet. Gemäß dem Satz von Picard-Lindelöf existiert unter der Annahme, dass $f$ in $x$ und $t$ kontinuierlich ist, genau eine lokale, eindeutige Lösung des Anfangswertproblems. Um das Verhalten des Systems über die Zeit effizient und genau bestimmen zu können, wird ein passendes Simulationswerkzeug benötigt \cite{noc_diehl_2022}. \\

Simulationsmethoden lassen sich grob in einstufige bzw. mehrstufige sowie explizite und implizite Methoden unterteilen. Die Methoden unterscheiden sich darin, wie sie die Lösung des IVPs approximieren. Bei hinreichend großer Simulationsauflösung~$N$ ($N \rightarrow \infty)$, wobei $t_\mathrm{k} := t_\mathrm{0} + k \Delta t$ mit $k = 0, \dots, N$, 
konvergieren jedoch alle Approximationen~$s_\mathrm{k}$ gegen die exakte Lösung~$x(t_\mathrm{k})$, also $s_\mathrm{k} \rightarrow x(t_\mathrm{k})$ \cite{noc_diehl_2022}. \\

Um das Verhalten des Systems zu simulieren wird das explizite, vierstufige Runge-Kutta-Verfahren verwendet. Es wertet die ODE an vier verschiedenen Stellen $s_{k,1}$,~$s_{k,2}$,~$s_{k,3}$~und~$s_{k,4}$ aus:  

\begin{equation}
\begin{aligned}
s_{k,1} &:= s_k, \\
s_{k,2} &:= s_k + \Delta t \cdot a_{21} f(s_{k,1}, t_{k,1}), \\
s_{k,3} &:= s_k + \Delta t \cdot a_{31} f(s_{k,1}, t_{k,1}) + \Delta t \cdot a_{32} f(s_{k,2}, t_{k,2}), \\
s_{k,4} &:= s_k + \Delta t \cdot a_{41} f(s_{k,1}, t_{k,1}) + \Delta t \cdot a_{42} f(s_{k,2}, t_{k,2}) + \Delta t \cdot a_{43} f(s_{k,3}, t_{k,3}) \mathrm{.}
\end{aligned}
\end{equation}

Der nächste Zustand~$s_\mathrm{k+1}$ wird dann aus den unterschiedlich gewichteten Auswertestellen approximiert:

\begin{equation}
s_{k+1} := s_k + \Delta t \sum _{j=1} ^{4} b_j f(s_{k,j}, t_{k,j}) \mathrm{.}
\end{equation}

Die Koeffizienten der einzelnen Auswertepunkte werden meist in einem Butcher Tableau zusammengefasst. Das Tableau für das vierstufige, explizite Runge-Kutta-Verfahren ist in Tabelle~\ref{ButcherTableauRungeKutta4thOrder} abgebildet. 

\begin{table}[h]
\begin{center}
\begin{tabular}{c|cccc}
0 \\
$\frac{1}{2}$ & $\frac{1}{2}$ \\
$\frac{1}{2}$ & 0 & $\frac{1}{2}$ \\
1 & 0 & 0 & 1 \\
\hline
& $\frac{1}{6}$ & $\frac{1}{3}$ & $\frac{1}{3}$ & $\frac{1}{6}$ \
\end{tabular}
\end{center}
\caption{Butcher Tableau für die Runge-Kutta Methode 4. Grades \cite{butcher1996history}.}
\label{ButcherTableauRungeKutta4thOrder}
\end{table}
