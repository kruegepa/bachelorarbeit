\subsection{Aufbau}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Setup} \label{Aufbau:Setup}
In dieser Arbeit wird ein um die z-Achse rotierender $n$-blättriger Rotor mit Masse~$m$ betrachtet~(siehe Abbildung~\ref{Aufbau}). Der Rotor kann sich frei entlang der z-Achse durch die Luft der Dichte~$\rho$ bewegen. Der Zustand des Rotors wird durch dessen Höhe~$z$, der vertikalen Geschwindigkeit~$\dot{z}$ und der Rotationsgeschwindigkeit~$\Omega$ beschrieben. Es besteht eine Verbindung zwischen Rotor und Boden über die eine Kraft~$F_\mathrm{Zug}$ nach unten auf den Rotor ausgeübt werden kann.
    
\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/Aufbau.jpeg}
\caption{Schematische Darstellung des Aufbaus.}
\label{Aufbau}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rotor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Rotor}
In Abbildung~\ref{RotorblattAbmessungen} sind die Abmessungen des Rotors schematisch dargestellt. Der Rotor besteht aus einer Rotornabe, an der die Rotorblätter mittels Stangen befestigt sind. Die Rotornabe besitzt die Masse~$m_\mathrm{Nabe}$ und den Radius~$r_\mathrm{Nabe}$. Die Distanz zwischen Nabenmittelpunkt und dem Rotorblattanfang wird mit $r_\mathrm{Stab}$ bezeichnet. Jede Stange besitzt die Masse~$m_\mathrm{Stab}$. Der Abstand zwischen dem Mittelpunkt und dem Ende der Rotorblätter der Masse~$m_\mathrm{Blatt}$ wird durch $r_\mathrm{Blatt}$ beschrieben. Es wird angenommen, dass die einzelnen Bauteile eine gleichmäßige Dichte besitzen. Für die Rotorblattfläche~$S$ in Abhängigkeit von der Blattlänge~$l = r_\mathrm{Blatt} - r_\mathrm{Stab}$ und der Blattbreite~$b$ gilt:
    
\begin{equation}
S = l \cdot b{.}
\end{equation}
    
\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/RotorblattAbmessungen.jpeg}
\caption{Schematische Darstellung eines Rotorblatts.}
\label{RotorblattAbmessungen}
\end{figure}

Abbildung~\ref{RotorblattBegriffe} zeigt den Querschnitt eines Rotorblatts. Die Verbindung zwischen der Vorderkante und der Hinterkante wird als Profilsehne bezeichnet. Die Rotorblätter befinden sich in der Ausgangslage, wenn die Profilsehne parallel zur Horizontalen ausgerichtet ist.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/RotorblattBegriffe.jpeg}
\caption{Graphik mit Rotorblatt-Begriffen.}
\label{RotorblattBegriffe}
\end{figure}


    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Kräfte & Winkel & Bewegungen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Kräfte, Winkel und Bewegungen} \label{Theorie:Kräfte, Winkel und Bewegungen}
Um das Verhalten des Rotors zu untersuchen, ist es wichtig die auf den Rotor wirkenden Kräfte zu verstehen. Abbildung~\ref{KräfteWinkelRichtungen} zeigt eine schematische Darstellung dieser Kräfte. In diesem Abschnitt werden die vier wesentlichen Kräfte näher bestimmt. \\
    
Der Rotor ist dem Schwerefeld der Erde ausgesetzt und wird folglich durch die Gravitationskraft~$F_\mathrm{G}$ nach unten beschleunigt. Für die Gravitationskraft in Abhängigkeit von der Gravitationsbeschleunigung~$g$ gilt:
    
\begin{equation} \label{eq:gravitation}
F_\mathrm{G} = g \cdot (m_\mathrm{Nabe} + n \cdot m_\mathrm{Stab} +  n \cdot m_\mathrm{Blatt}) = m \cdot g \mathrm{.}
\end{equation}

Weiter kann eine Zugkraft~$F_\mathrm{Zug}$ entgegen der z-Achse auf den Rotor ausgeübt werden. Bewegt sich der Rotor durch die Luft, so wirkt gemäß dem Gesetz von Bernoulli eine Kraft auf die Flügel, die den Rotor in vertikale und tangentiale Richtung beschleunigt. Für den Druck~$p$ der durch eine Strömung mit Geschwindigkeit~$v$ und Dichte~$\rho$ auf die Fläche~$A$ ausgeübt wird, gilt:

\begin{equation}
p = \frac{1}{2} \cdot \rho \cdot v^2 \mathrm{.}
\end{equation}
    
Um die auf den Rotor wirkende Kraft zu bestimmen, muss folglich die Geschwindigkeit des Luftstroms bestimmt werden. Diese hängt sowohl von der Rotationsgeschwindigkeit~$\Omega$, der Steiggeschwindigkeit~$\dot z$, als auch der Geschwindigkeit des durch den Rotor erzeugten Abwinds~$v_\mathrm{ab}$ ab~(siehe Abbildung~\ref{WindRichtung}). Für die Geschwindigkeit~$v_\mathrm{ges}$ des auf den Rotor strömenden Fluids in Abhängigkeit vom Radius~$r$ gilt:

\begin{equation} \label{eq:velocity}
v_\mathrm{ges}(r, z) = \sqrt{(\Omega \cdot r)^2 - (\dot h - v_\mathrm{ab})^2} \mathrm{.}
\end{equation}

Für den Anströmwinkel~$\alpha$ des Winds, in Abhängikeit vom Radius~$r$ und der Abwindgeschwindigkeit~$v_\mathrm{abw}$ gilt:

\begin{equation} \label{eq:alpha}
\alpha(r, v_\mathrm{abw}) = -\mathrm{arctan} \bigg(\frac{\dot z - v_\mathrm{abw}}{\Omega \cdot r} \bigg) \mathrm{.}
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/WindRichtung.jpeg}
\caption{Schematische Darstellung der Strömungen und Winkel.}
\label{WindRichtung}
\end{figure}
    
Da die Rotorblätter nicht immer parallel zur Horizentalen ausgerichtet sind, muss der Einstellwinkel~$\beta$ der Rotorblätter ebenfalls berücksichtigt werden. Der Winkel aus dem der Wind auf den Rotor trifft, wird mit $\gamma$ bezeichnet. Für den Anstellwinkel~$\gamma$ gilt:

\begin{equation}
\gamma(r, v_\mathrm{abw}) = \alpha(r, v_\mathrm{abw}) - \beta \mathrm{.}
\end{equation}

Da nun sowohl die Geschwindigkeit des Winds als auch dessen Richtung bekannt sind, können die Kräfte auf den Rotor bestimmt werden. Für die Auftriebskraft~$F_\mathrm{L}$ gilt:

\begin{equation} \label{eq:auftrieb}
F_\mathrm{L}(\gamma, r) = \frac{1}{2} \rho S v_\mathrm{wind}(r)^2 C_\mathrm{L}(\gamma) \mathrm{.}
\end{equation}

Die Widerstandskraft~$F_\mathrm{D}$ wird wie folgt berechnet:

\begin{equation} \label{eq:widerstand}
F_\mathrm{D}(\gamma, r) = \frac{1}{2} \rho S v_\mathrm{wind}(r)^2 C_\mathrm{D}(\gamma) \mathrm{.}
\end{equation}

Auf den Einfluss der Flügelform und insbesondere des Auftriebsbeiwerts~$C_\mathrm{L}$ und Widerstandsbeiwerts~$C_\mathrm{D}$ wird im Abschnitt~\ref{Aerodynamik}~Aerodynamik näher eingegangen.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/RotorKraefte.jpeg}
\caption{Schematische Darstellung der auf den Rotor wirkenden Kräfte.}
\label{KräfteWinkelRichtungen}
\end{figure}
