%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemein
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Allgemein}
In diesem Abschnitt wird die Qualität der in \autoref{Modelle} hergeleiteten Modelle analysiert. Zunächst werden in \autoref{Ergebnisse: Parameter} die Wahl der Parameter des in der Simulation verwendeten Rotors erläutert. \autoref{Ergebnisse: Validierung der Modelle und der Implementierung} beschäftigt sich mit der Validierung der Modelle und Implementierung. In \autoref{Ergebnisse: Auswertung der verschiedenen Modelle} wird die Genauigkeit und Geschwindigkeit für die Auswertung der Modelle untersucht. Die Auswirkung der Zeitkonstanten~$T$ wird in \autoref{Ergebnisse: Auswirkung der Wahl der Zeitkontanten} behandelt. Abschließend wird in \autoref{Ergebnisse: Pumping-Szenario und optimale Steuerung} auf die Steuerung des Rotors eingegangen, sodass ein Pumping-Zyklus realisiert werden kann.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Parameter} \label{Ergebnisse: Parameter}
%% Rotorparameter
Im Folgenden sind die während der Simulationen verwendeten Parameter gelistet. Untersucht wird ein Rotor mit einem Durchmesser von etwa~$2,2~\si{m}$, dessen Parameter \autoref{tab:ParameterLeichtGroßerRotor} und \autoref{tab:ParameterSchwerGroßerRotor} zu entnehmen sind.


%% Beiwerte
Für die in \autoref{Theorie: Aerodynamische Beiwerte} erläuterten Beiwerte wird angenommen, dass diese für kleine Winkel linear bzw. quadratisch zur Orientierung des Flügels sind. Die Rotorblätter des großen Rotors werden durch einen ovale Flügel approximiert~(siehe \autoref{Flügelform}). Die Beiwerte dieses Flügels~\cite{Airfoiltools_2023} wurden für verschiedene Flügelorientierungen experimentell bestimmt. In \autoref{BeiwerteGraphik} ist der Auftriebs- und Widerstandsbeiwert für verschiedene Angriffswinkel dargestellt. Ebenso abgebildet ist die lineare bzw. quadratische Approximation der Beiwerte.


\begin{figure}[h]
\centering
\includesvg[width=0.9\columnwidth]{Graphiken/Wing.svg}
\caption{Schaubild der für die Simulationen angenommenen Rotorblattform.}
\label{Flügelform}
\end{figure}


%% Zeitkonstante
Die Zeitkonstante~$T$ des induzierten Abwinds lässt sich ähnlich wie die Beiwerte ebenfalls nur schwer experimentell ermitteln, weshalb auf ein Modell~\cite{trevisi2023vortex} zur Simulation des Rotorabwinds zurückgegriffen wird. Mit Hilfe des Modells wurde für verschiedene Rotorabmessungen und Zustände das Verhalten des Wakes untersucht. Wie in \autoref{Modelle: Abwind} vermutet, stellt sich heraus, dass die Zeitkonstante nicht nur von den Rotoreigenschaften, sondern auch von dessen Geschwindigkeit abhängt. Entsprechend müsste die Zeitkonstante in jedem Simulationsschritt neu berechnet werden. Dies würde den Umfang dieser Arbeit jedoch weit überschreiten, weshalb angenommen wird, dass~$T$ konstant ist. Für die in \autoref{tab:ParameterSchwerGroßerRotor} angegebene Zeitkonstante wurden zahlreiche Annahmen für den Rotor und dessen Geschwindigkeit getroffen, weshalb es sich nur um eine grobe Abschätzung der Größenordnung handelt.


\begin{table}[h]
\centering
\caption{Allgemeine Parameter des simulierten Rotors.}
\label{tab:AllgemeineParameter}
\begin{tabular}{c|c|c}
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$g$ & Gravitationsbeschleunigung & 9,81 \si{m/s} \\
$\rho$ & Luftdichte & 1,225 \si{kg/m^3} \\
\end{tabular}
\end{table}


\begin{table}[h]
\centering
\caption{Leicht zu bestimmende Parameter des Rotors.}
\label{tab:ParameterLeichtGroßerRotor}
\begin{tabular}{c|c|c}
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$m_\mathrm{nabe}$ & Masse der Rotornabe & 0,4 \si{kg} \\
$m_\mathrm{stab}$ & Masse einer Verbindungsstange & 0,02 \si{kg} \\
$m_\mathrm{blatt}$ & Masse eines Rotorblatts & 0,2 \si{kg} \\
$n$ & Anzahl an Rotorblättern & 3 \\
$r_\mathrm{nabe}$ & Distanz zwischen Nabenmittelpunkt und Nabenrand & 0,2 \si{m} \\
$r_\mathrm{stab}$ & Distanz zwischen Nabenmittelpunkt und Rotorblattanfang & 0,25 \si{m} \\
$r_\mathrm{blatt}$ & Distanz zwischen Nabenmittelpunkt und Rotorblattende & 1,1 \si{m} \\
$b_\mathrm{blatt}$ & Breite eines Rotorblatts & 0,3 \si{m} \\
\end{tabular}
\end{table}


\begin{table}[h]
\centering
\caption{Schwer zu bestimmende Parameter des Rotors.}
\label{tab:ParameterSchwerGroßerRotor}
\begin{tabular}{c|c|c}
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$C_\mathrm{L\alpha}$ & Steigung des Auftriebsbeiwerts & 0,11 \si{\per \degree} \\
$C_\mathrm{L0}$ & Auftriebsbeiwert bei 0\degree & 0 \\
$C_\mathrm{D\alpha}$ & Steigung des Widerstandsbeiwert & 0,00015 \si{\per \degree} \\
$C_\mathrm{D0}$ & Widerstandsbeiwert bei 0\degree & 0,005 \\
$T$ & Zeitkonstante des Abwinds & 0,1 \si{s} \\
\end{tabular}
\end{table}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validierung der Modelle und der Implementierung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Validierung der Modelle und der Implementierung} \label{Ergebnisse: Validierung der Modelle und der Implementierung}
\subsubsection{Allgemein}
In diesem Abschnitt erfolgt eine Überprüfung der Realitätsnähe der hergeleiteten Modelle. Da nicht genügend Messdaten zur Verfügung stehen, werden bei der Simulation des Rotorverhaltens bestimmte Annahmen getroffen, die eine Überprüfung der Ergebnisse ermöglichen. Es ist wichtig zu beachten, dass die Korrektheit der Validierungsszenarien keine allgemeine Korrektheit der Modelle gewährleistet, sondern lediglich darauf hinweist, dass diese in bestimmte Szenarien den Erwartungen entsprechen.


%%% Versuchsaufbau und -durchführung %%%
\subsubsection{Versuchsaufbau und -durchführung}
In diesem Abschnitt werden die Zustände des großen Rotors in zwei verschiedenen Szenarien überprüft. Einmal wird untersucht, wie sich der Rotor im luftleeren Raum verhält. Dazu wird die Luftdichte~$\rho$ auf null gesetzt. Ebenso wird überprüft, wie sich der Rotor mit infinitesimal kleinen~($S \rightarrow 0$) bzw. gar keinen Flügeln im luftgefüllten Raum bewegt. Die Zustände zu Beginn der Simulation sind \autoref{tab:InvertialwerteValidierung} zu entnehmen.


\begin{table}[h]
\centering
\caption{Für die Simulationen verwendete Inertialwerte der Rotorzustände.}
\label{tab:InvertialwerteValidierung}
\begin{tabular}{c|c|c}
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$z$ & Höhe & 0 \si{m} \\
$\dot{z}$ & vertikale Geschwindigkeit & 0 \si{m/s} \\
$\Omega$ & Rotationsgeschwindigkeit & 1 \si{U/s} \\
\end{tabular}
\end{table}


%%% Ergebnisse %%%
\subsubsection{Ergebnisse}
%% Allgemeine Beschreibung
\autoref{ValidationNoAir} und \autoref{ValidationNoWings} zeigen das mittels \ac{RK4} simulierte Verhalten des Rotors für die beiden oben beschriebenen Szenarien. Für die Simulation wurden die im \autoref{Modelle} hergeleiteten Modelle verwendet. Aus den Abbildungen wird deutlich, dass das Basis- und Referenzmodell nahezu identische Ergebnisse für beide Szenarien liefern, während das neuronale Netz teilweise erheblich von den beiden Modellen abweicht.


%% Beschreibung: Höhe
Der von den Modellen vorhergesagte Höhenverlauf ähnelt in beiden Szenarien einer umgedrehten Parabel mit Scheitelpunkt bei~$z_0 = 0~\si{m}$. Die nach zwei Sekunden zurückgelegte Strecke unterscheidet sich jedoch bei den Modellen: Während das Basis- und Referenzmodell eine Höhenabnahme von etwa~$19,5~\si{m}$ vorhersagen, geht das \ac{FNN} davon aus, dass die Abnahme lediglich~$7~\si{m}$ bzw.~$12~\si{m}$ beträgt.


%% Beschreibung: vertikale Geschwindigkeit
Laut dem Basis- und dem Referenzmodell nimmt die vertikale Geschwindigkeit linear mit der Zeit um~$-9,81~\si{m/s^2}$ zu. Die Simulation des neuronalen Netzes geht für den Rotor im luftleeren Raum ebenfalls von einem linearen Zusammenhang von~$-7,25~\si{m/s^2}$ aus. Für das zweite Szenario prophezeit das Netz eine auf etwa $10~\si{m/s}$ begrenzte Abnahme der Geschwindigkeit.


%% Beschreibung: Rotationsgeschwindigkeit
Die Rotationsgeschwindigkeit des Rotors bleibt gemäß dem Basis- und Referenzmodell konstant bei der ursprünglichen Geschwindigkeit von~$\Omega = 2~\si{U/s}$. Das \ac{FNN} geht davon aus, dass sich die Rotationsgeschwindigkeit auf zwei bzw. drei Umdrehungen pro Sekunde ändert.


%% Beschreibung: mittlere Kraft
Alle drei Modelle sagen voraus, dass der Rotor so gut wie keine Kraft auf die ihn umgebende Luft ausübt. Die Simulation vom Basis- und Referenzmodell zeigt eine konstante mittlere Kraft von~$\bar{F} = 0~\si{N}$, während die vom \ac{FNN} vorausgesagte Kraft von $0~\si{N}$ abweicht, jedoch immer kleiner als $1~\si{N}$ ist.


%% Beschreibung: Energie
Die kinetische Energie nimmt gemäß der Modelle in beiden Szenarien zu, während die potenzielle Energie quadratisch abnimmt. Die Rotationsenergie bleibt laut dem Basis- und Referenzmodell konstant, während sie laut dem neuronalen Netz leicht zunimmt. Die Gesamtenergie bleibt jedoch bei allen Modellen entweder konstant oder nimmt nur leicht ab.


\begin{figure}[h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationNoAir.svg}
\caption{Simuliertes Verhalten eines Rotors im luftleeren Raum.}
\label{ValidationNoAir}
\end{figure}


\begin{figure}[h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationNoWings.svg}
\caption{Simuliertes Verhalten eines Rotors ohne Rotorblätter.}
\label{ValidationNoWings}
\end{figure}



%%% Auswertung %%%
\subsubsection{Diskussion}
%% Erwartetes Verhalten
Befindet sich der Rotor im luftleeren Raum oder ist die Flügelflächen sehr klein, so sollte die Auftriebs- und Widerstandskraft gemäß dem Gesetz von Bernoulli~\eqref{eq:bernoulli_gesetz} null betragen. Die einzige wirkende Kraft ist demnach die Gravitationskraft, welche den Rotor nach unten beschleunigt. Die vertikale Beschleunigung~$\ddot{z}$ sollte demnach der Gravitationsbeschleunigung~$g$ entsprechen. Für die vertikale Geschwindigkeit~$\dot{z}$ wird erwartet, dass~$\dot{z}(t) = -g \cdot t$ gilt. Befindet sich der Rotor zu Beginn der Simulation in Ruhe, lässt sich für die Höhe gemäß dem Zeit-Ort-Gesetz erwarten: $z(t) = z_\mathrm{0} - \frac{1}{2} \cdot g \cdot t^2$. Da keine Drehmomente auf den Rotor wirken, sollte die Rotationsgeschwindigkeit konstant bleiben.


%% Diskussion: Höhe
Der vom Basis- und Referenzmodell vorhergesagt Höhenverlauf stimmt mit dem Zeit-Ort-Gesetz überein. Die Vorhersage des \ac{FNN} kann hingegen nur für wenige Sekundenbruchteile als annähernd korrekt bezeichnet werden, weicht dann jedoch deutlich vom erwarteten Verhalten ab, auch wenn die Tendenz stimmt.

%% Diskussion: vertikale Geschwindigkeit
Die vertikale Geschwindigkeit wird vom Basis- und Referenzmodell korrekt vorhergesagt. Das neuronale Netz liefert für kurze Zeit eine gute Näherung, weicht dann allerdings stark von den Erwartungen ab.


%% Diskussion: Rotationsgeschwindigkeit
Da keine Kräfte in horizontale Richtung wirken, existiert auch kein Drehmoment, das eine Rotationsbeschleunigung hervorrufen könnte. Entsprechend muss die Rotationsgeschwindigkeit konstant bleiben, was sich in der Simulation vom Basis- und Refernzmodell zeigt. Die Simulation des Netzes geht fälschlicherweise von einer leichten Zunahme bzw. Abnahme der Geschwindigkeit aus.


%% Diskussion: mittlere Kraft
Da keine Luft existiert bzw. der Rotor keine Fläche besitzt, mit der er eine Kraft auf die Luft ausüben kann, wird erwartet, dass die mittlere Kraft durchgehend null ist. Während das Basis- und Referenzmodell dieser Erwartung vollständig entsprechen, geht das neuronale Netz davon aus, dass ein Abwind entsteht. Die vorhergesagte mittlere Kraft ist zwar im Vergleich zur Gravitationskraft gering, aber nicht vernachlässigbar.


%% Diskussion: Energie
Die Nettoleistung des Systems sollte null betragen, da keine Energie in das System hinein bzw. herausfließt. Diese Annahme trifft für alle Modelle für etwa eine Sekunde zu. Die zuerst genannten Modelle sagen die Gesamtenergie korrekt vorher, während letztgenanntes gegen Ende der Simulation geringfügig von den Erwartungen abweicht.


%% Allgemeine Diskussion
Ergebnis dieses Abschnitts ist, dass die Simulation des Basis- und Referenzmodell in den beiden beschriebenen Szenarien die Erwartungen voll erfüllt. Das \ac{FNN} liefert nur für wenige Sekundenbruchteile ausreichend korrekte Ergebnisse.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auswertung der verschiedenen Modelle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Auswertung der verschiedenen Modelle} \label{Ergebnisse: Auswertung der verschiedenen Modelle}
\subsubsection{Allgemein}
In diesem Abschnitt sollen die verschiedenen Modelle hinsichtlich ihrer Genauigkeit und Effizienz untersucht werden. Zunächst wird die Genauigkeit vom Basismodell und dem neuronalen Netz untersucht, das Referenzmodell liefert hierfür die Referenzwerte. Hierzu wird die vorhergesagte Zustandsänderung der beiden Modelle für verschiedene Zustände und Eingänge untersucht. Ebenso wird die Genauigkeit der beiden Modelle für ein realistisches (Pumping)-Szenario bestimmt. Im Anschluss wird untersucht, wie lange die Simulation der verschiedenen Modelle dauert.


\subsubsection{Definitionen}
\todo{MAE; Beschreibung erneuern}
%% Mean-Squared-Error
Die \ac{MSE} wird genutzt, um die Genauigkeit eines Modells zu bestimmen. Mit der \ac{MSE} wird die mittlere quadratische Abweichung zwischen Vorhersage des Modells und dem wahren Wert bestimmt:

\begin{equation}
    \mathrm{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{y}_i)^2 \text{.}
\end{equation}




\subsubsection{Genauigkeit: verschiedene Zustände und Eingänge}
\todo{MAE; Beschreibung erneuern}
%%% Vergleich von ODEs für verschiedene Zustände
%% Durchführung
Für die Bestimmung der Vorhersagegenauigkeit wird der große Rotor verwendet. Die drei hergeleiteten Modelle werden hierfür für verschiedene Zustände und Eingänge untersucht und die ausgegebene Zustandsänderung verglichen. Es wird angenommen, dass die Höhe~$z$ keinen Einfluss auf die Zustandsänderung hat und demnach bei der weiteren Untersuchung konstant auf~$z = 10~\si{m}$ gehalten werden kann. Der \ac{MSE} zwischen dem Referenzmodell und dem Basismodell bzw. dem neuronalen Netz wird für alle in \autoref{} aufgeführten Zustands- und Eingangskombinationen überprüft.


%% Ergebnisse: Basismodell
\autoref{HeatmapBasisModel} zeigt den \ac{MSE} des Basismodells für verschiedene Zustände. Ist die mittlere Kraft~$\bar{F} = 0~\si{N}$, so ist die Vorhersagequalität des Modells für alle Zustandskombinationen sehr gut. Die geringste Abweichung vom Referenzmodell ist für kleine absolute vertikale Geschwindigkeiten~$\vert v \vert$ kombiniert mit kleinen Rotationsgeschwindigkeiten. Für~$\vert v \vert \gg 0$ sinkt die Qualität des Modells deutlich, ist jedoch immer noch sehr gut. Auffällig ist, dass die Rotationsgeschwidigkeit nur einen untergeordneten Einfluss auf den \ac{MSE} hat.

Gilt für die mittlere Kraft~$\bar{F} = 50~\si{N}$, so steigt der Fehler deutlich an. Der geringste Fehler ist für negative vertikale Geschwindigkeiten in Kombination mit hohen Rotationsgeschwindigkeiten zu beobachten. Am höchsten ist der \ac{MSE} für hohe postive vertikale Geschwindigkeiten bei hohen Rotationsgeschwindigkeiten.


%% Ergebnisse: Neuronales Netz
In \autoref{HeatmapNeuralNetwork} ist der \ac{MSE} des \ac{FNN} für verschiedene Zustände abgebildet. Für~$\bar{F} = 0~\si{N}$ ist der Fehler deutlich größer als beim Basismodell. Die geringste Abweichung ist für~$\dot{z} > 0$ und~$\Omega > 0$ festzustellen. Große Abweichungen vom Referenzwert gibt es für~$\dot{z} \ll 0$ sowie~$\Omega \gg 0$.

Wird eine mittlere Kraft von~$\bar{F} = 50~\si{N}$ angenommen, schlägt sich das neuronale Netz besser als das Basismodell. Insbesondere für geringe Rotationsgeschwindigkeiten ist die Genauigkeit des Modells hoch. Für~$\vert \dot{z} \vert \gg 0$ oder~$\vert \Omega \vert \gg 0$ ist der \ac{MSE} hingegen hoch.


\begin{figure}[h]
\centering
\includesvg[width=\linewidth]{Graphiken/HeatMapsF0NMAE.svg}
\caption{\acs{MAE} des Basismodells~(l) und das \acs{FNN}~(r) für~$\bar{F} = 0~\si{N}$.}
\label{HeatmapsF0N}
\end{figure}


\begin{figure}[h]
\centering
\includesvg[width=\linewidth]{Graphiken/HeatMapsF50NMAE.svg}
\caption{\acs{MAE} des Basismodells~(l) und das \acs{FNN}~(r) für~$\bar{F} = 50~\si{N}$.}
\label{HeatmapsF50N}
\end{figure}



%% Diskussion
Das Basismodell liefert für geringe mittlere Kräfte deutlich bessere Ergebnisse als das neuronale Netz. Sobald sich jedoch ein Abwind und somit auch eine mittlere Kraft aufgebaut hat, liefert das neuronale Netz in den meisten Fällen bessere Ergebnisse. Dies liegt daran, dass das Basismodell die mittlere Kraft ignoriert, während das \ac{FNN} aus den Trainingsdaten auch den Einfluss des Abwinds erlernen konnte. Die Qualität des Netzes kann durch mehr Trainingsdaten und tiefere Hyperparameteroptimierung weiter optimiert werden.




\subsubsection{Genauigkeit: Pumping-Szenario}
%% Durchführung
Für die Berechnung der \ac{MSE} muss zunächst ein realistisches Szenario festgelegt werden. In diesem Fall wird das in \autoref{Ergebnisse: Pumping-Szenario und optimale Steuerung} bestimmte Reverse-Pumping-Szenario verwendet. Alle drei Modelle werden mit dem aus dem Pumping-Szenario gewonnen Eingangsvektor simuliert. Die Ausgänge der Modelle werden verwendet, um die \ac{MSE} für das Basismodell und das neuronale Netz zu berechnen.


%% Ergebnisse und Beschreibung der Ergebnisse
\autoref{GenauigkeitPumping} zeigt die Simulation eines Pumping-Zykluses. Die Eingabe~$u$ wurde durch optimale Steuerung des Referenzmodells gewonnen. Es zeigt sich, dass sowohl das Basismodell als auch das \ac{FNN} von dem Refernzmodell abweichen. Die mittlere quadratische Abweichung des Basismodells fällt mit~$MSE_\mathrm{BM} = 0,43$ jedoch deutlich geringer als beim neuronalen Netz~($MSE_\mathrm{BM} = 15,1)$ aus.


\begin{figure}[h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationAllModelsPumping.svg}
\caption{Simuliertes Rotorverhalten während eines Pumping-Zykluses.}
\label{GenauigkeitPumping}
\end{figure}


%% Diskussion
Es stellt sich heraus, dass die Genauigkeit des Basismodells deutlich besser, als die des neuronalen Netzes ist. Die Abweichung der Simulation des \ac{FNN} ist so groß, dass dieses nur als grobe Abschätzung genutzt werden sollte. Die Qualität des Modells kann jedoch gemäß dem universellen Approximationstheorem durch einen größeren Trainingsdatensatz sowie tiefere Hyperparametersuche beliebig verbessert werden. Eine Verbesserung des Netzes würde jedoch mit höheren Kosten in Form von Zeit und Ressourcen verbunden sein. 





\subsubsection{Berechnungsdauer}
%% Durchführung und Ergebnisse
Die Dauer pro Auswertung der drei Modelle wird für jeweils 1000 Auswertungen bestimmt und gemittelt~(siehe \autoref{tab:BerechnungsdauerModelle}). Ebenso wird gemessen, wie lange es dauert, um das \ac{FNN} zu erstellen, die notwendigen Trainings- und Testdaten zu generieren sowie das Modell zu trainieren. Die Auswertung wird auf einem Computer mit Apple-M1-Pro-Chip durchgeführt.

\begin{table}[h]
\centering
\caption{Berechnungsdauer für verschiedene Operationen.}
\label{tab:BerechnungsdauerModelle}
\begin{tabular}{c|c|c}
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$T_\mathrm{BM,ODE}$ & Zeit pro Auswertung der Basismodell-\ac{ODE} & 0,21 \si{ms} \\
$T_\mathrm{RM,ODE}$ & Zeit pro Auswertung der Referenzmodell-\ac{ODE} & 48 \si{ms} \\
$T_\mathrm{FNN,Pred}$ & Zeit pro Auswertung des \ac{FNN} & 38 \si{ms} \\
$T_\mathrm{FNN,Modell}$ & Zeit für die Erzeugung des \ac{FNN} & 61 \si{s} \\
\end{tabular}
\end{table}


%% Diskussion
Es wird deutlich, dass die Auswertung des Referenzmodells mehr als 200 Mal länger dauert als die Auswertung des Basismodells. Grund dafür ist, dass die Kräfte und Winkel für jedes Blattelement einzeln berechnet werden müssen. Das Vorhersagen einer Zustandsänderung mittels des \ac{FNN} dauert ähnlich lange wie mit dem Referenzmodell und ist somit ebenso sehr zeitintensiv. Beim neuronalen Netz muss ebenso beachtet werden, dass dessen Erstellung und Training viel Zeit in Anspruch nehmen kann. Diese Dauer kann jedoch vernachlässigt werden, da dieser Schritt nur einmal anfällt.

Durch die kurze Auswertezeit ist das Basismodell insbesondere für längere Simulationen geeignet. Ebenso ist das Modell geeignet, um schnelle und gleichzeitig qualitativ hochwertige Abschätzungen über das Rotorverhalten treffen zu können. Das \ac{FNN} liefert hingegen nur eine mäßige Qualität und benötigt deutlich mehr Zeit für die Auswertung als das Basismodell. Die Auswertezeiten können bei Bedarf durch effizientere Algorithmen weiter verbessert werden.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auswirkung der Wahl der Zeitkonstanten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Auswirkung der Wahl der Zeitkonstanten} \label{Ergebnisse: Auswirkung der Wahl der Zeitkontanten}

%% Allgemein
Das Basismodell und das Referenzmodell unterscheiden sich unter anderen darin, dass beim Referenzmodell beachtet wird, dass der Rotor eine Kraft auf die ihn umgebende Luft ausübt und somit einen Abwind erzeugt.


%% Bedeutung der Zeitkonstante
Im \autoref{Modelle: Abwind} wurde die Änderung dieser Kraft bereits hergeleitet. Die Geschwindigkeit der Änderung der mittleren Kraft und somit auch des Abwinds hängt von einer Zeitkonstanten~$T$ ab. Je kleiner die Zeitkonstante, desto schneller konvergiert die mittlere Kraft~$\bar{F}$ gegen die momentane Kraft, die der Rotor auf die Luft ausübt. 


%% Beschreibung
\autoref{WakesReferenceModel} zeigt das simulierte Rotorverhalten für verschiedene Zeitkonstanten~$T$. Es wird deutlich, dass die Zeitkonstante einen großen Einfluss auf das Verhalten des Rotors hat. Für kleine~$T$ nimmt die Höhe des Rotors innerhalb von zwei Sekunden fast doppelt so viel ab, wie für große~$T$. Der Betrag der vertikalen Geschwindigkeit ist für kleine Zeitkonstanten ebenfalls deutlich größer. Der Verlauf der Rotationsgeschwindigkeit hängt hingegen nur geringfügig von der Zeitkonstante ab. Die mittlere Kraft ändert sich erwartungsgemäß für kleine~$T$ am schnellsten. Es lässt sich vermuten, dass die mittlere Kraft für alle~$T$s gegen den gleichen Wert konvergiert. Der Energieverlust ist für kleine~$T$ größer als für große~$T$.

\begin{figure}[!h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationDifferentWakes.svg}
\caption{Simulation des Verhaltens des Rotors für verschiedene Zeitkonstanten~$T$.}
\label{WakesReferenceModel}
\end{figure}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pumping-Szenario und optimale Steuerung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Pumping-Szenario und optimale Steuerung} \label{Ergebnisse: Pumping-Szenario und optimale Steuerung}
\subsubsection{Versuchsaufbau und -durchführung}
%% Reverse-Pumping-Allgemein
Ziel des Reverse-Pumpings ist es, den Rotor auch bei Windstille auf einer Höhe zu halten. Diese Forderung lässt sich, wie in \autoref{Theorie: Optimale Steuerung} beschrieben, als Nebenbedingung ausdrücken. Für den Rotor soll sowohl zu Beginn eines Pumping-Zyklus~($x(t=0)$) als auch am Ende~($x(t=t_\mathrm{f})$) gelten:

\begin{equation} \label{Nebenbedingung 1}
     g_1(z) = z(0) - z(t_\mathrm{f}) \overset{!}{=} 0 \text{.}
\end{equation}

Diese Nebenbedingung alleine ist jedoch nicht ausreichend um den Rotor auf Dauer auf einer Höhe zu halten, den ein kurzes Aufsteigen und ein darauf folgender freier Fall wäre auch eine Lösung dieser Bedingung. Damit der Pumping-Prozess fortlaufend wiederholt werden kann, ohne, dass die Eingabe sich für jeden Zyklus ändert, ist es wünschenswert, wenn alle Zustände am Ende eines Zyklus den gleichen Wert wie zu Beginn annehmen. Es wird somit zusätzlich gefordert:

\begin{equation} \label{Nebenbedingung 2}
     g_2(z) = \dot{z}(0) - \dot{z}(t_\mathrm{f}) \overset{!}{=} 0 \text{,}
\end{equation}

\begin{equation} \label{Nebenbedingung 3}
     g_3(z) = \Omega(0) - \Omega(t_\mathrm{f}) \overset{!}{=} 0 \text{,}
\end{equation}

\begin{equation} \label{Nebenbedingung 4}
     g_4(z) = \bar{F}(0) - \bar{F}(t_\mathrm{f}) \overset{!}{=} 0 \text{.}
\end{equation}


%% Optimierungsfunktion
Der Eingang sollte dabei so gewählt werden, dass die mittlere Zugkraft möglichst gering ausfällt:

\begin{equation}
    \underset{u(t)}{\mathrm{min}} \, \Lambda_1(u(t)) = \underset{u(t)}{\mathrm{min}} \,  F_\mathrm{T}(u(t))^2 \text{.}
\end{equation}

Sollte es nicht möglich sein, eine Eingabe zu finden, sodass \eqref{Nebenbedingung 2}, \eqref{Nebenbedingung 3} und \eqref{Nebenbedingung 4} erfüllt werden, sollte die Abweichung dieser Zustände zumindest minimiert werden:

\begin{equation}
    \underset{u(t)}{\mathrm{min}} \, \Lambda_2(u(t)) = \underset{u(t)}{\mathrm{min}} \,  (x(0) - x(t_\mathrm{f}))^2 \text{.}
\end{equation}


\subsubsection{Ergebnisse}
Da keine Lösung für den Eingang gefunden wurde, die alle vier Nebenbedingungen erfüllt, wird auf Nebenbedingung~\eqref{Nebenbedingung 4} verzichtet und dafür als Optimierungsfunktion~$\Lambda_2$ gewählt. Die gefundene Lösung ist \autoref{PumpingSzenario} zu entnehmen. Es wird deutlich, dass der Rotor einer starken vertikalen Beschleunigung ausgesetzt ist. Die Rotationsgeschwindigkeit liegt zwischen 15 und 20 Umdrehungen pro Sekunde. Die mittlere Kraft erreicht einen maximalen Wert von~$\bar{F} = 70~\si{N}$. Zu Beginn und am Ende des Zyklus nimmt die Energie des Systems zu.

\begin{figure}[!h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationPumping.svg}
\caption{Optimaler Steuerung des Rotors.}
\label{PumpingSzenario}
\end{figure}


\subsubsection{Auswertung}
Die hohe vertikale Beschleunigung des Rotors zeigt, dass auf diesen eine große Kraft wirkt. Die Zugkraft~$F_\mathrm{T}$ erreicht ein Maximum von~$\vert F_\mathrm{T} \vert = 200~\si{N}$ und beträgt somit ein Vielfaches des Eigengewichts des Rotors. Die Konstruktion eines Rotors, der solch hohe Kräfte aushält, stellt sich vermutlich als schwierig heraus. Das Erreichen der hohen Rotationsgeschwindigkeiten wird vermutlich ebenfalls eine Herausforderung darstellen. In Anbetracht der kurzen Periodendauer des Zyklus scheint die berechnete Eingabe~$u$ wohl schwer umzusetzen sein. Aufgrund der oben geschilderten Punkte, lässt sich vermuten, dass die Realisierung des Pumping-Zyklus mit dem vorgestellten Rotor nur schwer in der Praxis erreichbar ist. 
