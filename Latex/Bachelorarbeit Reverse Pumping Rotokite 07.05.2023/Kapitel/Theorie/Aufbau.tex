\subsection{Aufbau}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Setup
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Setup}
    Im Folgenden wird ein nach oben ausgerichteter $n$-blättriger Rotor mit Masse~$m$ betrachtet, welcher sich frei entlang der z-Achse durch die Luft mit Dichte~$\rho$ bewegen kann (siehe Abbildung~\ref{Aufbau}). Der Rotor kann durch seine Höhe~$h$, seine vertikale Geschwindigkeit~$\dot h$ und Rotationsgeschwindigkeit~$\Omega$ beschrieben werden. Mittels einer Verbindung zum Boden ist es möglich eine Kraft~$F_\mathrm{Zug}$ auf den Rotor auszuüben.
    
    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\columnwidth]{Graphiken/Aufbau.jpeg}
    \caption{Schematische Darstellung des Rotoraufbaus.}
    \label{Aufbau}
    \end{figure}
        

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Rotor
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Rotor}
    In Abbildung~\ref{RotorblattAbmessungen} ist der Aufbau des Rotors schematisch dargestellt. Der Rotor besteht aus einer Rotornabe mit Masse~$m_\mathrm{Nabe}$ und Radius~$r_\mathrm{Nabe}$ an der die Rotorblätter mittels Stangen befestigt sind befestigt sind. Der Abstand zwischen Nabenmittelpunkt und dem Rotorblattanfang wird mit $r_\mathrm{Stab}$ bezeichnet. Die Distanz zwischen dem Mittelpunkt und dem Rotorblattende wird durch $r_\mathrm{Blatt}$ beschrieben. Die Rotorblattfläche~$S$ lässt sich mittels der Blattbreite~$b$ und der Blattlänge~$l = r_\mathrm{Blatt} - r_\mathrm{Stab}$ bestimmen. Für die Rotorblattfläche gilt:

    \begin{equation}
    S = l \cdot b{.}
    \end{equation}
    

    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\columnwidth]{Graphiken/RotorblattAbmessungen.jpeg}
    \caption{Schematische Darstellung eines Rotorblatts.}
    \label{RotorblattAbmessungen}
    \end{figure}


    In Abbildung~\ref{RotorblattBegriffe} ist der Querschnitt eines Rotorblatts abgebildet. Die Verbindung zwischen der Vorderkante und der Hinterkante wird als Profilsehne bezeichnet. Die Rotorblätter befinden sich in der Ausgangslage, wenn die Profilsehne parallel zur Horizontalen ausgerichtet ist.

    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\columnwidth]{Graphiken/RotorblattBegriffe.jpeg}
    \caption{Graphik mit Rotorblatt-Begriffen.}
    \label{RotorblattBegriffe}
    \end{figure}

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Kräfte & Winkel & Bewegungen
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \subsubsection{Kräfte \& Winkel \& Bewegungen}
    Um das Verhalten des Rotors zu untersuchen, ist es wichtig die auf den Rotor wirkenden Kräfte zu bestimmen (Abbildung~\ref{KräfteWinkelRichtungen}). Der Rotor ist dem Schwerefeld der Erde ausgesetzt und wird folglich durch die Gravitationskraft~$F_\mathrm{Gravitation}$ nach unten beschleunigt. Für die Gravitationskraft gilt:
    
    \begin{equation}
    F_\mathrm{Gravitation} = g \cdot (m_\mathrm{Nabe} + n  m_\mathrm{Stab} +  n  m_\mathrm{Blatt}) = m \cdot g \mathrm{.}
    \end{equation}

    Durch Ziehen am Verbindungsseil zwischen Rotor und Bodenstation kann eine Zugkraft~$F_\mathrm{Zug}$ nach unten auf den Rotor ausgeübt werden.

    Bewegt sich der Rotor durch die Luft, so wirkt gemäß dem Gesetz von Bernoulli eine Kraft auf die Flügel, die den Rotor in vertikale und tangentiale Richtung beschleunigt. Für den Druck~$p$ der durch eine Strömung mit Geschwindigkeit~$v$ und Dichte~$\rho$ auf die Fläche~$A$ ausgeübt wird, gilt:

    \begin{equation}
    p = \frac{1}{2} \cdot \rho \cdot v^2 \mathrm{.}
    \end{equation}
    
    Um die auf den Rotor wirkende Kraft zu bestimmen, muss folglich die Geschwindigkeit des Luftstroms bestimmt werden. Dieser hängt sowohl von der Rotationsgeschwindigkeit~$\Omega$, der Steiggeschwindigkeit~$\dot h$ als auch der Geschwindigkeit des durch den Rotor erzeugten Abwinds~$v_\mathrm{Abwind}$ ab (siehe Abbildung~\ref{WindRichtung}). Für die Geschwindigkeit~$v_\mathrm{Wind}$ des auf den Rotor strömenden Fluids gilt:

    \begin{equation}
    v_\mathrm{Wind}(r, z) = \sqrt{(\Omega \cdot r)^2 - (\dot h - v_\mathrm{Abwind})^2} \mathrm{.}
    \end{equation}

    Ebenfalls relevant ist die Richtung, aus der die Luft auf den Rotor zuströmt. Für den Anströmwinkel~$\alpha$ des Winds, in Abhängikeit vom Radius~$r$ und der Abwindgeschwindigkeit~$v_\mathrm{Abwind}$ gilt:

    \begin{equation}
    \alpha(r, z) = -\mathrm{arctan} \bigg(\frac{\dot h - v_\mathrm{Abwind}}{\Omega \cdot r} \bigg) \mathrm{.}
    \end{equation}    

    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\columnwidth]{Graphiken/WindRichtung.jpeg}
    \caption{Schematische Darstellung der Strömungen.}
    \label{WindRichtung}
    \end{figure}

    Da die Rotorblätter nicht immer parallel zur Horizentalen ausgerichtet sind, muss der Einstellwinkel~$\beta$ der Rotorblätter ebenfalls berücksichtigt werden. Der Winkel aus dem der Wind auf den Rotor trifft, wird mit $\gamma$ bezeichnet. Für den Anstellwinkel~$\gamma$ gilt:

    \begin{equation}
    \gamma(r, z) = \alpha(r, z) - \beta \mathrm{.}
    \end{equation}

    Da nun sowohl die Geschwindigkeit des Winds als auch dessen Richtung bekannt sind, können die Kräfte auf den Rotor bestimmt werden. Für die Auftriebskraft~$F_\mathrm{Auftrieb}$ gilt:

    \begin{equation} \label{eq:auftrieb}
    F_\mathrm{Auftrieb}(\gamma, r) = \frac{1}{2} \rho S v_\mathrm{wind}(r)^2 C_\mathrm{L}(\gamma) \mathrm{.}
    \end{equation}

    Die Widerstandskraft~$F_\mathrm{Widerstand}$ wird wie folgt berechnet:

    \begin{equation} \label{eq:widerstand}
    F_\mathrm{Widerstand}(\gamma, r) = \frac{1}{2} \rho S v_\mathrm{wind}(r)^2 C_\mathrm{D}(\gamma) \mathrm{.}
    \end{equation}

    Auf den Einfluss der Flügelform und insbesondere des Auftriebsbeiwerts~$c_\mathrm{l}$ und Widerstandsbeiwerts~$c_\mathrm{d}$ wird im Abschnitt~\ref{Aerodynamik} näher eingegangen.

    \begin{figure}[h]
    \centering
    \includegraphics[width=0.5\columnwidth]{Graphiken/RotorKraefte.jpeg}
    \caption{Schematische Darstellung der auf den Rotor wirkenden Kräfte.}
    \label{KräfteWinkelRichtungen}
    \end{figure}
    