%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemein
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\todo{TODO}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Zustände, Eingänge und Ausgänge
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Zustände, Eingänge und Ausgänge} \label{Modelle:Zustände, Eingänge und Ausgänge}
%% Zustände
Wie bereits im \autoref{Einleitung:Aufbau} erläutert wurde, wird der Zustand~$x$ des Rotors durch seine Höhe~$z$, seine vertikale Geschwindigkeit~$\dot{z}$, seine Rotationsgeschwindigkeit~$\Omega$ und die mittlere auf die Luft ausgeübte Kraft~$\bar{F}$ beschrieben:

\begin{equation}
x(t) = \begin{bmatrix} z(t)\\ \dot{z}(t)\\ \Omega(t)\\ \bar{F}\end{bmatrix} \quad  \mathrm{mit} \quad x(0) = x_0 = \begin{bmatrix} z_0\\ \dot{z}_0\\ \Omega_0\\ \bar{F}_0 \end{bmatrix} \text{.}
\end{equation}

Es wird angenommen, dass die Rotationsgeschwindigkeit jederzeit positiv ist. \\


%% Eingänge
Das Rotorkite wird mittels der Wahl des Einstellwinkels~$\beta$ und der Zugkraft~$F_\mathrm{T}$ gesteuert. Für den Eingang $u(t)$ mit $u: \mathbb{R} \rightarrow \mathbb{R}^2$ und~$t \mapsto u(t)$ gilt 

\begin{equation}
u(t) = \begin{bmatrix} \beta(t) \\ F_\mathrm{T}(t) \end{bmatrix} \text{.}
\end{equation}

Für die Zugkraft wird angenommen, dass diese nie nach oben gerichtet ist, also stets gilt:~$F_\mathrm{T}~\leq~0$. \\


%% Ausgänge
Es wird angenommen, dass alle Zustände des Systems gemessen werden können und der Ausgang~$y$ somit den Zuständen~$x$ entspricht.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rotorgeometrie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Geometrie des Rotors}
%% Rotorblattabmessungen
Das Rotorkite kann vereinfacht als Zusammensetzung aus Rotorblättern, Verbindungsstangen und einer Rotornabe betrachtet werden. Die Rotorblätter werden dabei durch ein elliptischen Flügel approximiert. Abbildung~\ref{RotorblattAbmessungen} zeigt die Draufsicht auf den Rotor mit den relevanten Abmessungen. Die Rotornabe kann näherungsweise als Zylinder mit Radius~$r_\mathrm{nabe}$ und Masse~$m_\mathrm{nabe}$ betrachtet werden. Zwischen Rotornabe und Rotorblättern sitzen Verbindungsstangen mit der Masse~$m_\mathrm{stab}$. Die Distanz zwischen Rotormittelpunkt und Beginn der Rotorblätter beträgt~$r_\mathrm{stab}$. Die Rotorblätter besitzen eine Länge von~$l = r_\mathrm{blatt} - r_\mathrm{stab}$ und eine Breite von~$b_\mathrm{blatt}$, woraus sich die Rotorblattfläche~$S$ ergibt:

\begin{equation}
S = l \cdot b_\mathrm{blatt} \text{.}
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\columnwidth]{Graphiken/RotorblattAbmessungen.drawio.png}
\caption{Schematische Darstellung eines Rotorblatts.}
\label{RotorblattAbmessungen}
\end{figure}


%% Fachbegriffe für Rotorblatt
Abbildung~\ref{RotorblattBegriffe} zeigt den Querschnitt eines Rotorblatts. Die Verbindung zwischen der Vorderkante und der Hinterkante wird als Profilsehne bezeichnet. Die Rotorblätter befinden sich in der Ausgangslage, wenn die Profilsehne parallel zur Horizontalen ausgerichtet ist.

\begin{figure}[h]
\centering
\includegraphics[width=1\columnwidth]{Graphiken/RotorblattBegriffe.drawio.png}
\caption{Graphik mit Rotorblatt-Begriffen.}
\label{RotorblattBegriffe}
\end{figure}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bewegungen, Kräfte und Winkel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Bewegungen, Kräfte und Winkel}
%% Allgemein
Für eine Analyse des Rotorverhaltens ist das Verständnis der auf den Rotor wirkenden relevanten Kräfte Voraussetzung. Im Folgenden werden die Gravitationskraft, die Auftriebskraft, die Widerstandskraft und die Zugkraft verwenden, um das Verhalten des Rotors zu beschreiben. \\

%% Schwerkraft
Der Rotor ist dem Schwerefeld der Erde ausgesetzt und wird folglich durch die Gravitationskraft~$F_\mathrm{G}$ nach unten beschleunigt. Für die Gravitationskraft in Abhängigkeit von der Gravitationsbeschleunigung~$g=9,81~\mathrm{m/s^2}$ gilt:
    
\begin{equation} \label{eq:Modelle:Bewegungen, Kräfte und Winkel:Gravitationskraft}
F_\mathrm{G} = g \cdot (m_\mathrm{nabe} + n \cdot m_\mathrm{stab} +  n \cdot m_\mathrm{blatt}) = g \cdot m \text{.}
\end{equation}

%% Zugkraft
Weiter kann eine Zugkraft~$F_\mathrm{T}$ entgegen der z-Achse auf den Rotor ausgeübt werden. \\

Die Bestimmung der Auftriebs- und Widerstandskraft setzt nach Bernoulli~\eqref{eq:bernoulli_gesetz_vereinfacht} Kenntnis über die Richtung und Geschwindigkeit der Strömung voraus. Es lassen sich drei Strömungen feststellen: Bewegt sich der Rotor entlang der z-Achse, so umströmt ihn die Luft mit der Geschwindigkeit~$-\dot{z}$. Ebenso relevant ist der durch den Rotor induzierte Abwind mit der Geschwindigkeit~$v_\mathrm{abw}$. Durch die Rotation durchdringen die Rotorblätter die Luft mit der vom Radius abhängigen Geschwindigkeit~$v_\mathrm{vert} = \Omega \cdot r$. Ein Überblick über die einwirkenden Strömungen und ihre Richtungen ist in Abbildung~\ref{KräfteWinkelRichtungen} dargestellt.

\begin{figure}[!h]
\centering
\includegraphics[width=0.7\columnwidth]{Graphiken/WindRichtung.drawio.png}
\caption{Schematische Darstellung der auf den Rotor wirkenden Kräfte.}
\label{KräfteWinkelRichtungen}
\end{figure}
\todo{Kräfte mit Pfeilen einzeichnen \& Winkel $\alpha$ einfügen}

Für die Geschwindigkeit~$v_\mathrm{w}$ des auf den Rotor strömenden Fluids in Abhängigkeit vom Radius~$r$ gilt:

\begin{equation} \label{eq:velocity}
v_\mathrm{w}(r, z) = \sqrt{(\Omega \cdot r)^2 + (\dot{z} + v_\mathrm{abw})^2} \text{.}
\end{equation}
\todo{Vorzeichen überprüfen}

Für den Anströmwinkel~$\alpha$ des Winds, in Abhängikeit vom Radius~$r$ und der Abwindgeschwindigkeit~$v_\mathrm{abw}$ gilt:

\begin{equation} \label{eq:alpha}
\alpha(r, v_\mathrm{abw}) = -\mathrm{arctan} \bigg(\frac{\dot z - v_\mathrm{abw}}{\Omega \cdot r} \bigg) \text{.}
\end{equation}
\todo{Vorzeichen überprüfen}

Um die in \autoref{TheorieAerodynamischeBeiwerte} beschriebenen Beiwerte zu ermitteln, muss der Winkel~$\gamma$ aus dem der Wind auf den Rotor trifft, bestimmt werden. Da die Rotorblätter nicht immer parallel zum Horizont ausgerichtet sind, muss der Einstellwinkel~$\beta$ der Rotorblätter ebenfalls berücksichtigt werden:

\begin{equation}
\gamma(r, v_\mathrm{abw}) = \alpha(r, v_\mathrm{abw}) - \beta \text{.}
\end{equation}

Für die Auftriebskraft in Abhängigkeit vom Anstellwinkel gilt nach \eqref{eq:bernoulli_gesetz_vereinfacht} und \eqref{eq:Auftriebsbeiwert} entsprechend:

\begin{equation} \label{eq:Auftriebskraft}
    F_\mathrm{L} = \frac{1}{2} \cdot n \cdot \rho \cdot (C_\mathrm{L0} + C_\mathrm{L\gamma} \cdot \gamma) \cdot S \cdot v_\mathrm{w}^2 = \frac{1}{2} \cdot n \cdot \rho \cdot C_\mathrm{L} \cdot S \cdot v_\mathrm{w}^2
\end{equation}

wobei~$n$ der Anzahl an Flügel entspricht. Für die Widerstandskraft gilt gemäß \eqref{eq:Widerstandsbeiwert}:

\begin{equation} \label{eq:Wiederstandskraft}
    F_\mathrm{D} = \frac{1}{2} \cdot n \cdot \rho \cdot (C_\mathrm{D0} + C_\mathrm{D\gamma} \cdot \gamma^2) \cdot S \cdot v_\mathrm{w}^2 = \frac{1}{2} \cdot n \cdot \rho \cdot C_\mathrm{D} \cdot S \cdot v_\mathrm{w}^2 \text{.}
\end{equation}

Abbildung~\ref{KräfteWinkelRichtungen} bietet einen Überblick über die auf den Rotor wirkenden Kräfte. 

\begin{figure}[!h]
\centering
\includegraphics[width=1\columnwidth]{Graphiken/KraefteWinkelRichtungen.drawio.png}
\caption{Schematische Darstellung der auf den Rotor wirkenden Kräfte.}
\label{KräfteWinkelRichtungen}
\end{figure}

Um die Änderung der vertikalen Geschwindigkeit bzw. der Rotationsgeschwindigkeit berechnen zu können, müssen die Auftriebs- und Widerstandskraft jedoch noch in ihren vertikalen und horizontalen Anteil zerlegt werden. Für die vertikalen Anteile der beiden Kräfte in Abhängigkeit vom Anströmwinkel gilt:

\begin{equation} \label{eq:KräfteVertikal}
F_\mathrm{L,v} = F_\mathrm{L} \cdot \mathrm{sin}(\alpha) \text{\quad bzw. \quad} F_\mathrm{D,v} = F_\mathrm{D} \cdot \mathrm{cos}(\alpha) \text{.}
\end{equation}

Für die horizontalen Kraftanteile gilt:

\begin{equation} \label{eq:KräfteHorizontal}
F_\mathrm{L,h} = F_\mathrm{L} \cdot \mathrm{cos}(\alpha) \text{\quad bzw. \quad} F_\mathrm{D,h} = F_\mathrm{D} \cdot \mathrm{sin}(\alpha) \text{.}
\end{equation}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abwind
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Abwind} \label{Modelle:Abwind}
Alle Kräfte die vertikal auf den Rotor wirken, wirken laut dem 3.~Newtonschen Gesetz auch auf die Luft. Die Luft wird somit durch den Rotor beschleunigt und besitzt eine Geschwindigkeit~$v_\mathrm{abw}$. Für den Massenfluss~$\dot{m}$ durch den Rotor gilt:

\begin{equation} \label{eq:Massefluss}
    \dot{m} = \rho \cdot A \cdot v_2 = \rho \cdot A \cdot v_\mathrm{abw}
\end{equation}

wobei~$A = \pi \cdot r_\mathrm{blatt}^2$ der Fläche des Rotors entspricht. Es wird davon ausgegangen, dass die Auswirkungen von Gravitations- und Zugkraft auf die Luft nur eine geringfügig sind und somit vernachlässigt werden können. Für die Kraft durch den Rotor auf die Luft~$\bar{F}$ gilt somit:

\begin{equation} \label{eq:MittlereKraft}
    \bar{F} = F_\mathrm{L,v} + F_\mathrm{D,v} = \dot{m} \cdot (v_1 - v_3)
\end{equation}

wobei~$v_1 = 0~\si{m/s}$ der Abwindgeschwindigkeit weit über dem Rotor und~$v_3$ der Geschwindigkeit weit unterhalb des Rotors entspricht. Gemäß dem Theorem von Froude und Rankine gilt für die Geschwindigkeit~$v_2$:

\begin{equation}
    v_2 = \frac{1}{2} \cdot {v_1 + v_3} = \frac{1}{2} \cdot v_3
\end{equation}

wodurch sich Gleichung~\eqref{eq:MittlereKraft} weiter vereinfachen lässt:

\begin{equation} \label{eq:MittlereKraftVereinfacht}
    \bar{F} = 2 \cdot \dot{m} \cdot v_2 \text{.}
\end{equation}

Für die Geschwindigkeit des Abwinds~$v_2 = v_\mathrm{abw}$ folgt aus~\eqref{eq:MittlereKraftVereinfacht} und~\eqref{eq:Massefluss} somit:

\begin{equation} \label{eq:Abwind}
    v_\mathrm{abw} = \frac{\bar{F}}{2 \cdot \dot{m}} = \frac{\bar{F}}{2 \cdot \rho \cdot A \cdot v_\mathrm{abw}} = \sqrt{\frac{\bar{F}}{2 \cdot \rho \cdot A}} \text{.}
\end{equation}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Referenzmodell
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Referenzmodell}
\subsubsection{Allgemein}
\todo{TODO}




\subsubsection{Methode und Annahmen}
%% Methode
Das Referenzmodell basiert auf der in \autoref{TheorieBlattelementetheorie} beschriebenen Blattelementetheorie. Die Rotorblätter werden demnach in radialer Richtung numerisch in infitisimal kleine Abschnitte gleicher Breite zerlegt. Die Strömungsgeschwindigkeit und der Anströmwinkel werden für jedes Element separat berechnet. Daraus lässt sich die Auftriebs- und Widerstandskraft für jeden Rotorabschnitt getrennt berechnen. Eine Integration über den Rotorradius und Summierung der einzelnen Kräfte liefert das Drehmoment und die an der Rotornabe angreifende Kraft.

%% Annahmen
Ebenso wird berücksichtigt, dass der Rotor nicht über den gesamten Radius Auftrieb generiert. Es wird davon ausgegangen, dass für $r < r_\mathrm{stab}$ weder eine Auftriebs- noch Abtriebskraft wirkt. Ebenso wird angenommen, dass Verluste an den Rotorblattspitzen auftreten~\cite{Glauert_1928}, weshalb die letzten 3~\% des Radius ignoriert werden:

\begin{equation}
r_\mathrm{max} = 0,97 \cdot r_\mathrm{Blatt} \text{.}
\end{equation}

Weiter wird beachtet, dass der Rotor eine Kraft auf die Luft ausübt und diese somit beschleunigt, sodass ein Abwind entsteht, welcher den Rotor ebenfalls beeinflusst. Die Geschwindigkeit des Abwinds~$v_\mathrm{abw}$ wird in \autoref{Modelle:Abwind} hergeleitet.

Für das Trägheitsmoment~$J$ wird angenommen, dass Rotornabe, die Verbindungsstäbe und die Rotorblätter unterschiedliche Dichten besitzen. Weiter wird der Abstand vom Mittelpunkt berücksichtigt. Für das Trägheitsmoment entlang der z-Achse gilt nach~\cite{Carceller_Candau_2020}:

\begin{equation}
J = n \cdot \bigg(\rho_\mathrm{nabe}  \cdot \int _0 ^{r_\mathrm{nabe}} x^2 dx + \rho_\mathrm{stab} \cdot \int _{r_\mathrm{nabe}} ^{r_\mathrm{stab}} x^2 dx + \rho_\mathrm{blatt} \cdot \int _{r_\mathrm{stab}} ^{r_\mathrm{blatt}} x^2 dx \bigg)
\end{equation}

wobei die Dichte der einzelnen Bauteile mit~$\rho$ bezeichnet wird.


\subsubsection{Herleitung der Systemgleichung}
Ziel dieses Abschnitts ist es eine Funktion~$f$ aufzustellen, welche die Änderung der Rotorzustände~$\dot{x}$ in Abhängigkeit von den aktuellen Zuständen~$x$ und dem Eingängen~$u$ beschreibt:

\begin{equation}
f(x(t), u(t)) = \dot{x}(x(t), u(t)) = \begin{bmatrix} 
\dot{z}(x(t), u(t)) \\
\ddot{z}(x(t), u(t)) \\ 
\dot{\Omega}(x(t), u(t)) \\ 
\dot{\bar{F}}(x(t), u(t))
\end{bmatrix} \text{.}
\end{equation}


%% Änderung der Höhe
Die Änderung der Höhe~$z$, also die vertikale Geschwindigkeit, lässt sich aus dem ursprünglichen Zustand entnehmen.


%% Herleitung: Änderung der vertikale Geschwindigkeit
Das Aufstellen einer Gleichung, die die Änderung der vertikalen Geschwindigkeit~$\frac{d \dot{z}}{dt} = \ddot{z}$ beschreibt, ist bereits deutlich aufwendiger. Gemäß dem zweiten Newtonschen Axiom gilt für die Kraft~$F$, die auf einen Körper der Masse~$m$ in Abhängigkeit von der Beschleunigung~$a$ wirkt:

\begin{equation} \label{eq:Modelle:Herleitung der Systemgleichung:Beschleunigung}
F = m \cdot a = m \cdot \ddot{z} \mathrm{.}
\end{equation}

Um die vertikale Beschleunigung des Rotors zu erhalten, müssen somit alle vertikalen Kräfte summiert werden, die auf den Rotor wirken:

\begin{equation} \label{eq:Modelle:Herleitung der Systemgleichung:Summe vertikaler Kräfte}
    F_\mathrm{v} = F_\mathrm{L,v} + F_\mathrm{D,v} + F_\mathrm{G} + F_\mathrm{T} \text{.}
\end{equation}

Durch Umstellen von Gleichung~\eqref{eq:Modelle:Herleitung der Systemgleichung:Beschleunigung} und Einsetzen der hergeleiteten vertikalen Kräfte~\eqref{eq:Modelle:Herleitung der Systemgleichung:Summe vertikaler Kräfte} erhält man die vertikale Beschleunigung des Rotors:

\begin{equation} \label{eq:Modelle:Herleitung der Systemgleichung:Änderung der vertikalen Geschwindigkeit}
\ddot{z} = \frac{1}{m} \cdot [F_\mathrm{L,v} + F_\mathrm{D,v} + F_\mathrm{G} + F_\mathrm{T}] = \frac{1}{m} \cdot F_\mathrm{v} \text{.}
\end{equation}


%% Herleitung: Änderung der Rotationsgeschwindigkeit
Ebenso muss eine Formel für die Rotationsbeschleunigung~$\dot{\Omega}$ gefunden werden. Für die Rotationsbeschleunigung in Abhängigkeit vom Trägheitsmoment~$J$ und dem Drehmoment~$M$ gilt allgemein:

\begin{equation} \label{eq:Modelle:Herleitung der Systemgleichung:Rotationsbeschleunigung}
\dot{\Omega} = \frac{M}{J} \mathrm{.}
\end{equation}

Für das Drehmoment~$M$ in Abhängigkeit vom Radius~$r$ gilt:

\begin{equation}
M = F \cdot r \text{.}
\end{equation}

Da sich die Ebene in der sich der Rotor dreht orthogonal zur z-Achse befindet, sind nur horizontale Kräfte~\eqref{eq:KräfteHorizontal} relevant:

\begin{equation} \label{eq:Modelle:Herleitung der Systemgleichung:Summe horizontalen Kräfte}
    F_\mathrm{h} = F_\mathrm{L,h} + F_\mathrm{D,h} \text{.}
\end{equation}

Für das Rotationsmoment gilt somit:

\begin{equation} \label{eq:Moment}
    M = r \cdot [F_\mathrm{L,h} + F_\mathrm{D,h}] = r \cdot F_\mathrm{h}
\end{equation}

woraus die Änderung der Rotationsgeschwindigkeit~$\dot{\Omega}$ folt:

\begin{equation} \label{eq:Modelle:Herleitung der Systemgleichung:Änderung der Rotationsbeschleunigung}
    \dot{\Omega} = \frac{M}{J} = \frac{r \cdot [F_\mathrm{L,h} + F_\mathrm{D,h}]}{J} \text{.}
\end{equation}


%% Herleitung: Änderung der mittleren Kraft
Es wird angenommen, dass für die Änderung des Zustands~$\bar{F}$ gilt:

\begin{equation} \label{eq:Modelle:Herleitung der Systemgleichung:Änderung der mittleren Kraft}
\dot{\bar{F}} = \frac{1}{T} (F(t) - \bar{F}(t))
\end{equation}

wobei die Zeitkonstante~$T$ beschreibt, wie schnell sich die mittlere Kraft der momentanen Kraft annähert. Je kleiner die Konstante, desto schneller konvergiert die mittlere Kraft gegen die momentane Kraft, die der Rotor auf die Luft ausübt. Es lässt sich davon ausgehen, dass die Konstante sowohl von den Rotoreigenschaften als auch der Rotorgeschwindigkeit abhängt~\cite{vermeer2003wind}.
\todo{Analyse von T nötig}

Bei Gleichung~\eqref{eq:Modelle:Herleitung der Systemgleichung:Änderung der mittleren Kraft} handelt es sich um eine lineare Differentialgleichung erster Ordnung. Die Lösung der Differentialgleichung lautet:

\begin{equation}
\bar{F} = e^{\frac{t}{T}} \bar{F}_0 + \int _0 ^t \frac{1}{T} e^{\frac{t-\tau}{T}} F(\tau) d\tau = \int _{-\infty} ^t w(\tau) F(t - \tau) d(t - \tau)
\end{equation}

wobei für die Zeitgewichtung~$w$ gilt: $w(\tau) = \frac{1}{T} e^{-\tau / T}$ und $\int _0 ^\infty w(\tau) = 1$.


%% Systemgleichung
Aus den Gleichungen~\eqref{eq:Modelle:Herleitung der Systemgleichung:Änderung der vertikalen Geschwindigkeit},~\eqref{eq:Modelle:Herleitung der Systemgleichung:Änderung der Rotationsbeschleunigung} und~\eqref{eq:Modelle:Herleitung der Systemgleichung:Änderung der mittleren Kraft} folgt die Änderung des Gesamtzustands~$\dot{x}$:

\begin{equation}
f(x, u) = 
\begin{bmatrix}
\dot z \\ 
\ddot z \\ 
\dot \Omega \\ 
\dot{\bar{F}}
\end{bmatrix}
=
\begin{bmatrix}
\dot z \\

\frac{1}{m}  \cdot [F_\mathrm{L,v} + F_\mathrm{D,v} + F_\mathrm{G} + F_\mathrm{T}] \\

\frac{1}{J} \cdot r \cdot [F_\mathrm{L,h} + F_\mathrm{D,h}] \\

\frac{1}{T} (F(t) - \bar{F}(t))
\end{bmatrix} \text{.}
\end{equation}






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Basismodell
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Basismodell}
\subsubsection{Allgemein}
\todo{TODO}

\subsubsection{Methode und Annahmen}
\todo{TODO}

\subsubsection{Herleitung der Systemgleichung}
\todo{TODO}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Neuronale Netz
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Neuronale Netz}
\subsubsection{Daten}
\todo{TODO}

\subsubsection{Architektur}
\todo{TODO}

\subsubsection{Training}
\todo{TODO}

\subsubsection{Hyperparameteroptimierung}
\todo{TODO}
