%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemein
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Ziel dieses Abschnitts besteht darin, die theoretischen Grundlagen zu wiederholen, die für das Verständnis der folgenden Kapitel erforderlich sind. Es wird auf die Aerodynamik von Drehflüglern eingegangen, welche essentiell für die Beschreibung des Rotorverhaltens ist. Neben einer kurzen Einführung in künstliche neuronale Netze liefert dieses Kapitel eine kurze Zusammenfassung der Werkzeuge aus den Bereichen Systemtheorie und Optimierung und Steuerung.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gesetz von Bernoulli
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gesetz von Bernoulli}
Das Gesetz von Bernoulli beschreibt den Zusammenhang zwischen der Geschwindigkeit~$v$ einer Strömung und dem daraus resultierenden Druck~$p$. Für stationäre, verlustfreie und inkompressible Strömungen sagt das Gesetzt aus, dass die Summe von Druckenergie, potentieller Energie und kinetischer Energie konstant bleibt:

\begin{equation} \label{eq:bernoulli_gesetz}
    \rho \cdot g \cdot h + \frac{1}{2} \cdot \rho \cdot v^2 + p = \mathrm{konst.} 
\end{equation}

Die Gleichung lässt sich in drei Teile zerlegen: Der geodätische Druck~$\rho \cdot g \cdot h$ beschreibt den Druck durch das Eigengewicht eines Fluids mit Dichte~$\rho$ und Höhe~$h$, das durch die Gravitationsbeschleunigung~$g$ beschleunigt wird. Der dynamische Druck~$\frac{1}{2} \cdot \rho \cdot v^2$ entspricht dem Druck, der durch die Geschwindigkeit~$v$ der Strömung entsteht. Der statische Druck wird durch das Symbol~$p$ gekennzeichnet. Es wird ausgegangen, dass sich die Höhe beim Umströmen des Rotors nicht ändert, weshalb der geodätische Druck im Folgenden ignoriert wird~\cite{Flurl_2023}.

Durch Annahme lässt sich Gleichung~\eqref{eq:bernoulli_gesetz} weiter vereinfachen und mittels~$F = \rho \cdot A$ nach der Kraft~$F$ auf eine Fläche~$A$ umstellen:

\begin{equation} \label{eq:bernoulli_gesetz_vereinfacht}
    F = \frac{1}{2} \cdot \rho \cdot \mathrm{konst.} \cdot A \cdot v^2 \text{.}
\end{equation}

Auf die Wahl der Konstanten wird im folgenden Abschnitt eingegangen.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Aerodynamische Beiwerte
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Aerodynamische Beiwerte} \label{TheorieAerodynamischeBeiwerte}
Umströmt ein Fluid ein Objekt, so übt die Strömung eine Auftriebskraft~$F_\mathrm{L}$ und eine Widerstandskraft~$F_\mathrm{D}$ auf das Objekt. Die Auftriebskraft beschreibt die dynamische Kraft auf einen umströmten Körper und wirkt senkrecht zur Strömungsrichtung und ist abhängig vom dimensionslosen Auftriebsbeiwert~$C_\mathrm{L}$~\cite{greulich1998lexikon_auftriebsbeiwert}. Der Auftriebsbeiwert hängt sowohl von der Rotorgeometrie als auch der Orientierung des Rotors zur Strömung ab~\cite{Zentrales_Informationssystem_Energieforschungsförderung_2023}.

Die Widerstandskraft wirkt parallel zur Strömungsrichtung. Sie ist Abhängig von der Oberflächenrauigkeit und Form des Körpers und ist abhängig vom größenlosen Widerstandsbeiwert~$C_\mathrm{D}$~\cite{greulich1998lexikon_widerstandsbeiwert}. Die beiden Beiwerte fassen den Einfluss der Flügeleingenschaften und -form zusammen.


Abbildung~\ref{BeiwerteGraphik} zeigt den für einen beispielhaften Flügel experimentell bestimmten Zusammenhang zwischen Flügelorientierungen~$\alpha$ und dem Auftriebsbeiwert~$C_\mathrm{L}$ bzw. Widerstandsbeiwert~$C_\mathrm{D}$. Es wird deutlich, dass für kleine Winkel~$\alpha$ nahezu ein linearen Zusammenhang zwischen dem Anstellwinkel und dem Auftriebsbeiwert existiert. Für kleine Winkel wird somit angenommen:

\begin{equation} \label{eq:Auftriebsbeiwert}
    C_\mathrm{L} = C_\mathrm{L0} + C_\mathrm{L\alpha} \cdot \alpha \text{.}
\end{equation}

Der Widerstandsbeiwert kann für kleine Winkel durch eine Parabel approximiert werden:

\begin{equation} \label{eq:Widerstandsbeiwert}
     C_\mathrm{D} = C_\mathrm{D0} + C_\mathrm{D\alpha} \cdot \alpha^2 \text{.}
\end{equation}

\begin{figure}[h]
\centering
\includesvg[width=0.9\textwidth]{Graphiken/Fluegel.svg}
\caption{Experimentell bestimmter Auftriebs- und Widerstandsbeiwert für verschiedene Angriffswinkel~\cite{Airfoiltools_2023}.}
\label{BeiwerteGraphik}
\end{figure}

Es ist wichtig zu beachten, dass diese Vereinfachungen nur für kleine Winkel gelten.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameterschätzung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Parameterschätzung}
Bei der Modellierung eines Systems werden häufig Parameter~$\theta$ verwendet, welche sich nur mit großem Aufwand direkt messen lassen. Statt die Parameter direkt zu ermitteln, können diese auch mittels eines Parameterschätzers approximiert werden.

Für die Parameterschätzung wird zunächst das Verhalten des Systems in verschiedenen Situationen untersucht:~$y = [y_1, \dots , y_N]^\mathrm{T}$, wobei $N$ der Anzahl an Messreihen entspricht. Im Anschluss wird ein mathematisch-physikalisches Modell für das Systemverhalten hergeleitet, welches die unbekannten Parameter enthält. Die mit dem realen System durchgeführten Messungen werden mit dem mathematisch-physikalischen System wiederholt. Die Simulation des Modells für die verschiedenen Szenarien~$\hat{y} = [\hat{y}_1, \dots, \hat{y}_N]^\mathrm{T}$ sollte dabei ähnlich zu den realen Messungen~$y$ sein. 

Die Bestimmung der optimalen Parameterwerte~$\theta^*$ stellt ein Optimierungsproblem da:

\begin{equation}
\theta^* = \underset{\theta}{\arg\min} \, \mathcal{L}(y - \hat{y})
\end{equation}

wobei~$\mathcal{L}$ der Fehlerfunktion entspricht, welche wie folgt definiert ist:

\begin{equation}
\mathcal{L} = \sum _{i=1} ^{N} (y_i - \hat{y}_i)^2 \text{.}
\end{equation}

Das Minimierungsproblem wird mit dem \ac{BFGS} Verfahren gelöst.






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Systemtheorie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Systemtheorie}
Ziel der Systemtheorie ist die Modellierung eines Systems und dessen Verhaltens. Das Systemverhalten wird mittels eines mathematischen Modells beschrieben. Ein System $\mathcal{H}$ besitzt einen Eingang $u$, internen Zuständen $x$ und einen Ausgang $y$.

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=2]
% Rechteck zeichnen
\draw (1,0) rectangle (4,1) node[midway] {Dynamisches System};
% Pfeil von links
\draw [->] (-1,0.5) -- node[midway,above] {Eingang} node[midway,below] {u} (1,0.5);
% Pfeil aus Rechteck
\draw [->] (4,0.5) -- node[midway,above] {Ausgang} node[midway,below] {y} (6,0.5);
\end{tikzpicture}
\caption{Schaubild eines Systems mit Eingang~$u$ und Ausgang~$y$.}
\label{fig:schaubild_dynamisches_system}
\end{figure}

Ein System~$\mathcal{H}$ ist eine abstrahierte Anordung, die mehrere Signale zueinander in Beziehung setzt. Es entspricht der Abbildung eines oder mehrerer Eingangssignale~$u(t)$ auf ein oder mehrere Ausgangssingnale~$y(t) = \mathcal{H} \{ u(t) \}$ \cite{frey2008signal}. Alle nötigen Informationen um das zukünftige Systemverhalten zu bestimmen, sind im Systemzustand~$x(t)$ mit~$x(t) \in \mathbb{R}^n$ enthalten, wobei $n \in \mathbb{N}$ der Anzahl an internen Zuständen entspricht~\cite{sr_diehl_2022}.

Die Änderung der Zustände über die Zeit~$\dot{x}(t)$ wird durch eine Differentialgleichung~$f$ mit~$f:~\mathbb{R}^n~\times~\mathbb{R}^m~\rightarrow~\mathbb{R}^n$ beschrieben:

\begin{equation} \label{eq:Theorie:Systemtheorie:Differentialgleichung}
\dot{x}(t) = f(x(t), u(t)) \text{.}
\end{equation}

Der Ausgang des Systems wird mit~$y(t)$ bezeichnet. Es wird angenommen, dass alle Zustände des Systems gemessen werden können. In der Praxis muss dies nicht immer der Fall sein.
Ebenso wird durch die Messung des Ausgangs ein  Rauschen~$w(t)$ auf die Zustände addiert wird. Für den Ausgang des Systems mit $y, w: \mathbb{R} \rightarrow \mathbb{R}^\mathrm{n}$, $t \mapsto y(t), w(t)$, wobei~$n \in \mathbb{N}$ der Anzahl an Zuständen entspricht, gilt:
            
\begin{equation}
y(t) = I \cdot x(t) + w(t) \text{.}
\end{equation}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Simulation}
Im Modellierungsteil dieser Arbeit wird eine \ac{ODE} hergeleitet, welche die Änderung der Rotorzustände beschreibt. Außerdem ist ein Initialzustand~$x_\mathrm{0}$ des Rotors vorgegeben. Gesucht ist eine Lösung der \ac{ODE}, die den Initialzustand erfüllt. Probleme dieser Klasse werden als Anfangswert- bzw. \ac{IVP} bezeichnet. Gemäß dem Satz von Picard-Lindelöf existiert unter der Annahme, dass~$f$ in~$x$ und~$t$ kontinuierlich ist, genau eine lokale, eindeutige Lösung des Anfangswertproblems. Um das Verhalten des Systems über die Zeit effizient und genau bestimmen zu können, wird ein passendes Simulationswerkzeug benötigt. Eine analytische Lösung des \ac{IVP} ist meist nicht möglich bzw. zu teuer, weshalb man sich numerischen Simulationsmethoden bedient~\cite{noc_diehl_2022}.

Simulationsmethoden approximieren die exakte Lösung eines \ac{IVP}. Dabei wird der Zustandsvektor zunächst über die Zeit diskretisiert: $t_k := t_0 + k \Delta t$ wobei $k = 0, \dots, N$ der Anzahl der Simulationsschritte und~$t_0$ der Startzeit entspricht. Die Approximation der exakten Lösung zum Zeitpunkt~$t_k$, also $x(t_k)$, entspricht~$s_k$~\cite{noc_diehl_2022}.

Simulationsmethoden lassen sich grob in einstufige bzw. mehrstufige sowie explizite und implizite Methoden unterteilen. Die Methoden unterscheiden sich darin, wie sie die Lösung des \ac{IVP} approximieren. Bei hinreichend guter Simulationsauflösung~$\Delta t$ mit~$\Delta t \rightarrow 0$ konvergieren jedoch alle Approximationen~$s_k$ gegen die exakte Lösung~$x(t_k)$: $s_k \rightarrow x(t_k)$~\cite{noc_diehl_2022}.

Um das Verhalten des Systems zu simulieren wird in dieser Arbeit das explizite, vierstufige Runge-Kutta-Verfahren verwendet. Es wertet die \ac{ODE} an vier verschiedenen Stellen~$s_{k,1}$,~$s_{k,2}$,~$s_{k,3}$ und~$s_{k,4}$ aus:  

\begin{equation}
\begin{aligned}
s_{k,1} &:= s_k, \\
s_{k,2} &:= s_k + \Delta t \cdot a_{21} f(s_{k,1}, u_{k,1}), \\
s_{k,3} &:= s_k + \Delta t \cdot a_{31} f(s_{k,1}, u_{k,1}) + \Delta t \cdot a_{32} f(s_{k,2}, u_{k,2}), \\
s_{k,4} &:= s_k + \Delta t \cdot a_{41} f(s_{k,1}, u_{k,1}) + \Delta t \cdot a_{42} f(s_{k,2}, u_{k,2}) + \Delta t \cdot a_{43} f(s_{k,3}, u_{k,3}) \mathrm{.}
\end{aligned}
\end{equation}

Der nächste Zustand~$s_{k+1}$ wird aus den Auswertestellen~$s_k$ approximiert. Die einzelnen Auswertestellen werden mit $b_j$ gewichtet, wobei gilt:

\begin{equation}
s_{k+1} := s_k + \Delta t \sum _{j=1} ^{4} b_j f(s_{k,j}, u_{k,j}) := F(s_k) \mathrm{.}
\end{equation}

Die Koeffizienten der einzelnen Auswertepunkte werden meist in einem Butcher Tableau zusammengefasst. Das Tableau für das vierstufige, explizite Runge-Kutta-Verfahren ist in Tabelle~\ref{ButcherTableauRungeKutta4thOrder} abgebildet. 

\begin{table}[h]
\begin{center}
\begin{tabular}{c|cccc}
0 \\
$\frac{1}{2}$ & $\frac{1}{2}$ \\
$\frac{1}{2}$ & 0 & $\frac{1}{2}$ \\
1 & 0 & 0 & 1 \\
\hline
& $\frac{1}{6}$ & $\frac{1}{3}$ & $\frac{1}{3}$ & $\frac{1}{6}$ \
\end{tabular}
\end{center}
\caption{Butcher Tableau für die Runge-Kutta Methode 4. Grades \cite{butcher1996history}.}
\label{ButcherTableauRungeKutta4thOrder}
\end{table}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Künstliches neuronales Netz
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Künstliches neuronales Netz}
\todo{überarbeiten}
%% Allgemein
Künstliche neuronale Netze werden verwendet um nichtlineare Funktionen zu approximieren. In dieser Arbeit wird ein vorwärtsgerichtetes neuronales Netz, auch bekannt als \ac{FNN}, verwendet. Es handlet sich um ein vorwärtsgerichtetes Netz, da es keine Rückkopplung zwischen den einzelnen Schichten gibt. Da das neuronale Netzwerk von der Funktionsweise von biologischen neuronalen Netzen inspiriert ist, weist es Ähnlichkeiten zu diesen auf. Bei dem Netz handelt es sich um einen azyklischen, gerichteten Graphen. Ziel ist es mittels eines \ac{FNN} eine nichtlineare Funktion zu approximieren. Gemäß des Universellen Approximations-Theorems kann jede stetige Funktion beliebig gut durch ein neuronales Netz approximiert werden.


%% Aufbau des Netzes
Abbildung~\ref{NeuronalesNetzAufbau} zeigt ein \ac{FNN}. Es besteht aus drei Arten von Schichten: einer Eingabeschicht~$i$, beliebig vielen versteckten Schichten~$h$ und einer Ausgabeschicht~$o$. Die Anzahl an versteckten Schichten wird als $n_\mathrm{lay}$ bezeichnet. Die einzelnen versteckten Schichten~$h^{(l)}$ werden durch den Index~$l$ mit~\mbox{$l=1,...,n_\mathrm{lay}$} unterschieden. Jede Schicht enthält sogenannte Neuronen, welche durch ihren Index~$j$ mit~\mbox{$j=1,...,n_\mathrm{nod}$} identifiziert werden, wobei~$n_\mathrm{nod}$ der Anzahl der Neuronen pro versteckter Schicht entspricht. Es wird angenommen, dass alle versteckten Schickten die gleiche Anzahl an Neuronen aufweisen. Auf die Funktionsweise der Neuronen wird später genauer eingegangen.

Im Folgenden wird angenommen, dass die Eingabeschicht genauso viele Neuronen enthält wie die zu approximierende Funktion~$f:~\mathbb{R}^n~\times~\mathbb{R}^m~\rightarrow~\mathbb{R}^n$ Eingänge besitzt, also~$n+m$. Die Anzahl der Neuronen in der Ausgabeschicht entspricht der Dimension des Ausgangs der Funktion~$f$. Von jedem Neuron der Eingangs- und versteckten Schichten existiert eine gewichtete und gerichtete Kante zu jedem Neuron der Nachfolgerschicht. Die Kantengewichte werden mit~$w_{s,e}^{(l)}$ bezeichnet, wobei~$s$ dem Index des Startneurons und~$e$ der Index des Endknotens entspricht. Der Index~$(l)$ entspricht der Startschicht. Jedes Neuron der versteckten Schichten erhält somit~$n_\mathrm{nod}$ gewichtete Eingabekanten von der Vorgängerschicht.

Für die Ausgabe~$a_j^{(l)}$ eines Neurons in Abhängigkeit von der Aktivierungsfunktion~$g$ und den mit~$w_{s,e}^{(l-1)}$ gewichteten Ausgabe der Vorgängerschicht~$h^{(l-1)}$ gilt somit:

\begin{equation}
a_{j}^{(l)} = g \cdot \bigg( \sum _{j=1} ^{n_\mathrm{nod}} w_{s,e}^{(l-1)} \cdot a_\mathrm{j}^{(l-1)} \bigg) \mathrm{.}
\end{equation}

Häufig verwendete Aktivierungsfunktionen sind ReLU, Sigmoid und Tanh. Ziel ist es, die Kantengewichte so zu wählen, dass die Ausgabe des Netzes möglichst der Ausgabe der \ac{ODE} entspricht. Der \ac{MSE} der Vorhersage wird mittels der Funktion~$\mathcal{L}$ bestimmt:
\begin{equation}
\mathcal{L} = \frac{1}{n_\mathrm{nod}} \sum _{i=1} ^{n_\mathrm{nod}} (y - \hat{y})^2 \mathrm{.}
\end{equation}

Der Prozess der Kantengewichtanpassung wird als Backpropagation bezeichnet.

\begin{figure}[h]
\centering
\includesvg[width=0.7\columnwidth]{Graphiken/StrukturNeuralesNetz.drawio.svg}
\caption{Aufbau eines künstlichen neuronalen Netzes.}
\label{NeuronalesNetzAufbau}
\end{figure}


%% Hyperparameteroptimierung
Das im vorangegangenen Abschnitt beschriebene Modell besitzt zahlreiche Hyperparameter~$\Lambda$, die für die Nachahmung der \ac{ODE} optimiert werden müssen. Für das beschriebene Modell muss die Anzahl der Schichten $n_\mathrm{lay}$, die Anzahl der Neuronen pro Schicht $n_\mathrm{nod}$, die Aktivierungsfunktion der Neuronen~$g$, die Lernrate~$\alpha_\mathrm{lr}$ sowie die Anzahl der Trainingsepochen $n_\mathrm{epo}$ optimiert werden. Es gilt, eine Kombination von Hyperparametern $\lambda \in \Lambda$ zu finden, die den Fehler des Modells minimiert:

\begin{equation}
\lambda^* = \underset{\lambda \in \Lambda}{\arg\min} , \mathcal{L}(\hat{y}^{\mathrm{opt}}_{\lambda}(D^{\mathrm{train}}, D^{\mathrm{val}})) \mathrm{.}
\end{equation}

Um die optimalen Werte für die Hyperparameter zu finden, wird der verfügbare Datensatz~$D$ in einen Trainingsdatensatz~$D^{\mathrm{train}}$, einen Validierungsdatensatz~$D^{\mathrm{val}}$ und einen Testdatensatz~$D^{\mathrm{test}}$ aufgeteilt. In jedem Durchlauf werden die Kantengewichte mittels des Trainingsdatensatzes optimiert und die Hyperparameter mit dem Validierungsdatensatz. Die Qualität des Modells wird mittels des Testdatensatzes ermittelt.

Da es meistens unzählig viele Hyperparameterkombinationen gibt, existieren verschiedene Methoden, die mit möglichst wenig Auswertungen versuchen, die optimalen Hyperparameter zu approximieren. Zu den simpleren Hyperparameteroptimierungsmethoden gehören Random-Search und Grid-Search. Bei Random-Search werden zufällige Hyperparameter aus dem Hyperparameterraum~$\Lambda$ gewählt. Grid-Search rastert den Hyperparameterraum systematisch ab.

Die Ergebnisse der beiden Methoden sind sehr ähnlich, Random-Search ist jedoch deutlich effizienter \cite{sklearn_2023}. Neben Random- und Grid-Search gibt es auch deutlich effizientere Methoden, die die Ergebnisse aus vorangegangenen Auswertungen verwenden, um die nächste Kombination auszuprobieren, worauf allerdings in dieser Arbeit nicht weiter eingegangen wird.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Optimale Steuerung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Optimale Steuerung}
\todo{TODO}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Strahltheorie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Strahltheorie}
\todo{TODO}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Blattelementetheorie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Blattelementetheorie} \label{TheorieBlattelementetheorie}
\todo{TODO}
