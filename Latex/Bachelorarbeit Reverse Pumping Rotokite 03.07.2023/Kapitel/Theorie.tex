%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemein
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Ziel dieses Abschnitts besteht darin, die theoretischen Grundlagen zu wiederholen, die für das Verständnis der folgenden Kapitel erforderlich sind. Es wird auf die Aerodynamik von Drehflüglern eingegangen, welche essentiell für die Herleitung der auf den Rotor wirkenden Kräfte ist. Das nötige Vokabular um das Rotor-System zu beschreiben, wird im Systemtheorie-Abschnitt erläutert. Abschnitt~\ref{Theorie:Parameterschätzung}:~\nameref{Theorie:Parameterschätzung} beschreibt einen Ansatz um Rotorparameter zu approximieren, welche sich nur mit großem Aufwand direkt messen lassen würden. Im Simulationsabschnitt werden die nötigen Werkzeuge erläutert um das Rotorverhalten zu simulieren. Abschließend werden die Grundlagen des Aufbaus und der Hyperparameteroptimierung eines Künstlichen Neuronalen Netzes behandelt.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Aerodynamik
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Aerodynamik} \label{Theorie:Aerodynamik}
% Allgemein
Der Rotor wird von verschiedenen Strömungen beeinflusst. Einerseits wird der Rotor durch seine Rotation und seine Bewegung durch die Luft beeinflusst, andererseits ist der von ihm erzeugte Abwind von großer Relevanz. Gemäß dem Gesetz von Bernoulli wirkt auf einen Körper mit Angriffsfläche~$A$, welcher von einem Medium der Dichte~$\rho$ und Geschwindigkeit~$v$ umströmt wird eine Kraft~$F$:

\begin{equation} \label{eq:Bernoulli}
F \propto \frac{1}{2} \rho A v^2 \mathrm{.}
\end{equation}

% Auftriebskraft und Widerstandskraft
Es wird zwischen der Auftriebskraft~$F_\mathrm{L}$ und der Widerstandskraft~$F_\mathrm{D}$ unterschieden. Die Auftriebskraft beschreibt die dynamische Kraft auf den umströmten Körper~\cite{greulich1998lexikon_auftriebsbeiwert}. Die Kraft wirkt senkrecht zur Strömungsrichtung und ist abhängig vom dimensionslosen Auftriebsbeiwert~$C_\mathrm{L}$. Die Widerstandskraft wirkt parallel zur Strömungsrichtung. Sie ist Abhängig von der Oberflächenrauigkeit und Form des Körpers und ist abhängig vom größenlosen Widerstandsbeiwert~$C_\mathrm{D}$. Der Widerstandsbeiwert für bestimmte Körper kann in Tabellen nachgeschlagen werden~\cite{greulich1998lexikon_widerstandsbeiwert}. Die beiden Beiwerte fassen den Einfluss der Flügeleingenschaften und -form zusammen. Durch Umstellen von~(\ref{eq:Bernoulli}) erhält man die Formeln für den Auftriebsbeiwert~$C_\mathrm{L}$ bzw. den Widerstandsbeiwert~$C_\mathrm{D}$:

\begin{equation}
C_\mathrm{L} = \frac{1}{2} \frac{F_\mathrm{L}}{\rho \cdot v^2 \cdot S} \quad \mathrm{bzw.} \quad C_\mathrm{D} = \frac{1}{2} \frac{F_\mathrm{D}}{\rho \cdot v^2 \cdot S} \mathrm{.}
\end{equation}


% Graphik
Abbildung~\ref{RotorKoefficientWinkel} zeigt den für einen beispielhaften Flügel experimentell bestimmten Zusammenhang zwischen Anstellwinkel~$\gamma$ und dem Auftriebsbeiwert~$C_\mathrm{L}$. Es wird deutlich, dass für kleine Winkel~$\alpha$ nahezu ein linearen Zusammenhang zwischen dem Anstellwinkel und dem Auftriebsbeiwert bzw. dem Widerstandsbeiwert existiert. Für kleine Winkel wird angenommen:

\begin{equation}
C_\mathrm{L} = C_\mathrm{L0} + C_\mathrm{L\alpha} \cdot \alpha \quad \mathrm{bzw.} \quad C_\mathrm{D} = C_\mathrm{D0} + C_\mathrm{D\alpha} \cdot \alpha \mathrm{.}
\end{equation}

Die Steigung des linearen Bereichs wird mit~$C_\mathrm{L\alpha}$ bzw.~$C_\mathrm{D\alpha}$ beschrieben, während~$C_\mathrm{L0}$ und~$C_\mathrm{D0}$ den Auftriebs- bzw. Widerstandsbeiwert in der Ausgangslage beschreiben. Die Koeffizienten~$C_\mathrm{L\alpha}$, $C_\mathrm{D\alpha}$, $C_\mathrm{L0}$ und $C_\mathrm{D0}$ werden meist experimentell ermittelt~\cite{benson_2021}.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/RotorKoefficientWinkel.png}
\caption{Zusammenhang zwischen Anstellwinkel und Auftriebsbeiwert.}
\label{RotorKoefficientWinkel}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameterschätzung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Parameterschätzung} \label{Theorie:Parameterschätzung}
Wie bereits in den vorherigen Abschnitten deutlich wurde, wird der Rotor durch eine Vielzahl an Parametern beschrieben. Während sich manche Parameter leicht ermitteln lassen, erfordert die Bestimmung anderer Parameter aufwendige Messungen. So lässt sich die Gesamtmasse~$m$ des Rotors durch eine simple Messung bestimmen, während die Bestimmung der Auftriebs- und Widerstandsbeiwerte $C_{L\alpha}, C_{D\alpha}, C_{L0}, C_{D0}$ einen Windkanal erfordern würde.

Ziel der Parameterschätzung ist es die unbekannten Parameter~$\theta = [C_{L\alpha}, C_{D\alpha}, C_{L0}, C_{D0}]^\mathrm{T}$ auch ohne aufwendige direkte Messungen der Werte zu bestimmen. Zunächst werden möglichst viele unterschiedliche Messungen der Rotorausgänge~$y$ mit dem realen Rotor durchgeführt: $y = [y_1, \dots , y_N]^\mathrm{T}$ wobei $N$ der Anzahl an Messreihen entspricht. Im Anschluss wird ein mathematisch-physikalisches Modell für das Rotorverhalten hergeleitet, welches die unbekannten Parameter enthält. Die mit dem realen Rotor durchgeführten Experimente werden mit dem mathematisch-physikalischen Modell wiederholt. Das Ergebnis der Simulationen~$\hat{y} = [\hat{y}_1, \dots, \hat{y}_N]^\mathrm{T}$ sollte dabei möglich mit der realen Messung~$y$ übereinstimmen. Die Bestimmung der optimalen Werte~$\theta^*$ für die unbekannten Parameter stellt ein Optimierungsproblem da:

\begin{equation}
\theta^* = \underset{\theta}{\arg\min} \, \mathcal{L}(y - \hat{y}) \mathrm{.}
\end{equation}

Als Fehlerfunktion~$\mathcal{L}$ wird der \ac{MSE} verwendet:

\begin{equation}
\mathcal{L} = MSE = \frac{1}{N} \sum _{i=1} ^{N} (y_i - \hat{y}_i)^2 \mathrm{.}
\end{equation}

Das Minimierungsproblem wird mit dem \ac{BFGS} Verfahren gelöst.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Systemtheorie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Systemtheorie} \label{Theorie:Systemtheorie}
Ziel der Systemtheorie ist die Modellierung eines Systems und dessen Verhaltens. Das Systemverhalten wird mittels eines mathematischen Modells beschrieben. Ein System $\mathcal{H}$ besitzt einen Eingang $u$, internen Zuständen $x$ und einen Ausgang $y$.

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=2]
% Rechteck zeichnen
\draw (1,0) rectangle (4,1) node[midway] {Dynamisches System};
% Pfeil von links
\draw [->] (-1,0.5) -- node[midway,above] {Eingang} node[midway,below] {u} (1,0.5);
% Pfeil aus Rechteck
\draw [->] (4,0.5) -- node[midway,above] {Ausgang} node[midway,below] {y} (6,0.5);
\end{tikzpicture}
\caption{Schaubild eines Systems mit Eingang~$u$ und Ausgang~$y$.}
\label{fig:schaubild_dynamisches_system}
\end{figure}

Ein System~$\mathcal{H}$ ist eine abstrahierte Anordung, die mehrere Signale zueinander in Beziehung setzt. Es entspricht der Abbildung eines oder mehrerer Eingangssignale~$u(t)$ auf ein oder mehrere Ausgangssingnale~$y(t) = \mathcal{H} \{ u(t) \}$ \cite{frey2008signal}. Alle nötigen Informationen um das zukünftige Systemverhalten zu bestimmen, sind im Systemzustand~$x(t)$ mit~$x(t) \in \mathbb{R}^n$ enthalten, wobei $n \in \mathbb{N}$ der Anzahl an internen Zuständen entspricht~\cite{sr_diehl_2022}.

Die Änderung der Zustände über die Zeit~$\dot{x}(t)$ wird durch eine Differentialgleichung~$f$ mit~$f:~\mathbb{R}^n~\times~\mathbb{R}^m~\rightarrow~\mathbb{R}^n$ beschrieben:

\begin{equation}
\dot{x}(t) = f(x(t), u(t)) \mathrm{.}
\end{equation}

Der Ausgang des Systems wird mit~$y(t)$ bezeichnet. Es wird angenommen, dass alle Zustände des Systems gemessen werden können. In der Praxis ist dies jedoch nicht immer der Fall. Weiter wird angenommen, dass durch die Messung ein Rauschen~$w(t)$ auf die Zustände addiert wird. Für den Ausgang des Rotokites mit $y, w: \mathbb{R} \rightarrow \mathbb{R}^\mathrm{n}$, $t \mapsto y(t), w(t)$, wobei~$n \in \mathbb{N}$ der Anzahl an Zuständen entspricht, gilt:
            
\begin{equation}
y(t) = I \cdot x(t) + w(t) \mathrm{.}
\end{equation}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Simulation} \label{Theorie:Simulation}
Im Modellierungsteil dieser Arbeit wird eine \ac{ODE} hergeleitet, welche die Änderung der Rotorzustände beschreibt. Außerdem ist ein Initialzustand~$x_\mathrm{0}$ des Rotors vorgegeben. Gesucht ist eine Lösung der \ac{ODE}, die den Initialzustand erfüllt. Probleme dieser Klasse werden als Anfangswert- bzw. \ac{IVP} bezeichnet. Gemäß dem Satz von Picard-Lindelöf existiert unter der Annahme, dass~$f$ in~$x$ und~$t$ kontinuierlich ist, genau eine lokale, eindeutige Lösung des Anfangswertproblems. Um das Verhalten des Systems über die Zeit effizient und genau bestimmen zu können, wird ein passendes Simulationswerkzeug benötigt. Eine analytische Lösung des \ac{IVP} ist meist nicht möglich bzw. zu teuer, weshalb man sich numerischen Simulationsmethoden bedient~\cite{noc_diehl_2022}.

Simulationsmethoden approximieren die exakte Lösung eines \ac{IVP}. Dabei wird der Zustandsvektor zunächst über die Zeit diskretisiert: $t_k := t_0 + k \Delta t$ wobei $k = 0, \dots, N$ der Anzahl der Simulationsschritte und~$t_0$ der Startzeit entspricht. Die Approximation der exakten Lösung zum Zeitpunkt~$t_k$, also $x(t_k)$, entspricht~$s_k$~\cite{noc_diehl_2022}.

Simulationsmethoden lassen sich grob in einstufige bzw. mehrstufige sowie explizite und implizite Methoden unterteilen. Die Methoden unterscheiden sich darin, wie sie die Lösung des \ac{IVP} approximieren. Bei hinreichend guter Simulationsauflösung~$\Delta t$ mit~$\Delta t \rightarrow 0$ konvergieren jedoch alle Approximationen~$s_k$ gegen die exakte Lösung~$x(t_k)$: $s_k \rightarrow x(t_k)$~\cite{noc_diehl_2022}.

Um das Verhalten des Systems zu simulieren wird in dieser Arbeit das explizite, vierstufige Runge-Kutta-Verfahren verwendet. Es wertet die \ac{ODE} an vier verschiedenen Stellen~$s_{k,1}$,~$s_{k,2}$,~$s_{k,3}$ und~$s_{k,4}$ aus:  

\begin{equation}
\begin{aligned}
s_{k,1} &:= s_k, \\
s_{k,2} &:= s_k + \Delta t \cdot a_{21} f(s_{k,1}, u_{k,1}), \\
s_{k,3} &:= s_k + \Delta t \cdot a_{31} f(s_{k,1}, u_{k,1}) + \Delta t \cdot a_{32} f(s_{k,2}, u_{k,2}), \\
s_{k,4} &:= s_k + \Delta t \cdot a_{41} f(s_{k,1}, u_{k,1}) + \Delta t \cdot a_{42} f(s_{k,2}, u_{k,2}) + \Delta t \cdot a_{43} f(s_{k,3}, u_{k,3}) \mathrm{.}
\end{aligned}
\end{equation}

Der nächste Zustand~$s_{k+1}$ wird aus den Auswertestellen~$s_k$ approximiert. Die einzelnen Auswertestellen werden mit $b_j$ gewichtet, wobei gilt:

\begin{equation}
s_{k+1} := s_k + \Delta t \sum _{j=1} ^{4} b_j f(s_{k,j}, u_{k,j}) := F(s_k) \mathrm{.}
\end{equation}

Die Koeffizienten der einzelnen Auswertepunkte werden meist in einem Butcher Tableau zusammengefasst. Das Tableau für das vierstufige, explizite Runge-Kutta-Verfahren ist in Tabelle~\ref{ButcherTableauRungeKutta4thOrder} abgebildet. 

\begin{table}[h]
\begin{center}
\begin{tabular}{c|cccc}
0 \\
$\frac{1}{2}$ & $\frac{1}{2}$ \\
$\frac{1}{2}$ & 0 & $\frac{1}{2}$ \\
1 & 0 & 0 & 1 \\
\hline
& $\frac{1}{6}$ & $\frac{1}{3}$ & $\frac{1}{3}$ & $\frac{1}{6}$ \
\end{tabular}
\end{center}
\caption{Butcher Tableau für die Runge-Kutta Methode 4. Grades \cite{butcher1996history}.}
\label{ButcherTableauRungeKutta4thOrder}
\end{table}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Künstliches neuronales Netz
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Künstliches neuronales Netz} \label{Theorie:Künstliches neuronales Netz}
\todo{Dieser Abschnitt muss überarbeitet werden}
\subsubsection{Allgemein}
Künstliche neuronale Netze werden verwendet um nichtlineare Funktionen zu approximieren. In dieser Arbeit wird ein vorwärtsgerichtetes neuronales Netz, auch bekannt als \ac{FNN}, verwendet. Es handlet sich um ein vorwärtsgerichtetes Netz, da es keine Rückkopplung zwischen den einzelnen Schichten gibt. Da das neuronale Netzwerk von der Funktionsweise von biologischen neuronalen Netzen inspiriert ist, weist es Ähnlichkeiten zu diesen auf. Bei dem Netz handelt es sich um einen azyklischen, gerichteten Graphen. Ziel ist es mittels eines \ac{FNN} eine nichtlineare Funktion zu approximieren. Gemäß des Universellen Approximations-Theorems kann jede stetige Funktion beliebig gut durch ein neuronales Netz approximiert werden.

\subsubsection{Aufbau des Netzes}
Abbildung~\ref{NeuronalesNetzAufbau} zeigt ein \ac{FNN}. Es besteht aus drei Arten von Schichten: einer Eingabeschicht~$i$, beliebig vielen versteckten Schichten~$h$ und einer Ausgabeschicht~$o$. Die Anzahl an versteckten Schichten wird als $n_\mathrm{lay}$ bezeichnet. Die einzelnen versteckten Schichten~$h^{(l)}$ werden durch den Index~$l$ mit~\mbox{$l=1,...,n_\mathrm{lay}$} unterschieden. Jede Schicht enthält sogenannte Neuronen, welche durch ihren Index~$j$ mit~\mbox{$j=1,...,n_\mathrm{nod}$} identifiziert werden, wobei~$n_\mathrm{nod}$ der Anzahl der Neuronen pro versteckter Schicht entspricht. Es wird angenommen, dass alle versteckten Schickten die gleiche Anzahl an Neuronen aufweisen. Auf die Funktionsweise der Neuronen wird später genauer eingegangen.

Im Folgenden wird angenommen, dass die Eingabeschicht genauso viele Neuronen enthält wie die zu approximierende Funktion~$f:~\mathbb{R}^n~\times~\mathbb{R}^m~\rightarrow~\mathbb{R}^n$ Eingänge besitzt, also~$n+m$. Die Anzahl der Neuronen in der Ausgabeschicht entspricht der Dimension des Ausgangs der Funktion~$f$. Von jedem Neuron der Eingangs- und versteckten Schichten existiert eine gewichtete und gerichtete Kante zu jedem Neuron der Nachfolgerschicht. Die Kantengewichte werden mit~$w_{s,e}^{(l)}$ bezeichnet, wobei~$s$ dem Index des Startneurons und~$e$ der Index des Endknotens entspricht. Der Index~$(l)$ entspricht der Startschicht. Jedes Neuron der versteckten Schichten erhält somit~$n_\mathrm{nod}$ gewichtete Eingabekanten von der Vorgängerschicht.

Für die Ausgabe~$a_j^{(l)}$ eines Neurons in Abhängigkeit von der Aktivierungsfunktion~$g$ und den mit~$w_{s,e}^{(l-1)}$ gewichteten Ausgabe der Vorgängerschicht~$h^{(l-1)}$ gilt somit:

\begin{equation}
a_{j}^{(l)} = g \cdot \bigg( \sum _{j=1} ^{n_\mathrm{nod}} w_{s,e}^{(l-1)} \cdot a_\mathrm{j}^{(l-1)} \bigg) \mathrm{.}
\end{equation}

Häufig verwendete Aktivierungsfunktionen sind ReLU, Sigmoid und Tanh. Ziel ist es, die Kantengewichte so zu wählen, dass die Ausgabe des Netzes möglichst der Ausgabe der \ac{ODE} entspricht. Der \ac{MSE} der Vorhersage wird mittels der Funktion~$\mathcal{L}$ bestimmt:
\begin{equation}
\mathcal{L} = \frac{1}{n_\mathrm{nod}} \sum _{i=1} ^{n_\mathrm{nod}} (y - \hat{y})^2 \mathrm{.}
\end{equation}

Der Prozess der Kantengewichtanpassung wird als Backpropagation bezeichnet.

\begin{figure}[h]
\centering
\includesvg[width=0.7\columnwidth]{Graphiken/StrukturNeuralesNetz.drawio.svg}
\caption{Aufbau eines künstlichen neuronalen Netzes.}
\label{NeuronalesNetzAufbau}
\end{figure}


\subsubsection{Hyperparameteroptimierung} \label{Hyperparameteroptimierung}
Das im vorangegangenen Abschnitt beschriebene Modell besitzt zahlreiche Hyperparameter~$\Lambda$, die für die Nachahmung der \ac{ODE} optimiert werden müssen. Für das beschriebene Modell muss die Anzahl der Schichten $n_\mathrm{lay}$, die Anzahl der Neuronen pro Schicht $n_\mathrm{nod}$, die Aktivierungsfunktion der Neuronen~$g$, die Lernrate~$\alpha_\mathrm{lr}$ sowie die Anzahl der Trainingsepochen $n_\mathrm{epo}$ optimiert werden. Es gilt, eine Kombination von Hyperparametern $\lambda \in \Lambda$ zu finden, die den Fehler des Modells minimiert:

\begin{equation}
\lambda^* = \underset{\lambda \in \Lambda}{\arg\min} , \mathcal{L}(\hat{y}^{\mathrm{opt}}_{\lambda}(D^{\mathrm{train}}, D^{\mathrm{val}})) \mathrm{.}
\end{equation}

Um die optimalen Werte für die Hyperparameter zu finden, wird der verfügbare Datensatz~$D$ in einen Trainingsdatensatz~$D^{\mathrm{train}}$, einen Validierungsdatensatz~$D^{\mathrm{val}}$ und einen Testdatensatz~$D^{\mathrm{test}}$ aufgeteilt. In jedem Durchlauf werden die Kantengewichte mittels des Trainingsdatensatzes optimiert und die Hyperparameter mit dem Validierungsdatensatz. Die Qualität des Modells wird mittels des Testdatensatzes ermittelt.

Da es meistens unzählig viele Hyperparameterkombinationen gibt, existieren verschiedene Methoden, die mit möglichst wenig Auswertungen versuchen, die optimalen Hyperparameter zu approximieren. Zu den simpleren Hyperparameteroptimierungsmethoden gehören Random-Search und Grid-Search. Bei Random-Search werden zufällige Hyperparameter aus dem Hyperparameterraum~$\Lambda$ gewählt. Grid-Search rastert den Hyperparameterraum systematisch ab.

Die Ergebnisse der beiden Methoden sind sehr ähnlich, Random-Search ist jedoch deutlich effizienter \cite{sklearn_2023}. Neben Random- und Grid-Search gibt es auch deutlich effizientere Methoden, die die Ergebnisse aus vorangegangenen Auswertungen verwenden, um die nächste Kombination auszuprobieren, worauf allerdings in dieser Arbeit nicht weiter eingegangen wird.
