%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemein
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Allgemein}
\todo{Allgemein}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Parameter}
%% Rotorparameter
Im Folgenden sind die für die Simulationen verwendeten Parameter gelistet. \autoref{tab:AllgemeineParameter} enthält allgemeine Konstanten. Es werden zwei Rotoren untersucht: ein großer Rotor mit einem Durchmesser von etwa 2~m und ein Rotor mit einem Durchmesser von nur 0,3~m. Die in \autoref{Modelle} hergeleiteten Modelle benötigen zahlreiche Informationen über die Rotoren. Einige dieser Parameter, wie etwa die Abmessungen lassen sich leicht bestimmen, während andere Parameter nur mit großem Aufwand bestimmbar sind. \autoref{tab:ParameterLeichtGroßerRotor}, \autoref{tab:ParameterSchwerGroßerRotor} und \autoref{tab:ParameterKleinerRotor} sind die ermittelten Parameter der beiden Rotoren zu entnehmen.


%% Beiwerte
Für die in \autoref{Theorie: Aerodynamische Beiwerte} erläuteten Beiwerte wird angenommen, dass diese für kleine Winkel linear bzw. quadratisch zur Orientierung des Flügel sind. Die Rotorblätter des großen Rotors werden durch einen ovale Flügel approximiert~(siehe \autoref{Flügelform}). Die Beiwerte dieses Flügels~\cite{Airfoiltools_2023} wurden für verschiedene Flügelorientierungen experimentell bestimmt. \autoref{BeiwerteGraphik} zeigt eine die Auftriebs- und Widerstandsbeiwerte für verschiedene Angriffswinkel sowie eine lineare bzw. quadratische Approximation. Die Beiwerte des kleinen Rotors werden mittels Messungen des Rotorverhaltens in \autoref{Ergebnisse: Approximierung der Beiwerte des kleinen Rotors} approximiert.


\begin{figure}[h]
\centering
\includesvg[width=0.7\columnwidth]{Graphiken/Wing.svg}
\caption{Schaubild der für die Simulationen angenommenen Rotorblattform.}
\label{Flügelform}
\end{figure}


%% Zeitkonstante
Die Zeitkonstante~$T$ des induzierten Abwinds lässt sich ähnlich wie die Beiwerte ebenfalls nur schwer experimentell ermitteln, weshalb auf ein Modell~\cite{} zur Simulation des Rotorabwinds zurückgegriffen wird. Mit Hilfe des Modells wurde für verschiedene Rotorabmessungen und Zustände das Verhalten des Wakes untersucht. Wie in \autoref{Modelle: Abwind} vermutet, stellt sich heraus, dass die Zeitkonstante nicht nur von den Rotoreigenschaften, sondern auch von dessen Geschwindigkeit abhängt. Entsprechend müsste die Zeitkonstante in jedem Simulationsschritt neu berechnet werden. Dies würde den Umfang dieser Arbeit jedoch weit überschreiten, weshalb angenommen wird, dass~$T$ konstant ist. Für die in \autoref{tab:ParameterSchwerGroßerRotor} und \autoref{tab:ParameterKleinerRotor} angegeben Zeitkonstanten wurden zahlreiche Annahmen für den Rotor und dessen Geschwindigkeit getroffen, weshalb es sich nur um eine grobe Abschätzung der Größenordnung handelt.


\begin{table}[h]
\centering
\caption{Allgemeine Parameter.}
\label{tab:AllgemeineParameter}
\begin{tabularx}{\textwidth}{|c|X|c|}
\hline
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$g$ & Gravitationsbeschleunigung & 9,81 \si{m/s} \\
$\rho$ & Luftdichte & 1,225 \si{kg/m^3} \\
\hline
\end{tabularx}
\end{table}


\begin{table}[h]
\centering
\caption{Leicht zu bestimmende Parameter des großen Rotors.}
\label{tab:ParameterLeichtGroßerRotor}
\begin{tabularx}{\textwidth}{|c|X|c|}
\hline
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$m_\mathrm{nabe}$ & Masse der Rotornabe & 0,4 \si{kg} \\
$m_\mathrm{stab}$ & Masse einer Verbindungsstange & 0,02 \si{kg} \\
$m_\mathrm{blatt}$ & Masse eines Rotorblatts & 0,2 \si{kg} \\
$n$ & Anzahl an Rotorblättern & 3 \\
$r_\mathrm{nabe}$ & Distanz zwischen Nabenmittelpunkt und Nabenrand & 0,2 \si{m} \\
$r_\mathrm{stab}$ & Distanz zwischen Nabenmittelpunkt und Rotorblattanfang & 0,25 \si{m} \\
$r_\mathrm{blatt}$ & Distanz zwischen Nabenmittelpunkt und Rotorblattende & 1,1 \si{m} \\
$b_\mathrm{blatt}$ & Breite eines Rotorblatts & 0,3 \si{m} \\
\hline
\end{tabularx}
\end{table}


\begin{table}[h]
\centering
\caption{Schwer zu bestimmende Parameter des großen Rotors.}
\label{tab:ParameterSchwerGroßerRotor}
\begin{tabularx}{\textwidth}{|c|X|c|}
\hline
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$C_\mathrm{L\alpha}$ & Steigung des Auftriebsbeiwerts & 0,11 \si{\per \degree} \\
$C_\mathrm{L0}$ & Auftriebsbeiwert bei 0\degree & 0 \\
$C_\mathrm{D\alpha}$ & Steigung des Widerstandsbeiwert & 0,00015 \si{\per \degree} \\
$C_\mathrm{D0}$ & Widerstandsbeiwert bei 0\degree & 0,005 \\
$T$ & Zeitkonstante des Abwinds & 0,1 \si{s} \\
\hline
\end{tabularx}
\end{table}


\begin{table}[h]
\centering
\caption{Parameter des kleinen Rotors.}
\label{tab:ParameterKleinerRotor}
\begin{tabularx}{\textwidth}{|c|X|c|}
\hline
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$m_\mathrm{nabe}$ & Masse der Rotornabe & 1 \si{g} \\
$m_\mathrm{stab}$ & Masse einer Verbindungsstange & 0 \si{g} \\
$m_\mathrm{blatt}$ & Masse eines Rotorblatts & 25 \si{g} \\
$n$ & Anzahl an Rotorblättern & 3 \\
$r_\mathrm{nabe}$ & Distanz zwischen Nabenmittelpunkt und Nabenrand & 2,5 \si{cm} \\
$r_\mathrm{stab}$ & Distanz zwischen Nabenmittelpunkt und Rotorblattanfang &  2,5 \si{cm} \\
$r_\mathrm{blatt}$ & Distanz zwischen Nabenmittelpunkt und Rotorblattende &  12 \si{cm} \\
$b_\mathrm{blatt}$ & Breite eines Rotorblatts & 4 \si{cm} \\
$T$ & Zeitkonstante des Abwinds & 0,01 \si{s} \\
\hline
\end{tabularx}
\end{table}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validierung der Modelle und der Implementierung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Validierung der Modelle und der Implementierung}
\subsubsection{Allgemein}
In diesem Abschnitt erfolgt eine Überprüfung der Realitätsnähe der hergeleiteten Modelle. Da nicht genügend Messdaten zur Verfügung stehen, werden bei der Simulation des Rotorverhaltens bestimmte Annahmen getroffen, die eine Überprüfung der Ergebnisse ermöglichen. Es ist wichtig zu beachten, dass die Korrektheit der Validierungsszenarien keine allgemeine Korrektheit gewährleistet, sondern lediglich darauf hinweist, dass die Modelle für bestimmte Szenarien korrekt sind.


%%% Versuchsaufbau und -durchführung %%%
\subsubsection{Versuchsaufbau und -durchführung}
In diesem Abschnitt werden die Zustände des großen Rotors in zwei verschiedenen Szenarien überprüft. Einmal wird untersucht, wie sich der Rotor im luftleeren Raum verhält. Dazu wird die Luftdichte~$\rho$ auf Null gesetzt. Ebenso wird überprüft, wie sich der Rotor mit infinitesimal kleinen~($S \rightarrow 0$) bzw. gar keinen Flügeln im luftgefüllten Raum bewegt. Die Zustände zu Beginn der Simulation sind \autoref{tab:InvertialwerteValidierung} zu entnehmen.

\begin{table}[h]
\centering
\caption{Inertialwerte für die Simulation.}
\label{tab:InvertialwerteValidierung}
\begin{tabularx}{\textwidth}{|c|X|c|}
\hline
\textbf{Symbol} & \textbf{Beschreibung} & \textbf{Wert}\\
\hline
$z$ & Höhe & 0 \si{m} \\
$\dot{z}$ & vertikale Geschwindigkeit & 0 \si{m/s} \\
$\Omega$ & Rotationsgeschwindigkeit & 1 \si{U/s} \\
\hline
\end{tabularx}
\end{table}



%%% Ergebnisse %%%
\subsubsection{Ergebnisse}
%% Allgemeine Beschreibung
Abbildungen~\ref{ValidationNoAir} und \ref{ValidationNoWings} zeigen das mittels \ac{RK4} simulierte Verhalten des Rotors für die beiden oben beschriebenen Szenarien. Für die Simulation wurden die im \autoref{Modelle} hergeleiteten Modelle verwendet. Aus den Abbildungen wird deutlich, dass das Basis- und Referenzmodell nahezu identische Ergebnisse für beide Szenarien liefern, während das neuronale Netz teilweise erheblich von den beiden Modellen abweicht.


%% Beschreibung: Höhe
Der von den Modellen vorhergesagte Höhenverlauf ähnelt in beiden Szenarien einer umgedrehten Parabel mit Scheitelpunkt bei der Ausgangshöhe~$z_0 = 0~\si{m}$. Die nach zwei Sekunden zurückgelegte Strecke unterscheidet sich jedoch bei den Modellen: Während das Basis- und Referenzmodell eine Höhenabnahme von etwa~$19,5~\si{m}$ vorhersagt, geht das \ac{FNN} davon aus, dass die Abnahme lediglich~$7~\si{m}$ bzw.~$12~\si{m}$ aus.


%% Beschreibung: vertikale Geschwindigkeit
Laut dem Basis- und dem Referenzmodell nimmt die vertikale Geschwindigkeit linear mit der Zeit um~$-9,81~\si{m/s^2}$ zu. Die Simulation des neuronalen Netzes geht für den Rotor im luftleeren Raum ebenfalls von einem linearen Zusammenhang von~$-7,25~\si{m/s^2}$ aus. Für das zweite Szenario prophezeit das Netz eine auf etwa $10~\si{m/s}$ begrenzte Abnahme der Geschwindigkeit.


%% Beschreibung: Rotationsgeschwindigkeit
Die Rotationsgeschwindigkeit des Rotors bleibt gemäß dem Basis- und Referenzmodell konstant bei der ursprünglichen Geschwindigkeit von~$\Omega = 2~\si{U/s}$. Das \ac{FNN} geht davon aus, dass sich die Rotationsgeschwindigkeit auf zwei bzw. drei Umdrehungen pro Sekunde ändert.


%% Beschreibung: mittlere Kraft
Alle drei Modelle sagen voraus, dass der Rotor so gut wie keine Kraft auf die ihn umgebende Luft ausübt. Die Simulation vom Basis- und Referenzmodell zeigt eine konstante mittlere Kraft von~$\bar{F} = 0~\si{N}$, während die vom \ac{FNN} vorausgesagte Kraft von $0~\si{N}$ abweicht, jedoch immer kleiner als $1~\si{N}$ ist.


%% Beschreibung: Energie
Die kinetische Energie nimmt gemäß der Modelle in beiden Szenarien zu, während die potenzielle Energie quadratisch abnimmt. Die Rotationsenergie bleibt laut dem Basis- und Referenzmodell konstant, während sie laut dem neuronalen Netz leicht zunimmt. Die Gesamtenergie bleibt jedoch bei allen Modellen entweder konstant oder nimmt nur leicht ab.


\begin{figure}[h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationNoAir.svg}
\caption{Simuliertes Verhalten eines Rotors im luftleeren Raum.}
\label{ValidationNoAir}
\end{figure}


\begin{figure}[h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationNoWings.svg}
\caption{Simuliertes Verhalten eines Rotors ohne Rotorblätter.}
\label{ValidationNoWings}
\end{figure}



%%% Auswertung %%%
\subsubsection{Diskussion}
%% Erwartetes Verhalten
Befindet sich der Rotor im luftleeren Raum oder sind die Flügelflächen minimal groß, so sollte die Auftriebs- und Widerstandskraft gemäß dem Gesetz von Bernoulli~\eqref{eq:bernoulli_gesetz} null betragen. Die einzige wirkende Kraft ist demnach die Gravitationskraft, welche den Rotor nach unten beschleunigt. Die vertikale Beschleunigung~$\ddot{z}$ sollte demnach der Gravitationsbeschleunigung~$g$ entsprechen. Für die vertikale Geschwindigkeit~$\dot{z}$ wird erwartet, dass~$\dot{z}(t) = -g \cdot t$ gilt. Befindet sich der Rotor zu Beginn der Simulation in Ruhe, lässt sich für die Höhe gemäß dem Zeit-Ort-Gesetz erwarten: $z(t) = z_\mathrm{0} - \frac{1}{2} \cdot g \cdot t^2$. Da keine Drehmomente auf den Rotor wirken, sollte die Rotationsgeschwindigkeit konstant bleiben.


%% Diskussion: Höhe
Der vom Basis- und Referenzmodell vorhergesagt Höhenverlauf stimmt mit dem Zeit-Ort-Gesetz überein. Die Vorhersage des \ac{FNN} kann hingegen nur für wenige Sekundenbruchteile als annähernd korrekt bezeichnet werden, weicht dann jedoch deutlich vom erwarteten Verhalten ab, auch wenn die Tendenz stimmt.

%% Diskussion: vertikale Geschwindigkeit
Die vertikale Geschwindigkeit wird vom Basis- und Referenzmodell korrekt vorhergesagt. Das neuronale Netz liefert für kurze Zeit eine gute Näherung, weicht dann allerdings stark von den Erwartungen ab.


%% Diskussion: Rotationsgeschwindigkeit
Da keine Kräfte in horizontale Richtung wirken, existiert auch kein Drehmoment, das eine Rotationsbeschleunigung hervorrufen könnte. Entsprechend muss die Rotationsgeschwindigkeit konstant bleiben, was sich in der Simulation vom Basis- und Refernzmodell zeigt. Die Simulation des Netzes geht fälschlicherweise von einer leichten Zunahme bzw. Abnahme der Geschwindigkeit aus.


%% Diskussion: mittlere Kraft
Da keine Luft existiert und der Rotor keine Fläche besitzt, mit der er eine Kraft auf die Luft ausüben kann, wird erwartet, dass die mittlere Kraft durchgehend null ist. Während das Basis- und Referenzmodell dieser Erwartung vollständig entsprechen, geht das neuronale Netz davon aus, dass ein Abwind entsteht. Die vorhergesagte mittlere Kraft ist zwar im Vergleich zur Gravitationskraft gering, aber nicht vernachlässigbar.


%% Diskussion: Energie
Die Nettoleistung des Systems sollte null betragen, da keine Energie in das System hinein bzw. herausfließt. Diese Annahme trifft für alle Modelle für etwa eine Sekunde zu. Die zuerst genannten Modelle sagen die Gesamtenergie korrekt vorher, während letztgenanntes gegen Ende der Simulation geringfügig von den Erwartungen abweicht.


%% Allgemeine Diskussion
Ergebnis dieses Abschnitts ist, dass die Simulation des Basis- und Referenzmodell in den beiden beschriebenen Szenarien die Erwartungen voll erfüllt. Das \ac{FNN} liefert zwar von der Tendenz her ordentliche Werte, diese sind jedoch nur für wenige Sekundenbruchteile ausreichend korrekt.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auswertung der verschiedenen Modelle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Auswertung der verschiedenen Modelle}
\subsubsection{Allgemein}
In diesem Abschnitt sollen die verschiedenen Modelle hinsichtlich ihrer Genauigkeit und Effizienz untersucht werden. Zunächst wird die Genauigkeit vom Basismodell und dem Neuronalem Netz untersucht, das Referenzmodell liefert hierfür die Referenzwerte. Hierzu wird die vorhergesagte Zustandsänderung der beiden Modelle für verschiedene Zustände und Eingänge untersucht. Ebenso wird die Genauigkeit der beiden Modelle für ein realistisches (Pumping)-Szenario bestimmt. Im Anschluss wird untersucht, wie lange die Simulation der verschiedenen Modelle dauert.


\subsubsection{Definitionen}
%% Mean-Squared-Error
Die \ac{MSE} wird genutzt, um die Genauigkeit eines Modells zu bestimmen. Mit der \ac{MSE} wird die mittlere quadratische Abweichung zwischen Vorhersage des Modells und dem wahren Wert bestimmt:

\begin{equation}
    \mathrm{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{y}_i)^2 \text{.}
\end{equation}


\subsubsection{Genauigkeit}
%%% Vergleich von ODEs für verschiedene Zustände
%% Durchführung
Für die Bestimmung der Vorhersagegenauigkeit wird der große Rotor verwendet. Die drei hergeleiteten Modelle werden hierfür für verschiedene Zustände und Eingänge untersucht und die ausgegebene Zustandsänderung verglichen. Es wird angenommen, dass die Höhe~$z$ keinen Einfluss auf die Zustandsänderung hat und demnach bei der weiteren Untersuchung konstant auf~$z = 10~\si{m}$ gehalten werden kann. Der \ac{MSE} zwischen dem Referenzmodell und dem Basismodell bzw. dem neuronalen Netz wird für alle in \autoref{} aufgeführten Zustands- und Eingangskombinationen überprüft.


%% Ergebnisse und Beschreibung der Ergebnisse
\autoref{HeatmapBasisModel} und \autoref{HeatmapNeuralNetwork} zeigen den \ac{MSE} des Basismodells und des \ac{FNN} für verschiedene Zustände. 

\begin{figure}[h]
\centering
\begin{minipage}[b]{0.45\linewidth}
\includesvg[width=\linewidth]{Graphiken/SimpleModelHeatMapF0N.svg}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.45\linewidth}
\includesvg[width=\linewidth]{Graphiken/SimpleModelHeatMapF50N.svg}
\end{minipage}
\caption{\ac{MSE} des Basismodells für $\bar{F} = 0~\si{N}$ (links) und $\bar{F} = 50~\si{N}$ (rechts).}
\label{HeatmapBasisModel}
\end{figure}



\begin{figure}[h]
\centering
\begin{minipage}[b]{0.45\linewidth}
\includesvg[width=\linewidth]{Graphiken/NeuralNetworkHeatMapF0N.svg}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.45\linewidth}
\includesvg[width=\linewidth]{Graphiken/NeuralNetworkHeatMapF50N.svg}
\end{minipage}
\caption{\ac{MSE} des Neuronalen Netzes für $\bar{F} = 0~\si{N}$ (links) und $\bar{F} = 50~\si{N}$ (rechts).}
\label{HeatmapNeuralNetwork}
\end{figure}


%% Interpretation der Ergebnisse
\todo{Interpretation der Ergebnisse}

%%% Squared R für Pumping-Szenario
%% Durchführung
Für die Berechnung des R-Square-Wertes muss zunächst ein realistisches Szenario ermittelt werden. In diesem Fall wird das in \autoref{Ergebnisse: Pumping-Szenario und optimale Steuerung} bestimmte Reverse-Pumping-Szenario verwendet. Alle drei Modelle werden mit dem aus dem Pumping-Szeniario gewonnen Eingangsvektor simuliert. Die Ausgänge der Modelle werden verwendet um die R2-Werte für das Basismodell und das neuronale Netz zu bestimmen.


%% Ergebnisse und Beschreibung der Ergebnisse
\todo{Ergebnisse und Beschreibung der Ergebnisse}
MSE-Basis = 0,43
MSE-Neural = 15,9


\begin{figure}[h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationAllModelsPumping.svg}
\caption{TODO TODO TODO.}
\label{GenauigkeitPumping}
\end{figure}


%% Interpretation der Ergebnisse
\todo{Interpretation der Ergebnisse}


\subsubsection{Berechnungsdauer}
\todo{Durchführung}
\todo{Ergebnisse und Beschreibung der Ergebnisse}
\todo{Interpretation der Ergebnisse}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auswirkung der Wahl der Zeitkontanten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Auswirkung der Wahl der Zeitkontanten}
\todo{Plot erneuern}
\todo{Beschreibung erneuern}

%% Allgemein
Das Basismodell und das Referenzmodell unterscheiden sich unter anderen darin, dass beim Referenzmodell beachtet wird, dass der Rotor eine Kraft auf die ihn umgebende Luft ausübt und somit einen Abwind erzeugt.

%% Bedeutung der Zeitkonstante
Im \autoref{Modelle: Abwind} wurde die Änderung dieser Kraft bereits hergeleitet. Die Geschwindigkeit der Änderung der mittleren Kraft und somit auch des Abwinds hängt von einer Zeitkonstanten~$T$ ab. Je kleiner die Zeitkonstante, desto schneller konvergiert die mittlere Kraft~$\bar{F}$ gegen die momentane Kraft, die der Rotor auf die Luft ausübt. 

%% Beschreibung
\autoref{WakesReferenceModel} zeigt das simulierte Rotorverhalten für verschiedene Zeitkonstanten~$T$. Es wird deutlich, dass die Zeitkonstante einen großen Einfluss auf das Verhalten des Rotors hat. Für kleine~$T$ nimmt die Höhe des Rotors innerhalb von zwei Sekunden fast doppelt so viel ab, wie für große~$T$. Der Betrag der vertikalen Geschwindigkeit ist für kleine Zeitkonstanten ebenfalls deutlich größer. Der Verlauf der Rotationsgeschwindigkeit hängt hingegen nur geringfügig von der Zeitkonstante ab. Die mittlere Kraft ändert sich erwartungsgemäß für kleine~$T$ am schnellsten. Es lässt sich vermuten, dass die mittlere Kraft für alle~$T$s gegen den gleichen Wert konvergieren. Der Energieverlust ist für kleine~$T$ größer als für große~$T$.

\begin{figure}[!h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationDifferentWakes.svg}
\caption{Simulation des Verhaltens des Rotors für verschiedene Abwindparameter.}
\label{WakesReferenceModel}
\end{figure}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pumping-Szenario und optimale Steuerung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Pumping-Szenario und optimale Steuerung} \label{Ergebnisse: Pumping-Szenario und optimale Steuerung}
\subsubsection{Versuchsaufbau und -durchführung}
%% Reverse-Pumping-Allgemein
Ziel des Reverse-Pumpings ist es den Rotor auch bei Windstille auf einer Höhe zu halten. Diese Forderung lässt sich, wie in \autoref{Theorie: Optimale Steuerung} beschrieben, als Nebenbedingung ausdrücken. Für den Rotor soll sowohl zu Beginn eines Pumping-Zyklus~($x[0]$) als auch am Ende~($x[n]$) gelten:

\begin{equation} \label{Nebenbedingung 1}
     g_1(z) = z[0] - z[n] \overset{!}{=} 0 \text{.}
\end{equation}

Diese Nebenbedingung alleine ist jedoch nicht ausreichend um den Rotor auf Dauer auf einer Höhe zu halten, den ein kurzes Aufsteigen und ein darauf folgender freier Fall wäre auch eine Lösung dieser Bedingung. Damit der Pumping-Prozess fortlaufend wiederholt werden kann, ohne, dass die Eingabe sich für jeden Zyklus ändert, ist es wünschenswert, wenn alle Zustände am Ende eines Zyklus den gleichen Wert wie zu Beginn annehmen. Es wird somit zusätzlich gefordert:

\begin{equation} \label{Nebenbedingung 2}
     g_2(z) = \dot{z}[0] - \dot{z}[n] \overset{!}{=} 0 \text{,}
\end{equation}

\begin{equation} \label{Nebenbedingung 3}
     g_3(z) = \Omega[0] - \Omega[n] \overset{!}{=} 0 \text{,}
\end{equation}

\begin{equation} \label{Nebenbedingung 4}
     g_4(z) = \bar{F}[0] - \bar{F}[n] \overset{!}{=} 0 \text{.}
\end{equation}


%% Optimierungsfunktion
Der Eingang sollte dabei so gewählt werden, dass die mittlere Kraft möglichst gering ausfällt:

\begin{equation}
    \underset{u(t)}{\mathrm{min}} \, \Lambda_1(u(t)) = \underset{u(t)}{\mathrm{min}} \,  F_\mathrm{T}(u(t))^2 \text{.}
\end{equation}

Sollte es nicht möglich sein eine Eingabe zu finden, sodass \eqref{Nebenbedingung 2}, \eqref{Nebenbedingung 3} und \eqref{Nebenbedingung 4} erfüllt werden, sollte die Abweichung dieser Zustände zumindest minimiert werden:

\begin{equation}
    \underset{u(t)}{\mathrm{min}} \, \Lambda_2(u(t)) = \underset{u(t)}{\mathrm{min}} \,  (x[0] - x[n])^2 \text{.}
\end{equation}


\subsubsection{Ergebnisse}
\todo{TODO}

\begin{figure}[!h]
\centering
\includesvg[width=\columnwidth]{Graphiken/SimulationPumping.svg}
\caption{TODO TODO TODO.}
\label{PumpingSzenario}
\end{figure}


\subsubsection{Auswertung}
\todo{TODO}
