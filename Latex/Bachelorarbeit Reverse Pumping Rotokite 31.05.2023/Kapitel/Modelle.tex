Ziel dieses Abschnitts ist es ein Modell herzuleiten, welches die Änderung der Rotorzustände beschreibt. Gesucht ist eine \ac{ODE}~$\dot{x} = f(x, u)$ welche zur Simulation des Rotorverhaltens verwendet werden kann. Zur Simulation des \ac{IVP} wird das im Abschnitt~\ref{Theorie:Simulation}:~\nameref{Theorie:Simulation} beschriebene \ac{RK4} verwendet. Das Modell basiert auf grundlegenden Kenntnissen der Physik von Drehflüglern und geht mit zahlreichen Annahmen einher, die in den entsprechenden Abschnitten erläutert werden.

Nach einer kurzen Beschreibung der Geometrie des Rotors sowie dessen Rotorzustände, Eingände und Ausgänge wird ein Referenzmodell hergeleitet, welches das Rotorverhalten möglichst genau beschreibt. Das Referenzmodell basiert auf der Blattelementetheorie auf die im entsprechenden Abschnitt genauer eingegangen wird. Außerdem wird in diesem Modell berücksichtigt, dass der Rotor einen Abwind erzeugt, der das Verhalten des Rotors ebenfalls beeinflusst.

Im Anschluss wird ein vereinfachtes Basismodell abgeleitet, welches deutlich mehr Annahmen trifft. Die unbekannten Parameter des Basismodells werden mittels Messreihen optimiert, die mit dem Referenzmodell erzeugt wurden.

Abschließend wird ein Neuronales Netz kreiert, welches die Zustandsänderung des Rotors vorhersagen soll. Das \ac{FNN} wird ebenfalls mit Daten trainiert, welche durch das Referenzmodell erzeugt wurden.  


\subsection{Zustände, Eingänge und Ausgänge}
Wie bereits im Abschnitt~\ref{Einleitung:Aufbau}:~\nameref{Einleitung:Aufbau} wird der Zustand~$x$ des Rotors durch seine Höhe~$z$, seine vertikale Geschwindigkeit~$\dot{z}$, seine Rotationsgeschwindigkeit~$\Omega$ und die mittlere auf die Luft ausgeübte Kraft~$\bar{F}$ beschrieben:

\begin{equation}
x(t) = \begin{bmatrix} z(t)\\ \dot{z}(t)\\ \Omega(t)\\ \bar{F}\end{bmatrix} \quad  \mathrm{mit} \quad x(0) = x_0 = \begin{bmatrix} z_0\\ \dot{z}_0\\ \Omega_0\\ \bar{F}_0 \end{bmatrix} \mathrm{.}
\end{equation}

Das Rotorkite wird mittels der Wahl des Einstellwinkels~$\beta$ und der Zugkraft~$F_\mathrm{Zug}$ gesteuert. Für den Eingang $u(t)$ gilt $u: \mathbb{R} \rightarrow \mathbb{R}^2$, $t \mapsto u(t)$ mit 

\begin{equation}
u(t) = \begin{bmatrix} \beta(t) \\ F_\mathrm{Zug}(t) \end{bmatrix} \mathrm{.}
\end{equation}

Es wird angenommen, dass alle Zustände des Systems gemessen werden können und der Ausgang~$y$ somit den Zuständen~$x$ entspricht.


\subsection{Geometrie des Rotors}
In Abbildung~\ref{RotorblattAbmessungen} sind die Abmessungen des Rotors schematisch dargestellt. Der Rotor besteht aus einer Rotornabe, an der die Rotorblätter mittels Stangen befestigt sind. Die Rotornabe besitzt die Masse~$m_\mathrm{Nabe}$ und den Radius~$r_\mathrm{Nabe}$. Die Distanz zwischen Nabenmittelpunkt und dem Rotorblattanfang wird mit $r_\mathrm{Stab}$ bezeichnet. Jede Stange besitzt die Masse~$m_\mathrm{Stab}$. Der Abstand zwischen dem Mittelpunkt und dem Ende der Rotorblätter der Masse~$m_\mathrm{Blatt}$ wird durch $r_\mathrm{Blatt}$ beschrieben. Es wird angenommen, dass die einzelnen Bauteile eine gleichmäßige Dichte besitzen. Für die Rotorblattfläche~$S$ in Abhängigkeit von der Blattlänge~$l = r_\mathrm{Blatt} - r_\mathrm{Stab}$ und der Blattbreite~$b$ gilt:
    
\begin{equation}
S = l \cdot b{.}
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/RotorblattAbmessungen.jpeg}
\caption{Schematische Darstellung eines Rotorblatts.}
\label{RotorblattAbmessungen}
\end{figure}

Abbildung~\ref{RotorblattBegriffe} zeigt den Querschnitt eines Rotorblatts. Die Verbindung zwischen der Vorderkante und der Hinterkante wird als Profilsehne bezeichnet. Die Rotorblätter befinden sich in der Ausgangslage, wenn die Profilsehne parallel zur Horizontalen ausgerichtet ist.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/RotorblattBegriffe.jpeg}
\caption{Graphik mit Rotorblatt-Begriffen.}
\label{RotorblattBegriffe}
\end{figure}



\subsection{Kräfte, Winkel und Bewegungen} \label{Theorie:Kräfte, Winkel und Bewegungen}
Um das Verhalten des Rotors zu untersuchen, ist es wichtig die auf den Rotor wirkenden Kräfte zu verstehen. Abbildung~\ref{KräfteWinkelRichtungen} zeigt eine schematische Darstellung dieser Kräfte. In diesem Abschnitt werden die vier wesentlichen Kräfte näher bestimmt.
    
Der Rotor ist dem Schwerefeld der Erde ausgesetzt und wird folglich durch die Gravitationskraft~$F_\mathrm{G}$ nach unten beschleunigt. Für die Gravitationskraft in Abhängigkeit von der Gravitationsbeschleunigung~$g=9,81~\mathrm{m/s^2}$ gilt:
    
\begin{equation} \label{eq:gravitation}
F_\mathrm{G} = g \cdot (m_\mathrm{Nabe} + n \cdot m_\mathrm{Stab} +  n \cdot m_\mathrm{Blatt}) = g \cdot m \mathrm{.}
\end{equation}

Weiter kann eine Zugkraft~$F_\mathrm{Zug}$ entgegen der z-Achse auf den Rotor ausgeübt werden. Bewegt sich der Rotor durch die Luft, so wirkt gemäß dem Gesetz von Bernoulli eine Kraft auf die Flügel, die den Rotor in vertikale und tangentiale Richtung beschleunigt. Für den Druck~$p$ der durch eine Strömung mit Geschwindigkeit~$v$ und Dichte~$\rho$ auf die Fläche~$A$ ausgeübt wird, gilt:

\begin{equation}
p = \frac{1}{2} \cdot \rho \cdot v^2 \mathrm{.}
\end{equation}
    
Um die auf den Rotor wirkende Kraft zu bestimmen, muss folglich die Geschwindigkeit des Luftstroms bestimmt werden. Diese hängt sowohl von der Rotationsgeschwindigkeit~$\Omega$, der Steiggeschwindigkeit~$\dot z$, als auch der Geschwindigkeit des durch den Rotor erzeugten Abwinds~$v_\mathrm{abw}$ ab~(siehe Abbildung~\ref{WindRichtung}). Für die Geschwindigkeit~$v_\mathrm{ges}$ des auf den Rotor strömenden Fluids in Abhängigkeit vom Radius~$r$ gilt:

\begin{equation} \label{eq:velocity}
v_\mathrm{ges}(r, z) = \sqrt{(\Omega \cdot r)^2 - (\dot{z} - v_\mathrm{abw})^2} \mathrm{.}
\end{equation}

Für den Anströmwinkel~$\alpha$ des Winds, in Abhängikeit vom Radius~$r$ und der Abwindgeschwindigkeit~$v_\mathrm{abw}$ gilt:

\begin{equation} \label{eq:alpha}
\alpha(r, v_\mathrm{abw}) = -\mathrm{arctan} \bigg(\frac{\dot z - v_\mathrm{abw}}{\Omega \cdot r} \bigg) \mathrm{.}
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/WindRichtung.jpeg}
\caption{Schematische Darstellung der Strömungen und Winkel.}
\label{WindRichtung}
\end{figure}
    
Da die Rotorblätter nicht immer parallel zur Horizentalen ausgerichtet sind, muss der Einstellwinkel~$\beta$ der Rotorblätter ebenfalls berücksichtigt werden. Der Winkel aus dem der Wind auf den Rotor trifft, wird mit $\gamma$ bezeichnet. Für den Anstellwinkel~$\gamma$ gilt:

\begin{equation}
\gamma(r, v_\mathrm{abw}) = \alpha(r, v_\mathrm{abw}) - \beta \mathrm{.}
\end{equation}

Da nun sowohl die Geschwindigkeit des Winds als auch dessen Richtung bekannt sind, können die Kräfte auf den Rotor bestimmt werden. Für die Auftriebskraft~$F_\mathrm{L}$ die auf ein Rotorblatt wirkt, gilt:

\begin{equation} \label{eq:auftrieb}
F_\mathrm{L}(\gamma, r) = \frac{1}{2} \rho S v_\mathrm{wind}(r)^2 C_\mathrm{L}(\gamma) \mathrm{.}
\end{equation}

Die Widerstandskraft~$F_\mathrm{D}$ die auf ein Rotorblatt wirkt, wird wie folgt berechnet:

\begin{equation} \label{eq:widerstand}
F_\mathrm{D}(\gamma, r) = \frac{1}{2} \rho S v_\mathrm{wind}(r)^2 C_\mathrm{D}(\gamma) \mathrm{.}
\end{equation}

Der Einfluss der Flügelform und insbesondere des Auftriebsbeiwerts~$C_\mathrm{L}$ und Widerstandsbeiwerts~$C_\mathrm{D}$ ist in Abschnitt~\ref{Theorie:Aerodynamik}:~\nameref{Theorie:Aerodynamik} näher erläutert.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/RotorKraefte.jpeg}
\caption{Schematische Darstellung der auf den Rotor wirkenden Kräfte.}
\label{KräfteWinkelRichtungen}
\end{figure}




\subsection{Referenzmodell}
\subsubsection{Allgemein}
Das Referenzmodell basiert auf der Blattelemententheorie. Wie in \cite{van2015grundlagen} beschrieben, werden die Rotorblätter in radialer Richtung numerisch zerlegt. Die Strömungsgeschwindigkeit und -winkel wird für jedes Element separat betrachtet. Es wird angenommen, dass die Strömung quasistationär ist. Für jedes Element wird die Strahltheorie angewandt. Eine Integration über den Rotorblattradius und Summierung der einzelnen Kräfte liefert das Drehmoment und die an der Rotornabe angreifende Kraft. 
    

\subsubsection{Methode und Annahmen}
% Winkel und Geschwindigkeiten
Im Gegensatz zu dem Basismodell auf Kapitel~\ref{SimpleModell} wird für das Referenzmodell nicht mehr angenommen, dass die vertikale Geschwindigkeit~$v_\mathrm{vert}$ über das gesamte Rotorblatt konstant ist. Stattdessen wird der Flügel in infitisimal kleine Abschnitte mit gleichem Radius zerlegt und die Geschwindigkeit des realen Winds und Anströmwinkels für jedes Blattelement separat mittels Gleichung~(\ref{eq:velocity}) und~(\ref{eq:alpha}) berechnet. Die Kräfte auf die einzelnen Elemente werden zu einer Auftriebs- und Widerstandskraft summiert.

Weiter wird diesmal berücksichtigt, dass nicht über den gesamten Radius Auftrieb generiert wird. Es wird davon ausgegangen, dass für $r < r_\mathrm{stab}$ weder eine Auftriebs- noch Abtriebskraft wirkt. Ebenso wird angenommen, dass Verluste an den Rotorblattspitzen auftreten~\cite{Glauert}, weshalb die letzten 3~\% des Radius ignoriert werden:

\begin{equation}
r_\mathrm{max} = 0,97 \cdot r_\mathrm{Blatt} \mathrm{.}
\end{equation}

Beim Referenzmodell wird beachtet, dass der Rotor eine Kraft auf die Luft ausübt und diese somit beschleunigt, sodass ein Abwind entsteht, welcher den Rotor ebenfalls beeinflusst. Die Geschwindigkeit des Abwinds~$v_\mathrm{abw}$ wird in Abschnitt~\ref{Referenzmodell:Herleitung der Systemgleichung} hergeleitet.

Während beim Basismodell angenommen wurde, dass der Rotor das Trägheitsmoment~$J$ eines Zylinders besitzt, wird dieses für das Referenzmodell genauer berechnet. Es wird angenommen, dass Rotornabe, die Verbindungsstäbe und die Rotorblätter unterschiedliche Dichten besitzen. Weiter wird der Abstand vom Mittelpunkt berücksichtigt. Für das Trägheitsmoment entlang der z-Achse gilt nach~\cite{Carceller_Candau_2020}:

\begin{equation}
\begin{aligned}
J &= n \cdot \bigg(\rho_\mathrm{core}  \cdot \int _0 ^{r_\mathrm{core}} x^2 dx + \rho_\mathrm{stick} \cdot \int _{r_\mathrm{core}} ^{r_\mathrm{stick}} x^2 dx + \rho_\mathrm{wing} \cdot \int _{r_\mathrm{stick}} ^{r_\mathrm{wing}} x^2 dx \bigg) \\
&= \frac{1}{3} r_\mathrm{core}^3 \frac{m_\mathrm{core}}{r_\mathrm{core}} + \frac{n}{3} r_\mathrm{stick}^3 \frac{m_\mathrm{stick}}{r_\mathrm{stick} - r_\mathrm{core}} - \frac{1}{3} r_\mathrm{core}^3 \frac{m_\mathrm{core}}{r_\mathrm{core}} + \frac{n}{3} r_\mathrm{wing}^3 \frac{m_\mathrm{wing}}{r_\mathrm{wing} - r_\mathrm{stick}} - \frac{n}{3} r_\mathrm{stick}^3 \frac{m_\mathrm{stick}}{r_\mathrm{stick} - r_\mathrm{core}} \mathrm{.}
\end{aligned}
\end{equation}
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Herleitung der Systemgleichung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Herleitung der Systemgleichung} \label{Referenzmodell:Herleitung der Systemgleichung}
% Herleitung Wake
Alle Kräfte die vertikal auf den Rotor wirken, wirken laut dem 3.~Newtonschen Gesetz auch auf die Luft. Die Luft wird somit durch den Rotor beschleunigt und besitzt eine Geschwindigkeit~$v_\mathrm{abw}$. Um diese Geschwindigkeit zu bestimmen, müssem alle vertikalen Rotorkräfte~$F_\mathrm{vert, tot}$ summiert werden:

\begin{equation}
F_\mathrm{vert, tot} = \sum F = n \cdot F_\mathrm{Lift, vert} - n \cdot F_\mathrm{Drag, vert} - F_\mathrm{Gravity} \mathrm{.}
\end{equation}

Für die Wakegeschwindigkeit gilt somit in Abhängigkeit von der Kraft auf die Luft~$F_\mathrm{vert, tot}$:

\begin{equation}
\bar{F} = \frac{1}{2} \rho A v_\mathrm{2}^2 \quad \Longleftrightarrow \quad v_\mathrm{2} = \frac{\sqrt{2 \bar{F}}}{\rho A} \mathrm{.}
\end{equation}

Die Geschwindigkeit des Abwinds~$v_2$ hängt von der mittleren Kraft~$\bar{F}$ auf die Luft über die Zeit ab:

\begin{equation}
\bar{F}(t) = \int _0 ^\infty w(\tau) F(t-\tau) d \tau = \int _{- \infty} ^t w(\tau) F(t -\tau) d(t - \tau) 
\end{equation}

wobei für die Zeitgewichtung~$w$ gilt: $w(\tau) = \frac{1}{T} e^{-\tau / T}$ und $\int _0 ^\infty w(\tau) = 1$. \\

Das in Kapitel~\ref{SimpleModell} beschriebene Modell wird um den Zustand~$\bar{F}$ erweitert um darauf die Abwindgeschwindigkeit zu bestimmen. Für die Änderung des neu eingeführten Zustands gilt:

\begin{equation}
\dot{\bar{F}} = \frac{1}{T} (F(t) - \bar{F(t)}) \mathrm{.}
\end{equation}

Für die Änderung der vertikalen Geschwindigkeit und der Rotortionsgeschwindigkeit gelten weiterhin die durch Gleichung~(\ref{eq:force drag vert}-\ref{eq:Moment}) hergeleiteten Zusammenhänge. Für die Änderung des Zustands~$x$:

\begin{equation}
x =
\begin{bmatrix} 
z \\
\dot z \\ 
\Omega \\
\bar{F}
\end{bmatrix}
\end{equation}

gilt somit:

\begin{equation}
f(x, u) = 
\begin{bmatrix}
\dot z \\ 
\ddot z \\ 
\dot \Omega \\ 
\bar{F}
\end{bmatrix}
=
\begin{bmatrix}
\dot z \\

\frac{1}{m}  \cdot [n \cdot F_\mathrm{L,v}(\gamma, \tilde{r}) - n \cdot F_\mathrm{D,v}(\gamma, \tilde{r}) - F_\mathrm{G}] \\

n \cdot \tilde{r} \cdot \frac{1}{2 \cdot m \cdot J} \cdot [F_\mathrm{L,h}(\gamma, \tilde{r}) - F_\mathrm{D,h}(\gamma, \tilde{r})] \\

\frac{1}{T} (F(t) - \bar{F(t)})
\end{bmatrix} \mathrm{.}
\end{equation}




\subsection{Grundmodell} \label{SimpleModell}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allgemein
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Allgemein} % Wofür wird das Modell benötigt?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Methode und Annahmen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Methode und Annahmen} % Welche Annahmen werden getroffen?
Bei der folgenden Modellierung werden zahlreiche Annahmen getroffen. Zwar wird der Modellierungsprozess vereinfacht anderseits nimmt die Genauigkeit des Modells ab. Da sich Fehler bei der Simulation summieren, können bereits kleine Fehler bei der Vorhersage der Zustandsänderung nach wenigen Sekundenbruchteilen zu großen Abweichungen von der Realität führen.

% Rotorannahmen
Für den Rotor wird angenommen, dass die tangentiale Geschwindigkeit~$v_\mathrm{t}$ unabhängig vom Radius~$r$ ist. Es wird davon ausgegangen, dass die tangentiale Geschwindigkeit überall der Geschwindigkeit beim halben Radius~$\tilde{r}$ mit~$\tilde{r}=0,5\cdot r_\mathrm{Blatt}$ entspricht:

\begin{equation}
v_\mathrm{t} = \Omega \cdot \tilde{r} \mathrm{.}
\end{equation}

Der Rotor wird durch eine infinitesimal dünne Scheibe approximiert, welche eine Druckdifferenz erzeugt~\cite{van2015grundlagen}. Weiter wird davon ausgegangen, dass über den gesamten Rotorradius~$0\le r \le r_\mathrm{Blatt}$ Schub erzeugt wird. Es wird somit ignoriert, dass der Rotor neben den Rotorblättern auch eine Rotornabe und Stäbe enthält, welche keinen Schub erzeugen. Die Form und Anzahl der Rotorblätter wird ebenfalls unbeachtet belassen. Zusätzlich wird außen vor gelassen, dass der Rotor einen Luftstrom generiert.

% Fliudannahmen
Außerdem wird angenommen, dass das Fluid, welches den Rotor umströmt eindimensional, qusistationär, inkompresibel und reibungsfrei sei. Innerhalb von Ebenen, welche parallel zum Rotor ausgerichtet sind, sollen außerdem identische Verhältnisse gelten. Zusätzlich wird angenommen, dass sich die Strömung nicht mit der Zeit ändert~\cite{van2015grundlagen}.


% Masseannahmen
Eine weitere Annahme ist, dass der Rotor das Trägheitsmoment~$J$ eines Zylinders besitzt:

\begin{equation}
J = \frac{1}{2} \cdot m \cdot \tilde{r}^2 \mathrm{.}
\end{equation}

Die Dichte des Rotors wird ebenso über den gesamten Radius als konstant angenommen.

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Herleitung der Systemgleichung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Herleitung der Systemgleichung}
% Zustände
Zunächst muss untersucht werden, welche Zustände~$x$ erforderlich sind, um den Rotor zu beschreiben, bevor eine \ac{ODE} aufgestellt werden kann, die die Zustandsänderung~$\dot{x}$ des Rotors beschreibt. Wie bereits im Abschnitt~\ref{Aufbau:Setup}:~\nameref{Aufbau:Setup} erwähnt, kann der Zustand des Rotors durch seine Höhe~$z$, seine vertikale Geschwindigkeit~$\dot{z}$ und seine Rotationsgeschwindigkeit~$\Omega$ beschrieben werden. Für den Zustand~$x$ und die zeitliche Änderung des Zustands~$\dot{x}$ gilt somit:

\begin{equation}
x =
\begin{bmatrix} 
z \\
\dot z \\ 
\Omega \\ 
\end{bmatrix} \quad \mathrm{bzw.} \quad
\dot{x} =
\begin{bmatrix} 
\dot{z} \\
\ddot z \\ 
\dot{\Omega} \\ 
\end{bmatrix} \mathrm{.}
\end{equation}

Es gilt eine Funktion~$f(x, u)$ aufzustellen, welche der Zustandsänderung~$\dot{x}$ beschreibt. Die Änderung der Höhe~$z$, also die vertikale Geschwindigkeit, lässt sich auf dem ursprünglichen Zustand entnehmen.

Das Aufstellen einer Gleichung, die die Änderung der vertikalen Geschwindigkeit~$\dot{z}$, also $\ddot{z}$ beschreibt, ist bereits deutlich komplizierter. Gemäß dem 2. Newtonschen Axiom gilt für die Kraft~$F$, die auf einen Körper der Masse~$m$ in Abhängigkeit von der Beschleunigung~$a$:

\begin{equation} \label{eq:Newton}
F = m \cdot a = m \cdot \ddot{z} \mathrm{.}
\end{equation}

Um die vertikale Beschleunigung des Rotors zu erhalten, müssen alle Kräfte bestimmt werden, die auf den Rotor wirken. In Abschnitt~\ref{Theorie:Kräfte, Winkel und Bewegungen}:~\nameref{Theorie:Kräfte, Winkel und Bewegungen} wurden bereits die Ausdrücke für die Auftriebs-, Widerstands, Gravitations- und Zugkraft hergeleitet~(Gleichung~\ref{eq:gravitation},~\ref{eq:auftrieb}~und~\ref{eq:widerstand}).

Da sich der Rotor nur entlang der z-Achse bewegen kann, besteht nur Interesse an Kräften, die entlang dieser Achse wirken. Da die Gravitations- und Zugkraft entlang der z-Achse nach unten wirken, können diese vollständig einbezogen werden. Anders sieht es bei der Auftriebs- und Widerstandskraft aus: Da der relative Wind den Rotor mit dem Anströmwinkel~$\alpha$ erreicht, wirkt die Widerstandskraft ebenso in diese Richtung. Für den vertikalen Anteil~$F_\mathrm{D,v}$ an der Widerstandskraft~$F_\mathrm{D}$ in Abhängigkeit vom Anströmwinkel~$\alpha$ gilt:

\begin{equation} \label{eq:force drag vert}
F_\mathrm{D,v} = F_\mathrm{D} \cdot \mathrm{cos}(\alpha) \mathrm{.}
\end{equation}

Die Auftriebskraft~$F_\mathrm{L}$ ist stets senkrecht zur Strömungsrichtung gerichtet \cite{Hall_2022}. Für den vertikalen Kraftanteil~$F_\mathrm{L,v}$ gilt:

\begin{equation} \label{eq:force lift vert}
F_\mathrm{L,v} = F_\mathrm{L} \cdot \mathrm{sin}(\alpha) \mathrm{.}
\end{equation}

Durch Umstellen von Gleichung~(\ref{eq:Newton}) und Einsetzen der hergeleiteten vertikalen Kräfte (Gleichung~\ref{eq:gravitation},~\ref{eq:force drag vert}~und~\ref{eq:force lift vert}) erhält man die vertikale Beschleunigung des Rotors in Abhängigkeit vom Anströmwinkel~$\alpha$, Einstellwinkel~$\beta$ und Anstellwinkel~$\gamma$:

\begin{equation} \label{BasisModell:VertikaleBeschleunigung}
\ddot{z} = \frac{1}{m}  \cdot [n \cdot F_\mathrm{Lift}(\gamma, \tilde{r}) \cdot \mathrm{cos}(\alpha(\tilde{r})) - n \cdot F_\mathrm{Drag}(\gamma, \tilde{r}) \cdot \mathrm{sin}(\alpha(\tilde{r})) - F_\mathrm{Gravity}] \mathrm{.}
\end{equation}
    
Zuletzt muss noch eine Formel für die Rotationsbeschleunigung~$\dot{\Omega}$ gefunden werden. Für die Rotationsbeschleunigung in Abhängigkeit vom Trägheitsmoment~$J$ und dem Drehmoment~$M$ gilt allgemein:

\begin{equation}
\dot{\Omega} = \frac{M}{J} \mathrm{.}
\end{equation}

Für das Drehmoment~$M$ in Abhängigkeit vom Radius~$r$ mit $r = \tilde{r}$ gilt:

\begin{equation}
M = F \cdot \tilde{r} \mathrm{.}
\end{equation}

Da sich die Ebene in der sich der Rotor dreht orthogonal zur z-Achse befindet, sind nur horizontale Kräfte relevant. Für den horizontalen Anteil der Auftriebs- $F_\mathrm{L,h}$ bzw. Widerstandskraft $F_\mathrm{D,h}$ gilt:

\begin{equation} \label{BasisModell:Rotationsbeschleunigung}
F_\mathrm{L,h} = F_\mathrm{L} \cdot \mathrm{cos}(\alpha) \quad \mathrm{bzw.} \quad F_\mathrm{D,h} = F_\mathrm{D} \cdot \mathrm{sin}(\alpha)\mathrm{.}
\end{equation}

Für das Rotationsmoment gilt somit:

\begin{equation} \label{eq:Moment}
M = \tilde{r} \cdot (F_\mathrm{L,h} - F_\mathrm{D,h}) \mathrm{.}
\end{equation}
    

Mittels der Gleichungen~(\ref{BasisModell:VertikaleBeschleunigung}) und~(\ref{BasisModell:Rotationsbeschleunigung}) lässt sich schließlich die gesuchte Funktion für die Zustandsänderung aufstellen:

\begin{equation}
f(x, u) = 
\begin{bmatrix}
\dot z \\ 
\ddot z \\ 
\dot \Omega \\ 
\end{bmatrix}
=
\begin{bmatrix}
\dot z \\

\frac{1}{m}  \cdot [n \cdot F_\mathrm{L,v}(\gamma, \tilde{r}) - n \cdot F_\mathrm{D,v}(\gamma, \tilde{r}) - F_\mathrm{G}] \\

n \cdot \tilde{r} \cdot \frac{1}{2 \cdot m \cdot J} \cdot [F_\mathrm{L,h}(\gamma, \tilde{r}) - F_\mathrm{D,h}(\gamma, \tilde{r})]
\end{bmatrix} \mathrm{.}
\end{equation}




\subsection{Neuronales Netz} \label{Modelle:NeuronalesNetz}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modell
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Modell} \label{Modelle:NeuronalesNetz:Modell}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Daten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Daten} \label{Modelle:NeuronalesNetz:Daten}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Training
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Training} \label{Modelle:NeuronalesNetz:Training}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auswertung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Auswertung} \label{Modelle:NeuronalesNetz:Auswertung}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hyperparameteroptimierung
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Hyperparameteroptimierung} \label{Modelle:NeuronalesNetz:Hyperparameteroptimierung}



