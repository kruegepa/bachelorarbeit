%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Energiebedarf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Allgemein}
Energie ist unverzichtbar für jegliche Form von industriellen, wirtschaftlichen und gesellschaftlichen Wohlstand. Gleichzeitig stellt uns die Energieversorgung vor große Herausforderungen: Es besteht eine große Abhängigkeit von anderen Staaten. So importierte die EU im Jahr 2021 mehr als die Hälfte der benötigten Energie~\cite{Statistisches_Bundesamt_2023}.

Eine weitere Herausforderung stellt die im Übereinkommen von Paris angestrebte Klimaneutralität. Ein Großteil der europäischen Treibhausemissionen ist auf den Energieverbrauch zurückzuführen, entsprechend besteht in diesem Bereich großer Handlungsbedarf~\cite{EAA_2022}. Die EU-Mitgliedstaaten haben sich entsprechend darauf geeinigt bis 2030 den Anteil nachhaltiger Energiequellen am Gesamtenergieverbrauch auf mindestens 42,5~\% auszubauen um diesen Herausforderungen zu begegnen~\cite{Rat_der_EU_2023}.

In dieser Arbeit werden \ac{AWES} als eine Form der kostengünstigen und CO2-neutralen Energiegewinnung untersucht. \\



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Windkraftwerke
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Windkraftwerke}
Die Nutzung von Windenergie zählt zu den kostengünstigsten Formen der Energiegewinnung~\cite{kost3stromgestehungskosten}. Gemäß dem Gesetz von Bernoulli gilt für die Leistung~$P$ des Windes in Abhängigkeit von der durchströmten Fläche~$A$ und der Windgeschwindigkeit~$v$:

\begin{equation} \label{eq:PowerAreaVelocity}
P \propto \frac{1}{2} A v^3 \mathrm{.}
\end{equation}

Um die Leistung eines Windkraftwerks zu steigern muss demnach entweder die Rotorfläche~$A$ erhöht werden oder Wind mit höheren Geschwindigkeiten~$v$ \glqq geerntet\grqq{} werden. Entsprechend hat sich in den letzten Jahrzehnten der Rotorradius~$R$ von etwa 10~m auf 150~m erhöht. Die Leistung~$P$ eines Windkraftwerks wurde im gleichen Zeitraum von etwa 25~kW auf 10~MW gesteigert~\cite{6828249}. Die Leistungssteigerung geht mit höheren Kosten für die größeren Rotorblätter, einen stabileren Turm und leistungsstärkere Gondel einher, was die Anschaffungskosten signifikant ansteigen lässt~\cite{stehly20202019}. \\

Eine weitere Möglichkeit der Leistungssteigerung ist es Orte mit höheren Windgeschwindigkeiten aufzusuchen. Aus Gleichung~(\ref{eq:PowerAreaVelocity}) wird deutlich, dass die Leistung proportional zum Kubik der Windgeschwindigkeit ist. Die Windgeschwindigkeit nimmt mit der Höhe~$h$ zu

\begin{equation}
v(h) = v_0 \bigg [ \frac{\mathrm{ln}(\frac{h}{z_0})}{\mathrm{ln}(\frac{h_0}{z_0})} \bigg ]
\end{equation}

wobei $v_0$ der bekannten Windgeschwindigkeit bei der Höhe~$h_0$ und $z_0$ der Rauigkeit der Oberfläche entspricht \cite{6828249}. Abbildung~\ref{WindGeschwindigkeitHöhe} zeigt die durchschnittliche Windgeschwindigkeit in 10~m und 200~m Höhe über dem Boden. Es wird deutlich, dass die mittlere Windgeschwindigkeit in der Höhe weit größer als in Bodennähe ist.  

\begin{figure}[h]
\centering
\begin{minipage}[b]{0.45\linewidth}
\includegraphics[width=\linewidth]{Graphiken/Wind10m.png}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.45\linewidth}
\includegraphics[width=\linewidth]{Graphiken/Wind200m.png}
\end{minipage}
\caption{Durchschnittliche Windgeschwindigkeit in 10~m und 200~m Höhe \cite{Global_Wind_Atlas_2023}.}
\label{WindGeschwindigkeitHöhe}
\end{figure}

Da die Konstruktion von \ac{CWP} dieser Höhe nicht möglich ist, kommen nur \ac{AWES} in Betracht. \\

Flugwindkraftwerke sind Fluggeräte, welche nutzbare Energie erzeugen. Sie sind entweder frei fliegend oder mittels eines Kabels mit dem Boden verbunden. Die Energie wird entweder als Zugkraft oder direkt als Elektrizität bereitgestellt~\cite{ahrens2013airborne}. Abbildung~\ref{AirbornWindEnergySystem} zeigt Flugwindkraftwerk mit Kite, welches mittels einer Schnur mit der Bodenstation verbunden ist. \\

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/AirbornWindEnergySystem.jpeg}
\caption{Beispiel für Flugwindkraftwerk mit Kite.}
\label{AirbornWindEnergySystem}
\end{figure}

Bei \ac{CWP} machen die Kosten für die Rotorblätter, die Gondel und den Turm bei \ac{CWP} knapp die Hälfte der Anschaffungskosten aus~\cite{stehly20202019}. Aufgrund ihrer großen Flughöhe benötigen \ac{AWES} einen deutlich kleineren Rotor um die gleiche Leistung wie ein \ac{CWP} zu generieren. Weiter benötigen \ac{AWES} keinen Turm. Entsprechend ist davon auszugehen, dass die Initialkosten für \ac{AWES} pro KWh geringer als bei \ac{CWP} ausfallen. Weiter wird davon ausgegangen, dass die Betriebskosten aufgrund von weniger beweglichen Teilen etwa 25~\% geringer als bei herkömmlichen Windkraftwerken sind~\cite{6828249}. \\

Ein weiterer Vorteil von \ac{AWES} gegenüber \ac{CWP} ist der Flächenverbrauch \cite{osti_964608}. Da Flugwindkraftwerke nur eine kleine Bodenstation besitzen und auch der Rotor deutlich kleiner ist, verbrauchen sie nur etwa ein Zehntel der Fläche die ein \ac{CWP} gleicher Leistung benötigen würde \cite{osti_964608}. \\

Luftgestützte Windenergiesysteme haben den entscheidenden Nachteil, dass sie auf Wind angewiesen sind um ihre Flughöhe zu halten. Ist kein Wind vorhanden gleitet der Rotor zu Boden. Das Landen und Starten des Rotors erfordert zusätzliche Infrastruktur am Boden, stellt ein Sicherheitsrisiko da und mindert die Wirtschaftlichkeit der Anlage \cite{lozano2013reverse}. Entsprechend gilt es den Start- und Landeprozess so gut wie möglich zu vermeiden. \\

Das Startup Altaeros Energies statete den Rotor mit einem Heliumballon aus, der den Rotor auch bei ausbleibendem Wind in der gewünschten Höhe hält \cite{Matheson_2014}. Ein weiterer Ansatz ist es das System mit Propellern auszustatten, die bei Bedarf den nötigen Auftrieb erzeugen. Beide Ansätze besitzen zahlreiche Nachteile, die deren Einsatz unattraktiver machen. Beispielsweise müssen Helium-Ballons mit der Zeit nachgefüllt werden und zusätzliche Propeller und deren Energieversorgung würde mit einem höheren Gewicht einhergehen~\cite{lozano2013reverse}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Aufbau
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Aufbau} \label{Einleitung:Aufbau}
In dieser Arbeit wird ein weiterer Ansatz betrachtet, wie der Rotor auch bei Windstille in der Luft gehalten werden kann. Beim Reverse Pumping wird dem Rotor durch Ziehen an der Verbindungsschnur zum Boden kinetische Energie hinzugefügt. Diese wird wiederum in Rotationsenergie umgewandelt. Sobald das Ziehen beendet wurde, kann die Rotationsenergie verwendet werden um in potentielle Energie gewandelt zu werden. \\

Der betrachtete Aufbau besteht aus einem um die z-Achse rotierenden $n$-blättrigen Rotor mit Masse~$m$~(siehe Abbildung~\ref{Aufbau}). Der Rotor kann sich frei entlang der z-Achse durch die Luft der Dichte~$\rho$ bewegen. Der Zustand des Rotors wird durch dessen Höhe~$z$, der vertikalen Geschwindigkeit~$\dot{z}$, der Rotationsgeschwindigkeit~$\Omega$ und der mittleren auf die Luft ausgeübten Kraft~$\bar{F}$ beschrieben. Es besteht eine Verbindung zwischen Rotor und Boden über die eine Kraft~$F_\mathrm{Zug}$ nach unten auf den Rotor ausgeübt werden kann.
    
\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/Aufbau.jpeg}
\caption{Schematische Darstellung des Aufbaus.}
\label{Aufbau}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ziele und Herangehensweise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Ziele und Herangehensweise}
Ziel dieser Arbeit ist es das Rotorverhalten während des Reverse Pumping Prozesses zu modellieren und simulieren. Da aufgrund der komplexität des Aufbaus keine echten Messdaten vorhanden sind, wird zunächst ein Referenzmodell aufgestellt, welches zur Erzeugung von künstlichen Messdaten genutzt wird. Sollten zu einem späteren Zeitpunkt echte Messdaten vorliegen, kann das Referenzmodell ebenfalls zur Analyse des Rotorverhaltens genutzt werden. Neben dem Referenzmodell wird zunächst ein vereinfachtes Basismodell hergeleitet, welches das Verhalten des Rotors beschreiben soll. Außerdem wird ein Neuronales Netz mit den Messdaten trainiert und ebenfalls dazu genutzt um das Verhalten des Rotors vorherzusagen. Im Abschnitt~\ref{Ergebnisse}:~\nameref{Ergebnisse} wird das Basismodell mit dem Neuronalen Netz verglichen und die Simulationsergebnisse validert.
