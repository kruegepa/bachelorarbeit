%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modelle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Allgemein
Ziel dieses Abschnitts ist es verschiedene Modelle herzuleiten, welche die Änderung der Rotorzustände beschreiben. Gesucht ist eine \ac{ODE}~$\dot{x} = f(x, u)$ welche zur Simulation des Rotorverhaltens verwendet werden kann. Zur Simulation des \ac{IVP} wird das im Abschnitt~\ref{Theorie:Simulation}:~\nameref{Theorie:Simulation} beschriebene \ac{RK4} verwendet. Die Modelle basieren auf grundlegenden Kenntnissen der Physik von Drehflüglern und gehen mit zahlreichen Annahmen einher, die in den entsprechenden Abschnitten erläutert werden.

%% Vorgehen: Referenzmodell
Nach einer kurzen Beschreibung der Geometrie des Rotors sowie dessen Rotorzuständen, Eingängen und Ausgängen wird ein Referenzmodell hergeleitet, welches das Rotorverhalten möglichst genau beschreibt. Das Referenzmodell basiert auf der Blattelementetheorie~\cite{Drzewiecki_1892} auf die im entsprechenden Abschnitt genauer eingegangen wird. Außerdem wird in diesem Modell berücksichtigt, dass der Rotor eine Kraft auf die ihn umgebende Luft ausübt, was zu einem Abwind führt, der das Verhalten des Rotors ebenfalls beeinflusst.

%% Vorgehen: Basismodell
Im Anschluss wird ein vereinfachtes Basismodell abgeleitet, welches deutlich mehr Annahmen trifft. Die unbekannten Parameter des Basismodells werden mittels Messreihen optimiert, die mit dem Referenzmodell erzeugt wurden.

%% Vorgehen: Neuronales Netz
Abschließend wird ein Neuronales Netz kreiert, welches die Zustandsänderung des Rotors vorhersagen soll. Das \ac{FNN} wird ebenfalls mit Daten trainiert, welche durch das Referenzmodell erzeugt wurden.  



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Zustände, Eingänge und Ausgänge
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Zustände, Eingänge und Ausgänge} \label{Modelle:Zustände, Eingänge und Ausgänge}
%% Zustände
Wie bereits im Abschnitt~\ref{Einleitung:Aufbau}:~\nameref{Einleitung:Aufbau} erläutert wurde, wird der Zustand~$x$ des Rotors durch seine Höhe~$z$, seine vertikale Geschwindigkeit~$\dot{z}$, seine Rotationsgeschwindigkeit~$\Omega$ und die mittlere auf die Luft ausgeübte Kraft~$\bar{F}$ beschrieben:

\begin{equation}
x(t) = \begin{bmatrix} z(t)\\ \dot{z}(t)\\ \Omega(t)\\ \bar{F}\end{bmatrix} \quad  \mathrm{mit} \quad x(0) = x_0 = \begin{bmatrix} z_0\\ \dot{z}_0\\ \Omega_0\\ \bar{F}_0 \end{bmatrix} \mathrm{.}
\end{equation}


%% Eingänge
Das Rotorkite wird mittels der Wahl des Einstellwinkels~$\beta$ und der Zugkraft~$F_\mathrm{Zug}$ gesteuert. Für den Eingang $u(t)$ mit $u: \mathbb{R} \rightarrow \mathbb{R}^2$ und~$t \mapsto u(t)$ gilt 

\begin{equation}
u(t) = \begin{bmatrix} \beta(t) \\ F_\mathrm{Zug}(t) \end{bmatrix} \mathrm{.}
\end{equation}


%% Ausgänge
Es wird angenommen, dass alle Zustände des Systems gemessen werden können und der Ausgang~$y$ somit den Zuständen~$x$ entspricht.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Geometrie des Rotors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Geometrie des Rotors}
%% Abmessungen
In Abbildung~\ref{RotorblattAbmessungen} sind die Abmessungen des Rotors schematisch dargestellt. Der Rotor besteht aus einer Rotornabe, an der die Rotorblätter mittels Stangen befestigt sind. Die Rotornabe besitzt die Masse~$m_\mathrm{Nabe}$ und den Radius~$r_\mathrm{Nabe}$. Die Distanz zwischen Nabenmittelpunkt und dem Rotorblattanfang wird mit $r_\mathrm{Stab}$ bezeichnet. Jede Stange besitzt die Masse~$m_\mathrm{Stab}$. Der Abstand zwischen dem Mittelpunkt und dem Ende der Rotorblätter der Masse~$m_\mathrm{Blatt}$ wird durch $r_\mathrm{Blatt}$ beschrieben. Es wird angenommen, dass die einzelnen Bauteile eine gleichmäßige Dichte besitzen. Für die Rotorblattfläche~$S$ in Abhängigkeit von der Blattlänge~$l = r_\mathrm{Blatt} - r_\mathrm{Stab}$ und der Blattbreite~$b$ gilt:
    
\begin{equation}
S = l \cdot b{.}
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\columnwidth]{Graphiken/RotorblattAbmessungen.drawio.png}
\caption{Schematische Darstellung eines Rotorblatts.}
\label{RotorblattAbmessungen}
\end{figure}


%% Fachbegriffe für Rotorblatt
Abbildung~\ref{RotorblattBegriffe} zeigt den Querschnitt eines Rotorblatts. Die Verbindung zwischen der Vorderkante und der Hinterkante wird als Profilsehne bezeichnet. Die Rotorblätter befinden sich in der Ausgangslage, wenn die Profilsehne parallel zur Horizontalen ausgerichtet ist.

\begin{figure}[h]
\centering
\includegraphics[width=1\columnwidth]{Graphiken/RotorblattBegriffe.drawio.png}
\caption{Graphik mit Rotorblatt-Begriffen.}
\label{RotorblattBegriffe}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Kräfte, Winkel und Bewegungen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Kräfte, Winkel und Bewegungen} \label{Theorie:Kräfte, Winkel und Bewegungen}
%% Notwendigkeit von Kräften für Untersuchung
Um das Verhalten des Rotors zu untersuchen, ist es wichtig die auf den Rotor wirkenden Kräfte zu verstehen. Abbildung~\ref{KräfteWinkelRichtungen} zeigt eine schematische Darstellung dieser Kräfte, welche in diesem Abschnitt näher bestimmt werden.


%% Schwerkraft
Der Rotor ist dem Schwerefeld der Erde ausgesetzt und wird folglich durch die Gravitationskraft~$F_\mathrm{G}$ nach unten beschleunigt. Für die Gravitationskraft in Abhängigkeit von der Gravitationsbeschleunigung~$g=9,81~\mathrm{m/s^2}$ gilt:
    
\begin{equation} \label{eq:gravitation}
F_\mathrm{G} = g \cdot (m_\mathrm{Nabe} + n \cdot m_\mathrm{Stab} +  n \cdot m_\mathrm{Blatt}) = g \cdot m \mathrm{.}
\end{equation}


%% Zugkraft, Auftriebskraft und Widerstandskraft
Weiter kann eine Zugkraft~$F_\mathrm{Zug}$ entgegen der z-Achse auf den Rotor ausgeübt werden. Bewegt sich der Rotor durch die Luft, so wirkt gemäß dem Gesetz von Bernoulli eine Kraft auf die Rotorblätter, die den Rotor in vertikale und tangentiale Richtung beschleunigt. Für den Druck~$p$ der durch eine Strömung mit Geschwindigkeit~$v$ und Dichte~$\rho$ auf die Fläche~$A$ ausgeübt wird, gilt:

\begin{equation}
p = \frac{1}{2} \cdot \rho \cdot v^2 \mathrm{.}
\end{equation}

%% Herleitung von Windrichtung und -geschwindigkeit
Um die auf den Rotor wirkende Kraft zu bestimmen, muss folglich die Geschwindigkeit des Luftstroms bestimmt werden. Diese hängt sowohl von der Rotationsgeschwindigkeit~$\Omega$, der Steiggeschwindigkeit~$\dot z$, als auch der Geschwindigkeit des durch den Rotor erzeugten Abwinds~$v_\mathrm{abw}$ ab~(siehe Abbildung~\ref{WindRichtung}). Für die Geschwindigkeit~$v_\mathrm{wind}$ des auf den Rotor strömenden Fluids in Abhängigkeit vom Radius~$r$ gilt:

\begin{equation} \label{eq:velocity}
v_\mathrm{wind}(r, z) = \sqrt{(\Omega \cdot r)^2 - (\dot{z} - v_\mathrm{abw})^2} \mathrm{.}
\end{equation}

Für den Anströmwinkel~$\alpha$ des Winds, in Abhängikeit vom Radius~$r$ und der Abwindgeschwindigkeit~$v_\mathrm{abw}$ gilt:

\begin{equation} \label{eq:alpha}
\alpha(r, v_\mathrm{abw}) = -\mathrm{arctan} \bigg(\frac{\text{vert. Strömungsgeschwindigkeit}}{\text{Rotationsgeschwindigkeit}} \bigg) = -\mathrm{arctan} \bigg(\frac{\dot z - v_\mathrm{abw}}{\Omega \cdot r} \bigg) \mathrm{.}
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\columnwidth]{Graphiken/WindRichtung.drawio.png}
\caption{Schematische Darstellung der Strömungen und Winkel.}
\label{WindRichtung}
\end{figure}

%% Einstellwinkel
Da die Rotorblätter nicht immer parallel zum Horizont ausgerichtet sind, muss der Einstellwinkel~$\beta$ der Rotorblätter ebenfalls berücksichtigt werden. Der Winkel aus dem der Wind auf den Rotor trifft, wird mit $\gamma$ bezeichnet. Für den Anstellwinkel~$\gamma$ gilt:

\begin{equation}
\gamma(r, v_\mathrm{abw}) = \alpha(r, v_\mathrm{abw}) - \beta \mathrm{.}
\end{equation}


%% Berechnung von Auftriebs- und Widerstandskraft
Da nun sowohl die Geschwindigkeit des Winds als auch dessen Richtung bekannt sind, können die Kräfte auf den Rotor bestimmt werden. Für die Auftriebskraft~$F_\mathrm{L}$ die auf ein Rotorblatt wirkt, gilt:

\begin{equation} \label{eq:auftrieb}
F_\mathrm{L}(\gamma, r) = \frac{1}{2} \rho S v_\mathrm{wind}(r)^2 C_\mathrm{L}(\gamma) \mathrm{.}
\end{equation}

Die Widerstandskraft~$F_\mathrm{D}$ pro Rotorblatt, wird wie folgt berechnet:

\begin{equation} \label{eq:widerstand}
F_\mathrm{D}(\gamma, r) = \frac{1}{2} \rho S v_\mathrm{wind}(r)^2 C_\mathrm{D}(\gamma) \mathrm{.}
\end{equation}

Der Einfluss der Flügelform und insbesondere des Auftriebsbeiwerts~$C_\mathrm{L}$ und Widerstandsbeiwerts~$C_\mathrm{D}$ ist in Abschnitt~\ref{Theorie:Aerodynamik}:~\nameref{Theorie:Aerodynamik} näher erläutert.

\begin{figure}[!h]
\centering
\includegraphics[width=1\columnwidth]{Graphiken/KräfteWinkelRichtungen.drawio.png}
\caption{Schematische Darstellung der auf den Rotor wirkenden Kräfte.}
\label{KräfteWinkelRichtungen}
\end{figure}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Referenzmodell
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Referenzmodell}
\subsubsection{Allgemein}
Das Referenzmodell basiert auf der Blattelemententheorie. Wie in \cite{van2015grundlagen} beschrieben, werden die Rotorblätter in radialer Richtung numerisch zerlegt. Die Strömungsgeschwindigkeit und der Anströmwinkel wird für jedes Element separat betrachtet. Es wird angenommen, dass die Strömung quasistationär ist, sich also nicht mit der Zeit ändert. Eine Integration über den Rotorblattradius und Summierung der einzelnen Kräfte liefert das Drehmoment und die an der Rotornabe angreifende Kraft. 
    

\subsubsection{Methode und Annahmen}
%% Methode
Das Referenzmodell soll möglich präzise Ergebnisse liefern, weshalb nur wenige Annahmen getroffen werden. Die Rotorblätter werden in infitisimal kleine Abschnitte mit gleichem Radius zerlegt. Die Geschwindigkeit des realen Winds und Anströmwinkels für jedes Blattelement separat mittels Gleichung~(\ref{eq:velocity}) und~(\ref{eq:alpha}) berechnet. Die Kräfte auf die einzelnen Elemente werden zu einer Auftriebs- und Widerstandskraft summiert.

%% Annahmen
Ebenso wird berücksichtigt, dass der Rotor nicht über den gesamten Radius Auftrieb generiert. Es wird davon ausgegangen, dass für $r < r_\mathrm{stab}$ weder eine Auftriebs- noch Abtriebskraft wirkt. Ebenso wird angenommen, dass Verluste an den Rotorblattspitzen auftreten~\cite{Glauert_1928}, weshalb die letzten 3~\% des Radius ignoriert werden:

\begin{equation}
r_\mathrm{max} = 0,97 \cdot r_\mathrm{Blatt} \mathrm{.}
\end{equation}

Weiter wird beachtet, dass der Rotor eine Kraft auf die Luft ausübt und diese somit beschleunigt, sodass ein Abwind entsteht, welcher den Rotor ebenfalls beeinflusst. Die Geschwindigkeit des Abwinds~$v_\mathrm{abw}$ wird in Abschnitt~\ref{Referenzmodell:Herleitung der Systemgleichung} hergeleitet.

Für das Trägheitsmoment~$J$ wird angenommen, dass Rotornabe, die Verbindungsstäbe und die Rotorblätter unterschiedliche Dichten besitzen. Weiter wird der Abstand vom Mittelpunkt berücksichtigt. Für das Trägheitsmoment entlang der z-Achse gilt nach~\cite{Carceller_Candau_2020}:

\begin{equation}
\begin{aligned}
J &= n \cdot \bigg(\rho_\mathrm{core}  \cdot \int _0 ^{r_\mathrm{core}} x^2 dx + \rho_\mathrm{stick} \cdot \int _{r_\mathrm{core}} ^{r_\mathrm{stick}} x^2 dx + \rho_\mathrm{wing} \cdot \int _{r_\mathrm{stick}} ^{r_\mathrm{wing}} x^2 dx \bigg) \\
&= \frac{1}{3} r_\mathrm{core}^3 \frac{m_\mathrm{core}}{r_\mathrm{core}} + \frac{n}{3} r_\mathrm{stick}^3 \frac{m_\mathrm{stick}}{r_\mathrm{stick} - r_\mathrm{core}} - \frac{1}{3} r_\mathrm{core}^3 \frac{m_\mathrm{core}}{r_\mathrm{core}} + \frac{n}{3} r_\mathrm{wing}^3 \frac{m_\mathrm{wing}}{r_\mathrm{wing} - r_\mathrm{stick}} - \frac{n}{3} r_\mathrm{stick}^3 \frac{m_\mathrm{stick}}{r_\mathrm{stick} - r_\mathrm{core}} \mathrm{.}
\end{aligned}
\end{equation}



\subsubsection{Herleitung der Systemgleichung} \label{Referenzmodell:Herleitung der Systemgleichung}
%% Allgemein
Zunächst muss untersucht werden, welche Zustände~$x$ erforderlich sind, um den Rotor zu beschreiben, bevor eine \ac{ODE} aufgestellt werden kann, die die Zustandsänderung~$\dot{x}$ des Rotors beschreibt. Wie bereits im Abschnitt~\ref{Aufbau:Setup}:~\nameref{Aufbau:Setup} erwähnt, kann der Zustand des Rotors durch seine Höhe~$z$, seine vertikale Geschwindigkeit~$\dot{z}$ und seine Rotationsgeschwindigkeit~$\Omega$ beschrieben werden. Für den Zustand~$x$ und die zeitliche Änderung des Zustands~$\dot{x}$ gilt somit:

\begin{equation}
x =
\begin{bmatrix} 
z \\
\dot z \\ 
\Omega \\ 
\bar{F}
\end{bmatrix} \quad \text{bzw.} \quad
\dot{x} =
\begin{bmatrix} 
\dot{z} \\
\ddot z \\ 
\dot{\Omega} \\ 
\dot{\bar{F}}
\end{bmatrix} \mathrm{.}
\end{equation}



%% Herleitung: Änderung der Höhe
Es gilt eine Funktion~$f(x, u)$ aufzustellen, welche der Zustandsänderung~$\dot{x}$ beschreibt. Die Änderung der Höhe~$z$, also die vertikale Geschwindigkeit, lässt sich auf dem ursprünglichen Zustand entnehmen.


%% Herleitung: Änderung der vertikale Geschwindigkeit
Das Aufstellen einer Gleichung, die die Änderung der vertikalen Geschwindigkeit~$\frac{d \dot{z}}{dt} = \ddot{z}$ beschreibt, ist bereits deutlich aufwendiger. Gemäß dem zweiten Newtonschen Axiom gilt für die Kraft~$F$, die auf einen Körper der Masse~$m$ in Abhängigkeit von der Beschleunigung~$a$ wirkt:

\begin{equation} \label{eq:Newton}
F = m \cdot a = m \cdot \ddot{z} \mathrm{.}
\end{equation}

Um die vertikale Beschleunigung des Rotors zu erhalten, müssen somit alle vertikalen Kräfte bestimmt werden, die auf den Rotor wirken. Da die Gravitationskraft~(\ref{eq:gravitation}) und die Zugkraft~$F_\mathrm{Zug}$ entlang der z-Achse wirken, müssen diese vollständig einbezogen werden. Die Widerstandskraft~$F_\mathrm{D}$~(\ref{eq:widerstand}) wirkt parallel zur Strömungsrichtung und ist somit vom Anströmwinkel~$\alpha$ abhängig. Für den vertikalen Anteil~$F_\mathrm{D,v}$ an der Widerstandskraft~$F_\mathrm{D}$ in Abhängigkeit vom Anströmwinkel~$\alpha$ gilt:

\begin{equation} \label{eq:force drag vert}
F_\mathrm{D,v} = F_\mathrm{D} \cdot \mathrm{cos}(\alpha) \mathrm{.}
\end{equation}

Die Auftriebskraft~$F_\mathrm{L}$ ist stets senkrecht zur Strömungsrichtung gerichtet \cite{Hall_2022}. Für den vertikalen Kraftanteil~$F_\mathrm{L,v}$ gilt:

\begin{equation} \label{eq:force lift vert}
F_\mathrm{L,v} = F_\mathrm{L} \cdot \mathrm{sin}(\alpha) \mathrm{.}
\end{equation}

Durch Umstellen von Gleichung~(\ref{eq:Newton}) und Einsetzen der hergeleiteten vertikalen Kräfte (Gleichung~\ref{eq:gravitation},~\ref{eq:force drag vert}~und~\ref{eq:force lift vert}) erhält man die vertikale Beschleunigung des Rotors in Abhängigkeit vom Anströmwinkel~$\alpha$, Einstellwinkel~$\beta$ und Anstellwinkel~$\gamma$:

\begin{equation} \label{BasisModell:VertikaleBeschleunigung}
\ddot{z} = \frac{1}{m}  \cdot [n \cdot F_\mathrm{L,v}(\gamma, r) - n \cdot F_\mathrm{D,v}(\gamma, r) - F_\mathrm{G}] \mathrm{.}
\end{equation}



%% Herleitung: Änderung der Rotationsgeschwindigkeit
Ebenso muss eine Formel für die Rotationsbeschleunigung~$\dot{\Omega}$ gefunden werden. Für die Rotationsbeschleunigung in Abhängigkeit vom Trägheitsmoment~$J$ und dem Drehmoment~$M$ gilt allgemein:

\begin{equation} \label{eq:BasisModell:Rotationsbeschleunigung}
\dot{\Omega} = \frac{M}{J} \mathrm{.}
\end{equation}

Für das Drehmoment~$M$ in Abhängigkeit vom Radius~$r$ mit $r = \tilde{r}$ gilt:

\begin{equation}
M = F \cdot r \mathrm{.}
\end{equation}

Da sich die Ebene in der sich der Rotor dreht orthogonal zur z-Achse befindet, sind nur horizontale Kräfte relevant. Für den horizontalen Anteil der Auftriebs-~$F_\mathrm{L,h}$ bzw. Widerstandskraft~$F_\mathrm{D,h}$ gilt:

\begin{equation}
F_\mathrm{L,h} = F_\mathrm{L} \cdot \mathrm{cos}(\alpha) \quad \text{bzw.} \quad F_\mathrm{D,h} = F_\mathrm{D} \cdot \mathrm{sin}(\alpha)\mathrm{.}
\end{equation}

Für das Rotationsmoment gilt somit:

\begin{equation} \label{eq:Moment}
M = r \cdot (F_\mathrm{L,h} - F_\mathrm{D,h}) \mathrm{.}
\end{equation}



%% Herleitung: Änderung der mittleren Kraft
Alle Kräfte die vertikal auf den Rotor wirken, wirken laut dem 3.~Newtonschen Gesetz auch auf die Luft. Die Luft wird somit durch den Rotor beschleunigt und besitzt eine Geschwindigkeit~$v_\mathrm{abw}$. Es wird davon ausgegangen, dass die Auswirkungen von Gravitations- und Zugkraft nur eine geringfügige Auswirkung auf die Luft haben und somit vernachlässigt werden können. Für die relevanten vertikalen Kräfte~$\bar{F}$ gilt:

\begin{equation} \label{eq:ForceVerticalTotal}
\bar{F} = n \cdot F_\mathrm{L, v} - n \cdot F_\mathrm{D,v} \mathrm{.}
\end{equation}

Aus dieser Kraft lässt sich die Geschwindigkeit des Abwinds in der Rotorebene bestimmten:

\begin{equation}
\bar{F} = \frac{1}{2} \rho A v_\mathrm{abw}^2 \quad \Longleftrightarrow \quad v_\mathrm{abw} = \frac{\sqrt{2 \bar{F}}}{\rho A} \mathrm{.}
\end{equation}

Es wird angenommen, dass für die Änderung des Zustands~$\bar{F}$ gilt:

\begin{equation}
\dot{\bar{F}} = \frac{1}{T} (F(t) - \bar{F}(t))
\end{equation}

wobei~$T$ der zeitlichen Verzögerung des Abwinds entspricht. Hierbei handelt es sich um eine lineare Differentialgleichung erster Ordnung. Die Lösung der Differentialgleichung lautet:

\begin{equation}
\bar{F} = e^{\frac{t}{T}} \bar{F}_0 + \int _0 ^t \frac{1}{T} e^{\frac{t-\tau}{T}} F(\tau) d\tau = \int _{-\infty} ^t w(\tau) F(t - \tau) d(t - \tau) \mathrm{.}
\end{equation}

\begin{equation}
\bar{F} = e^{\frac{t}{T}} \bar{F}_0 + \int _0 ^t \frac{1}{T} e^{\frac{t-\tau}{T}} F(t - \tau) d\tau \mathrm{.}
\end{equation}

wobei für die Zeitgewichtung~$w$ gilt: $w(\tau) = \frac{1}{T} e^{-\tau / T}$ und $\int _0 ^\infty w(\tau) = 1$.



%% Systemgleichung
Für die Änderung der vertikalen Geschwindigkeit und der Rotortionsgeschwindigkeit gelten weiterhin die durch Gleichung~(\ref{eq:force drag vert}-\ref{eq:Moment}) hergeleiteten Zusammenhänge. Für die Änderung des Gesamtzustands~$x$ gilt somit:

\begin{equation}
f(x, u) = 
\begin{bmatrix}
\dot z \\ 
\ddot z \\ 
\dot \Omega \\ 
\dot{\bar{F}}
\end{bmatrix}
=
\begin{bmatrix}
\dot z \\

\frac{1}{m}  \cdot [n \cdot F_\mathrm{L,v}(\gamma, r) - n \cdot F_\mathrm{D,v}(\gamma, r) - F_\mathrm{G}] \\

n \cdot r \cdot \frac{1}{2 \cdot m \cdot J} \cdot [F_\mathrm{L,h}(\gamma, r) - F_\mathrm{D,h}(\gamma, r)] \\

\frac{1}{T} (F(t) - \bar{F}(t))
\end{bmatrix} \mathrm{.}
\end{equation}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grundmodell
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Grundmodell} \label{Grundmodell}
\subsubsection{Allgemein} % Wofür wird das Modell benötigt?
Bei der folgenden Modellierung werden zahlreiche Annahmen getroffen. Zwar wird der Modellierungsprozess vereinfacht anderseits nimmt die Genauigkeit des Modells ab. Da sich Fehler bei der Simulation summieren, können bereits kleine Fehler bei der Vorhersage der Zustandsänderung nach wenigen Sekundenbruchteilen zu großen Abweichungen von der Realität führen.


\subsubsection{Methode und Annahmen} % Welche Annahmen werden getroffen?
% Rotorannahmen
Für den Rotor wird angenommen, dass die tangentiale Geschwindigkeit~$v_\mathrm{t}$ unabhängig vom Radius~$r$ ist. Es wird davon ausgegangen, dass die tangentiale Geschwindigkeit überall der Geschwindigkeit beim halben Radius~$\tilde{r}$ mit~$\tilde{r}=0,5\cdot r_\mathrm{Blatt}$ entspricht:

\begin{equation}
v_\mathrm{t} = \Omega \cdot \tilde{r} \mathrm{.}
\end{equation}

Der Rotor wird durch eine infinitesimal dünne Scheibe approximiert, welche eine Druckdifferenz erzeugt~\cite{van2015grundlagen}. Weiter wird davon ausgegangen, dass über den gesamten Rotorradius~$0\le r \le r_\mathrm{Blatt}$ Schub erzeugt wird. Es wird somit ignoriert, dass der Rotor neben den Rotorblättern auch eine Rotornabe und Stäbe enthält, welche keinen Schub erzeugen. Die Form und Anzahl der Rotorblätter wird ebenfalls unbeachtet belassen. Zusätzlich wird außen vor gelassen, dass der Rotor einen Luftstrom generiert.

% Fliudannahmen
Außerdem wird angenommen, dass das Fluid, welches den Rotor umströmt eindimensional, qusistationär, inkompresibel und reibungsfrei sei. Innerhalb von Ebenen, welche parallel zum Rotor ausgerichtet sind, sollen außerdem identische Verhältnisse gelten. Zusätzlich wird angenommen, dass sich die Strömung nicht mit der Zeit ändert~\cite{van2015grundlagen}.


% Masseannahmen
Eine weitere Annahme ist, dass der Rotor das Trägheitsmoment~$J$ eines Zylinders besitzt:

\begin{equation} \label{eq:BasisModellTrägheitsmoment}
J = \frac{1}{2} \cdot m \cdot \tilde{r}^2 \mathrm{.}
\end{equation}

Die Dichte des Rotors wird ebenso über den gesamten Radius als konstant angenommen.


\subsubsection{Herleitung der Systemgleichung}
%% Herleitung: Änderung der Höhe und vertikalen Geschwindigkeit
Die zeitliche Änderung der Höhe kann dem Zustand entnommen werden. Die vertikale Beschleunigung erhält man durch Summierung aller vertikalen Kräfte, also der Gravitationskraft~$F_\mathrm{G}$, der Zugkraft~$F_\mathrm{T}$ sowie den vertikalen Anteilen der Auftriebs- bzw. Widerstandskraft~$F_\mathrm{L,v}$ und~$F_\mathrm{D,v}$.
Im Gegensatz zum Referenzmodell wird bei der Berechnung der Kräfte und Winkel ignoriert, dass der Rotor einen Abwind erzeugt. Ebenso wird vernachlässigt, dass die Rotationsgeschwindigkeit vom Radius abhängt. Aus dem zweiten Netwonschen Gesetz folgt die Beschleunigung die der Rotor erfährt~(\ref{BasisModell:VertikaleBeschleunigung}).


%% Herleitung: Änderung der Rotationsgeschwindigkeit
Die Rotationsbeschleunigung wird wie beim Referenzmodell aus der Summe aller horizontaler Kräfte~($F_\mathrm{L,h}$~und~$F_\mathrm{D,h}$) berechnet. Auch hier wird der Einfluss des Rotors auf die ihn umgebende Luft ignoriert. Ein weiterer Unterschied zum Referenzmodell ist, dass das Trägheitsmoment deutlich gröber geschätzt wird. Für die Änderung der Rotationsgeschwindigkeit mit dem Trägheitsmoment~(\ref{eq:BasisModellTrägheitsmoment}) gilt~(\label{eq:BasisModell:Rotationsbeschleunigung}).


%% Herleitung: Änderung der mittleren Kraft und Systemgleichung
Da der vom Rotor erzeugte Abwind vom Basismodell ignoriert wird, muss auch keine Änderung der mittleren Kraft auf die Luft bestimmt werden. Die gesuchte Funktion für die Änderung der Systemzustände ist somit:

\begin{equation}
f(x, u) = 
\begin{bmatrix}
\dot z \\ 
\ddot z \\ 
\dot \Omega \\ 
\dot{\bar{F}}
\end{bmatrix}
=
\begin{bmatrix}
\dot z \\

\frac{1}{m}  \cdot [n \cdot F_\mathrm{L,v}(\gamma, \tilde{r}) - n \cdot F_\mathrm{D,v}(\gamma, \tilde{r}) - F_\mathrm{G}] \\

n \cdot \tilde{r} \cdot \frac{1}{2 \cdot m \cdot J} \cdot [F_\mathrm{L,h}(\gamma, \tilde{r}) - F_\mathrm{D,h}(\gamma, \tilde{r})] \\

0
\end{bmatrix} \mathrm{.}
\end{equation}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Neuronales Netz
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Neuronales Netz} \label{Modelle:NeuronalesNetz}
%% Ziel und Herangehensweise
% Systemgleichung des Referenzmodells durch \ac{FFN} zu approximieren
% 1. Daten zum Training mittels Referenzmodell erzeugen; Referenzmodell wird mit verschiedenen Zustandskombinationen gefüttert und Zustandsänderung ausgelesen; Aufteilung in Training und Test
% 2. Suche nach optimaler Netzstruktur: 6 Eingänge und 4 Ausgänge; 1-3 versteckte Schichten; 4-128 Neuronen pro Schicht; Aktivierungsfunktionen "ReLU", "Sigmoid" und "Tanh"; verschiedene Lernraten und Epochen



\subsubsection{Daten} \label{Modelle:NeuronalesNetz:Daten}
%% Daten


\subsubsection{Architektur} \label{Modelle:NeuronalesNetz:Architektur}
%% Architektur


\subsubsection{Hyperparameteroptimierung} \label{Modelle:NeuronalesNetz:Hyperparameteroptimierung}
%% Hyperparameteroptimierung
